# Attendance tracker project

## Developer setup:

#### NOTES: After installation of Python, PostgreSQL, PyCharm, Node.js PC restart is required.

### API:
- Install Python 3.7 from https://www.python.org/downloads/
- Install PostgreSQL 10.5 from https://www.openscg.com/bigsql/postgresql/installers.jsp/
-- Make sure that you set password to be: 0000
- Install PyCharm from https://www.jetbrains.com/pycharm/
- Open psql and create database schema by running `CREATE DATABASE attendance_tracker;`
- Open cloned project using PyCharm and open terminal in it (Alt + F12)
- Run `pip install -r requirements.txt` (Django, Django REST framework and PostgreSQL python driver)
- Run `python manage.py migrate` (This will create some basic database structure) 
- Run `python manage.py createsuperuser` (Go through the dialog - this will create superadmin user for you)
- Start development server either from PyCharm or by running `python manage.py runserver`

### FRONT:
- Install node.js interpreter from https://nodejs.org/en/download/
- From cmd/powershell run `npm install -g expo-cli`
- From cmd/powershell run `npm install`
- From cmd/powershell run `npm start` (make sure you are inside front folder)

## Deployments (API)
Attendance tracker API has 2 currently deployed versions.

### Develop
This is a development server used to instantly test new features. Also used by front-end developers so that there is no need for them to set up local API for development.
Source code for this deployment is taken from branch `develop` and the app is automatically updated on every push to branch `develop`.  
It is available at: https://attendance-tracker-develop.herokuapp.com/  
Admin panel is available at: https://attendance-tracker-develop.herokuapp.com/admin/  
Administrator credentials `username: admin, password: admin`

### Release
This is a production server used to deploy already tested features. It will be used for closed beta testing by customers.
Source code for this deployment is taken from branch `master` and the app is automatically updated on every push to branch `master`.  
It is available at: https://attendance-tracker-release.herokuapp.com/

## Deployments (Mobile app)

### Android
Attendance tracker mobile application for Android is published on Google Play and is available at: https://play.google.com/store/apps/details?id=ba.pmf
Due to the unpopularity and small number of downloads it is currently hard to find using just 'Attendance tracker' search.

### iOS
Still not published.
