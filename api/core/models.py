"""
    Contains all core database models for the app.
    Author: ezdilar 15-10-2018
"""
from django.db import models
from django.utils import timezone


class Device (models.Model):
    """
    Model for device table.
    """

    class Meta:
        db_table = 'device'

    id = models.BigAutoField(unique=True, primary_key=True)
    device_external_id = models.CharField(max_length=255, unique=True)

    def __str__(self)-> str:
        return "Device. ID: {}. External device ID: {}".format(self.id, self.device_external_id)

    def __repr__(self)-> str:
        return self.__str__()


class Course (models.Model):
    """
    Model for course table.
    """

    class Meta:
        db_table = 'course'

    id = models.BigAutoField(unique=True, primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self) -> str:
        return "Course. ID: {}. Name: {}".format(self.id, self.name)

    def __repr__(self)-> str:
        return self.__str__()


class QrCode(models.Model):
    """
    Model for qr_code table.
    """

    class Meta:
        db_table = 'qr_code'

    id = models.BigAutoField(unique=True, primary_key=True)
    valid_from = models.DateTimeField(default=timezone.now)
    valid_to = models.DateTimeField(null=True)

    def __str__(self) -> str:
        return "QR code. ID: {}. Valid from: {} to: {}".format(self.id, self.valid_from, self.valid_to)

    def __repr__(self)-> str:
        return self.__str__()


class GpsCoordinate(models.Model):
    """
    Model for gps_coordinate table.
    """

    class Meta:
        db_table = 'gps_coordinate'

    id = models.BigAutoField(unique=True, primary_key=True)
    lat = models.FloatField()
    lon = models.FloatField()
    alt = models.FloatField()

    def __str__(self) -> str:
        return "GPS Coordinate. ID: {}. Lat: {}, lon: {}, alt: {}".format(self.id, self.lat, self.lon, self.alt)

    def __repr__(self)-> str:
        return self.__str__()


class OrgUnit(models.Model):
    """
    Model for org_unit table.
    """

    class Meta:
        db_table = 'org_unit'

    id = models.BigAutoField(unique=True, primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self) -> str:
        return "Organisational unit. ID: {}. Name: {}".format(self.id, self.name)

    def __repr__(self)-> str:
        return self.__str__()


class Classroom(models.Model):
    """
    Model for classroom table.
    """

    class Meta:
        db_table = 'classroom'

    id = models.BigAutoField(unique=True, primary_key=True)
    name = models.CharField(max_length=255)
    coordinates = models.ForeignKey(GpsCoordinate, db_column='coordinates', on_delete=models.PROTECT)
    org_unit = models.ForeignKey(OrgUnit, db_column='org_unit', on_delete=models.PROTECT)

    def __str__(self) -> str:
        return "Classroom. ID: {}. Name: {}. Location: {}. {}".\
            format(self.id, self.name, self.coordinates, self.org_unit)

    def __repr__(self)-> str:
        return self.__str__()


class Lecture(models.Model):
    """
    Model for lecture table.
    """
    LECTURE_TYPE = (
        ('L', 'Lecture'),
        ('E', 'Exercise'),
    )

    class Meta:
        db_table = 'lecture'

    id = models.BigAutoField(unique=True, primary_key=True)
    course = models.ForeignKey(Course, db_column='course', on_delete=models.PROTECT)
    qr_code = models.ForeignKey(QrCode, db_column='qr_code', on_delete=models.PROTECT)
    type = models.CharField(max_length=1, choices=LECTURE_TYPE)
    classroom = models.ForeignKey(Classroom, db_column='classroom', on_delete=models.PROTECT)
    person_limit = models.IntegerField(null=True)

    def __str__(self) -> str:
        return "Lecture. ID: {}. Type: {}, {}, {}, Person limit: {}, {}".\
            format(self.id, self.type, self.course.id, self.classroom.id, self.person_limit, self.qr_code.id)

    def __repr__(self)-> str:
        return self.__str__()


class Person(models.Model):
    """
    Model for person table.
    """

    class Meta:
        db_table = 'person'

    id = models.BigAutoField(unique=True, primary_key=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    external_id = models.CharField(max_length=255, unique=True)
    email = models.CharField(max_length=255)

    def __str__(self) -> str:
        return "Person. ID: {}, {} {}, {}, {}".\
            format(self.id, self.first_name, self.last_name, self.external_id, self.email)

    def __repr__(self)-> str:
        return self.__str__()


class PersonLecture(models.Model):
    """
    Model for person_lecture table. (attendance)
    """

    ATTENDANCE_TYPE = (
        ('PR', 'Present'),
        ('AB', 'Absent'),
        ('QP', 'Questionably present'),
    )

    class Meta:
        db_table = 'person_lecture'

    person = models.ForeignKey(Person, db_column='person', on_delete=models.PROTECT)
    lecture = models.ForeignKey(Lecture, db_column='lecture', on_delete=models.PROTECT)
    attendance_type = models.CharField(max_length=2, choices=ATTENDANCE_TYPE)
    created_at = models.DateTimeField()
    coordinates = models.ForeignKey(GpsCoordinate, db_column='coordinates', on_delete=models.PROTECT)

    def __str__(self) -> str:
        return "Attendee: {} was {} in a lecture {}. At: {}, Location: {}".\
            format(self.person, self.attendance_type, self.lecture,  self.created_at, self.coordinates)

    def __repr__(self)-> str:
        return self.__str__()


class PersonDevice(models.Model):
    """
    Model for person_device table. (attendance)
    """

    class Meta:
        db_table = 'person_device'

    person = models.ForeignKey(Person, db_column='person', on_delete=models.PROTECT)
    device = models.ForeignKey(Device, db_column='device', on_delete=models.PROTECT)
    valid_from = models.DateTimeField(default=timezone.now)
    valid_to = models.DateTimeField(null=True)

    def __str__(self) -> str:
        return "{} owns a {}. Valid from {} to {}".\
            format(self.person, self.device, self.valid_from, self.valid_to)

    def __repr__(self)-> str:
        return self.__str__()


class PersonCourse(models.Model):
    """
    Model for person_course table. (enrolment)
    """
    ROLE_TYPE = (
        ('T', 'Teacher'),
        ('A', 'Teaching assistant'),
        ('S', 'Student'),
    )

    class Meta:
        db_table = 'person_course'

    person = models.ForeignKey(Person, db_column='person', on_delete=models.PROTECT)
    role = models.CharField(max_length=1, choices=ROLE_TYPE)
    course = models.ForeignKey(Course, db_column='course', on_delete=models.PROTECT)

    def __str__(self) -> str:
        return "{} has a {} role on a {}".format(self.person, self.role, self.course)

    def __repr__(self)-> str:
        return self.__str__()
