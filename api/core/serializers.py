"""
    Created by MelihaK on 24/10/2018.
    Contains classes for serializing model objects.
"""

from rest_framework import serializers
from .models import *


class DeviceSerializer(serializers.ModelSerializer):
    """
    Serializer for Device model. Contains only id.
    """
    
    class Meta:
        model = Device
        fields = '__all__'
        # exclude = ('device_id',)
        

class CourseSerializer(serializers.ModelSerializer):
    """
    Serializer for Course model. Contains all fields.
    """

    class Meta:
        model = Course
        fields = '__all__'


class QrCodeSerializer(serializers.ModelSerializer):
    """
    Serializer for QrCode model. Contains all fields.
    """

    class Meta:
        model = QrCode
        fields = '__all__'


class GpsCoordinateSerializer(serializers.ModelSerializer):
    """
    Serializer for GpsCoordinate model. Contains all fields.
    """

    class Meta:
        model = GpsCoordinate
        fields = '__all__'


class OrgUnitSerializer(serializers.ModelSerializer):
    """
    Serializer for OrgUnit model. Contains all fields.
    """

    class Meta:
        model = OrgUnit
        fields = '__all__'


class ClassroomSerializer(serializers.ModelSerializer):
    """
    Serializer for Classroom model. Contains id, name and organizational unit.
    """

    org_unit = OrgUnitSerializer(read_only=True)
    org_unit_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=OrgUnit.objects)

    class Meta:
        model = Classroom
        exclude = ('coordinates',)


class LectureSerializer(serializers.ModelSerializer):
    """
    Serializer for Lecture model. Contains basic info fields.
    """

    qr_code = QrCodeSerializer(read_only=True)
    qr_code_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=QrCode.objects)

    classroom = ClassroomSerializer(read_only=True)
    classroom_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Classroom.objects)

    course = CourseSerializer(read_only=True)
    course_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Course.objects)

    class Meta:
        model = Lecture
        fields = '__all__'

    def create(self, validated_data):
        vd = validated_data

        lecture = Lecture.objects.create(qr_code=vd.get('qr_code_id'),
                                         classroom=vd.get('classroom_id'),
                                         course=vd.get('course_id'),
                                         type=vd.get('type'),
                                         person_limit=vd.get('person_limit', None))
        return lecture

    def update(self, instance, validated_data):

        instance.qr_code = validated_data.get('qr_code_id')
        instance.classroom = validated_data.get('classroom_id', instance.classroom)
        instance.course = validated_data.get('course_id', instance.course)
        instance.type = validated_data.get('type', instance.type)
        instance.person_limit = validated_data.get('person_limit', instance.person_limit)

        instance.save()
        return instance


class LectureSimpleSerializer(serializers.ModelSerializer):
    """
    Serializer for Lecture model. Contains basic info fields.
    """

    class Meta:
        model = Lecture
        fields = '__all__'


class PersonSerializer(serializers.ModelSerializer):
    """
    Serializer for Person model. Contains all fields.
    """

    class Meta:
        model = Person
        fields = '__all__'


class PersonLectureSerializer(serializers.ModelSerializer):
    """
    Serializer for PersonLecture model. Contains basic info fields.
    """

    person = PersonSerializer(read_only=True)
    person_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Person.objects)
    lecture = LectureSerializer(read_only=True)
    lecture_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Lecture.objects)
    coordinates_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=GpsCoordinate.objects)

    class Meta:
        model = PersonLecture
        exclude = ('coordinates',)

    def create(self, validated_data):
        vd = validated_data
        person_lecture = PersonLecture.objects.create(person=vd.get('person_id'),
                                                      lecture=vd.get('lecture_id'),
                                                      created_at=vd.get('created_at', timezone.now()),
                                                      attendance_type=vd.get('attendance_type'),
                                                      coordinates=vd.get('coordinates_id'))
        return person_lecture


class PersonLectureSimpleSerializer(serializers.ModelSerializer):
    """
    Serializer for PersonLecture model. Contains basic info fields.
    """

    person = PersonSerializer(read_only=True)
    person_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Person.objects)
    lecture = LectureSimpleSerializer(read_only=True)
    lecture_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Lecture.objects)

    class Meta:
        model = PersonLecture
        fields = '__all__'


class PersonDeviceSerializer(serializers.ModelSerializer):
    """
    Serializer for PersonDevice model. Contains all fields.
    """

    person = PersonSerializer(read_only=True)
    person_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Person.objects)
    device = DeviceSerializer(read_only=True)
    device_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Device.objects)

    class Meta:
        model = PersonDevice
        fields = '__all__'

    def create(self, validated_data):
        vd = validated_data

        person_device = PersonDevice.objects.create(person=vd.get('person_id'),
                                                    device=vd.get('device_id'),
                                                    valid_from=timezone.now())

        return person_device


class PersonCourseSerializer(serializers.ModelSerializer):
    """
    Serializer for PersonCourse model. Contains all fields.
    """

    course = CourseSerializer()
    person = PersonSerializer()

    class Meta:
        model = PersonCourse
        fields = '__all__'
