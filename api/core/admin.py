from django.contrib import admin
from .models import Device, Course, QrCode, GpsCoordinate, OrgUnit, Classroom, Lecture, Person, PersonLecture,\
    PersonDevice, PersonCourse

admin.site.register(Device)
admin.site.register(Course)
admin.site.register(QrCode)
admin.site.register(GpsCoordinate)
admin.site.register(OrgUnit)
admin.site.register(Classroom)
admin.site.register(Lecture)
admin.site.register(Person)
admin.site.register(PersonLecture)
admin.site.register(PersonDevice)
admin.site.register(PersonCourse)
