from django.test import TestCase

from core.models import GpsCoordinate
from core.utils.location import calculate_coordinates_distance


class TestLocation(TestCase):
    def setUp(self):
        self.location_1 = GpsCoordinate(lat=43.854028, lon=18.395167, alt=531)
        self.location_2 = GpsCoordinate(lat=43.826496, lon=18.3023321, alt=531)

    def test_location_calculation(self):
        self.assertAlmostEqual(calculate_coordinates_distance(self.location_1, self.location_2), 8.06, places=1)

    def tearDown(self):
        GpsCoordinate.objects.all().delete()
