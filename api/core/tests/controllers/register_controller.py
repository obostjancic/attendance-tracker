"""
    Unit tests for registration.
    Author: Emilija Zdilar 04-11-2018
"""
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status

from core.models import Person, Device, PersonDevice
from core.serializers import PersonSerializer, DeviceSerializer


class TestSuccessfulRegister(TestCase):
    def setUp(self):
        self.existing_user = Person.objects.create(first_name='Ognjen', last_name='Bostjancic',
                                                   email='ognjen.bostjancic@gmail.com', external_id='274/5264/M-17')
        self.new_user_1 = Person(first_name='Emilija', last_name='Zdilar',
                                 email='zdilar.emilija@gmail.com', external_id='external_1')

        self.new_user_2 = Person(first_name='Lejla', last_name='Dzananovic',
                                 email='lejladzananovic@gmail.com', external_id='external_2')

        self.existing_device = Device.objects.create(device_external_id="device1Id")
        self.new_device_1 = Device(device_external_id="device2Id")
        self.new_device_2 = Device(device_external_id="device3Id")

    def test_registration_existing_user_existing_device(self):

        client = Client()
        vs_person = PersonSerializer(self.existing_user)
        vs_device = DeviceSerializer(self.existing_device)

        response = client.post(
            reverse('core:register'),
            data={
                "person":
                    {"first_name": vs_person.data["first_name"],
                     "last_name": vs_person.data["last_name"],
                     "email": vs_person.data["email"],
                     "external_id": vs_person.data["external_id"]
                     },
                "device":
                    {
                        "device_external_id": vs_device.data["device_external_id"]
                    }
            },
            content_type="application/json"
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_new_user_new_device(self):
        client = Client()
        vs_person = PersonSerializer(self.new_user_1)
        vs_device = DeviceSerializer(self.new_device_1)

        response = client.post(
            reverse('core:register'),
            data={
                "person":
                    {"first_name": vs_person.data["first_name"],
                     "last_name": vs_person.data["last_name"],
                     "email": vs_person.data["email"],
                     "external_id": vs_person.data["external_id"]
                     },
                "device":
                    {
                        "device_external_id": vs_device.data["device_external_id"]
                    }
            },
            content_type="application/json"
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_new_user_existing_device(self):
        client = Client()
        vs_person = PersonSerializer(self.new_user_2)
        vs_device = DeviceSerializer(self.existing_device)

        response = client.post(
            reverse('core:register'),
            data={
                "person":
                    {"first_name": vs_person.data["first_name"],
                     "last_name": vs_person.data["last_name"],
                     "email": vs_person.data["email"],
                     "external_id": vs_person.data["external_id"]
                     },
                "device":
                    {
                        "device_external_id": vs_device.data["device_external_id"]
                    }
            },
            content_type="application/json"
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_existing_user_new_device(self):
        client = Client()
        vs_person = PersonSerializer(self.existing_user)
        vs_device = DeviceSerializer(self.new_device_2)

        response = client.post(
            reverse('core:register'),
            data={
                "person":
                    {"first_name": vs_person.data["first_name"],
                     "last_name": vs_person.data["last_name"],
                     "email": vs_person.data["email"],
                     "external_id": vs_person.data["external_id"]
                     },
                "device":
                    {
                        "device_external_id": vs_device.data["device_external_id"]
                    }
            },
            content_type="application/json"
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def tearDown(self) -> None:
        PersonDevice.objects.all().delete()
        Person.objects.all().delete()
        Device.objects.all().delete()


class TestFailedRegister(TestCase):
    def setUp(self):
        self.existing_user = Person.objects.create(first_name='Ognjen', last_name='Bostjancic',
                                                   email='ognjen.bostjancic@gmail.com', external_id='274/5264/M-17')
        self.invalid_user = Person(first_name='', last_name='Zdilar',
                                   email='zdilar.emilija@gmail.com', external_id='')

        self.existing_device = Device.objects.create(device_external_id="device1Id")
        self.invalid_device = Device(device_external_id="")

    def test_registration_existing_user_invalid_device(self):
        client = Client()
        vs_person = PersonSerializer(self.existing_user)
        vs_device = DeviceSerializer(self.invalid_device)

        response = client.post(
            reverse('core:register'),
            data={
                "person":
                    {"first_name": vs_person.data["first_name"],
                     "last_name": vs_person.data["last_name"],
                     "email": vs_person.data["email"],
                     "external_id": vs_person.data["external_id"]
                     },
                "device":
                    {
                        "device_external_id": vs_device.data["device_external_id"]
                    }
            },
            content_type="application/json"
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_invalid_user_existing_device(self):
        client = Client()
        vs_person = PersonSerializer(self.invalid_user)
        vs_device = DeviceSerializer(self.existing_device)

        response = client.post(
            reverse('core:register'),
            data={
                "person":
                    {"first_name": vs_person.data["first_name"],
                     "last_name": vs_person.data["last_name"],
                     "email": vs_person.data["email"],
                     "external_id": vs_person.data["external_id"]
                     },
                "device":
                    {
                        "device_external_id": vs_device.data["device_external_id"]
                    }
            },
            content_type="application/json"
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def tearDown(self) -> None:
        PersonDevice.objects.all().delete()
        Person.objects.all().delete()
        Device.objects.all().delete()
