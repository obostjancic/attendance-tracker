import datetime

from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from core.models import QrCode
from core.serializers import QrCodeSerializer
from core.tests.controllers.controller_tests_parent import ControllerTest

"""
    Unit tests for REST controller containing qr_code endpoint.
    Author: Emilija Zdilar 17-11-2018
"""


class TestGetAllQrCodes(ControllerTest):

    def setUp(self):
        super().setUp()
        self.all_qr_codes_url = reverse('core:get_all_qr_codes')
        self.qr_code1 = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                              valid_to=datetime.datetime(2018, 1, 1, 12, 59, 59))
        self.qr_code2 = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 3, 12, 14, 59),
                                              valid_to=datetime.datetime(2018, 1, 3, 12, 59, 59))

    def test_get_all_qr_codes(self):
        client = Client()
        response = client.get(self.all_qr_codes_url, HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        all_qr_codes = QrCode.objects.all()
        serializer = QrCodeSerializer(all_qr_codes, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self) -> None:
        QrCode.objects.all().delete()
        super().tearDown()
