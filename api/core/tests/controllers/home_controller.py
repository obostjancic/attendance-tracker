from django.test import TestCase, Client
from django.urls import reverse

from core.tests.controllers.controller_tests_parent import ControllerTest

"""
    Unit tests for REST controller containing home endpoint.
    Author: Emilija Zdilar  14-11-2018
"""


class TestHome(ControllerTest):

    def setUp(self):
        super().setUp()

    def test_home(self):
        pass

    def tearDown(self):
        super().tearDown()
