import datetime
from django.test import Client
from django.urls import reverse
from rest_framework import status
from core.models import QrCode, Course, OrgUnit, Lecture, Classroom, GpsCoordinate
from core.serializers import LectureSerializer
from core.tests.controllers.controller_tests_parent import ControllerTest

"""
    Unit tests for REST controller containing lecture endpoint.
    Author: Emilija Zdilar  14-11-2018
"""


class TestGetAllLectures(ControllerTest):

    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)

        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                             valid_to=datetime.datetime(2018, 1, 1, 12, 59, 59))

        self.lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code, type="L",
                                              classroom=self.classroom, person_limit=20)

    def test_get_all_lectures(self):
        client = Client()
        response = client.get(reverse('core:get_all_lectures'), HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        all_lectures = Lecture.objects.all()
        serializer = LectureSerializer(all_lectures, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self) -> None:
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        super().tearDown()


class TestGetSingleLecture(ControllerTest):

    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                             valid_to=datetime.datetime(2018, 1, 1, 12, 59, 59))

        self.lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code, type="L",
                                              classroom=self.classroom, person_limit=20)

    def test_get_valid_single_lecture(self):
        client = Client()
        response = client.get(
            reverse('core:get_single_lecture', kwargs={'lecture_id': self.lecture.pk}),
            HTTP_DEVICE_ID=self.device_teacher.device_external_id)

        single_lecture = Lecture.objects.get(pk=self.lecture.pk)
        single_lecture_serializer = LectureSerializer(single_lecture)

        self.assertEqual(response.data, single_lecture_serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_lecture(self):
        client = Client()
        response = client.get(
            reverse('core:get_single_lecture', kwargs={'lecture_id': 8000}),
            HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def tearDown(self) -> None:
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        super().tearDown()


class TestDeleteSingleLecture (ControllerTest):

    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                             valid_to=datetime.datetime(2018, 1, 1, 12, 59, 59))

        self.lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code, type="L",
                                              classroom=self.classroom, person_limit=20)

    def test_valid_delete_lecture(self):
        client = Client()
        response = client.delete(
            reverse('core:delete_lecture', kwargs={'lecture_id': self.lecture.pk}),
            HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_lecture(self):
        client = Client()
        response = client.delete(
            reverse('core:delete_lecture', kwargs={'lecture_id': 8000}),
            HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def tearDown(self) -> None:
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        super().tearDown()


class TestPostLecture (ControllerTest):

    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                             valid_to=datetime.datetime(2018, 1, 1, 12, 59, 59))

        self.valid_lecture = Lecture(course=self.course, qr_code=self.qr_code, type="L",
                                     classroom=self.classroom, person_limit=20)

    def test_create_valid_lecture(self):

        client = Client()
        response = client.post(
            reverse('core:post_lecture'),
            data={
                "course_id": self.valid_lecture.course_id,
                "qr_code_id": self.valid_lecture.qr_code_id,
                "type": self.valid_lecture.type,
                "classroom_id": self.valid_lecture.classroom_id,
                "person_limit": self.valid_lecture.person_limit
                },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def tearDown(self) -> None:
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        super().tearDown()


class TestUpdateLecture (ControllerTest):
    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                             valid_to=datetime.datetime(2018, 1, 1, 12, 59, 59))

        self.lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code, type="L",
                                              classroom=self.classroom, person_limit=20)

        self.updated_lecture = Lecture(course=self.course, qr_code=self.qr_code, type="L",
                                       classroom=self.classroom, person_limit=100)

    def test_update_valid_lecture(self):
        client = Client()
        response = client.put(

            reverse('core:put_lecture', kwargs={'lecture_id': self.lecture.pk}),
            data={
                "course_id": self.updated_lecture.course_id,
                "qr_code_id": self.updated_lecture.qr_code_id,
                "type": self.updated_lecture.type,
                "classroom_id": self.updated_lecture.classroom_id,
                "person_limit": self.updated_lecture.person_limit
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self) -> None:
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        super().tearDown()

