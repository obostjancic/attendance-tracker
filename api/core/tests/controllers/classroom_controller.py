from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from core.models import OrgUnit, Classroom, GpsCoordinate
from core.serializers import ClassroomSerializer
from core.tests.controllers.controller_tests_parent import ControllerTest

"""
    Unit tests for REST controller containing classroom endpoint.
    Author: Emilija Zdilar  17-11-2018
"""


class TestGetAllClassrooms(ControllerTest):

    def setUp(self):
        super().setUp()
        self.all_classrooms_url = reverse('core:get_all_classrooms')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.org_unit_2 = OrgUnit.objects.create(name='ETF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.classroom_2 = Classroom.objects.create(name='728', coordinates=self.gps_coordinates,
                                                    org_unit=self.org_unit_2)

    def test_get_all_classrooms(self):
        client = Client()
        response = client.get(self.all_classrooms_url, HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        all_classrooms = Classroom.objects.all()
        serializer = ClassroomSerializer(all_classrooms, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_classrooms_pmf(self):
        client = Client()
        response = client.get('/api/core/classroom/?org_unit={}'.format(self.org_unit.pk),
                              HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        all_classrooms_pmf = Classroom.objects.filter(org_unit=self.org_unit.pk)
        serializer = ClassroomSerializer(all_classrooms_pmf, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_classroom(self):
        client = Client()
        response = client.get('/api/core/classroom/{}/'.format(self.classroom_2.pk),
                              HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        single_classroom = Classroom.objects.filter(pk=self.classroom_2.pk).first()
        serializer = ClassroomSerializer(single_classroom)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self) -> None:
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        OrgUnit.objects.all().delete()
        super().tearDown()
