from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from core.models import GpsCoordinate
from core.serializers import GpsCoordinateSerializer
from core.tests.controllers.controller_tests_parent import ControllerTest

"""
    Unit tests for REST controller containing gps_coordinate endpoint.
    Author: Emilija Zdilar  17-11-2018
"""


class TestGetAllGpsCoordinates(ControllerTest):

    def setUp(self):
        super().setUp()
        self.all_gps_coordinates_url = reverse('core:get_all_gps_coordinates')
        self.gps_coordinates1 = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.gps_coordinates2 = GpsCoordinate.objects.create(lat=44.854029, lon=18.395167, alt=0)
        self.gps_coordinates3 = GpsCoordinate.objects.create(lat=43.854020, lon=19.395177, alt=0)

    def test_get_all_gps_coordinates(self):
        client = Client()
        response = client.get(self.all_gps_coordinates_url, HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        all_gps_coordinates = GpsCoordinate.objects.all()
        serializer = GpsCoordinateSerializer(all_gps_coordinates, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self) -> None:
        GpsCoordinate.objects.all().delete()
        super().tearDown()
