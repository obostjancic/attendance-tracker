from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from core.models import OrgUnit
from core.serializers import OrgUnitSerializer
from core.tests.controllers.controller_tests_parent import ControllerTest

"""
    Unit tests for REST controller containing org_unit endpoint.
    Author: Emilija Zdilar  17-11-2018
"""


class TestGetAllOrgUnits(ControllerTest):

    def setUp(self):
        super().setUp()
        self.all_org_units_url = reverse('core:get_all_org_units')
        self.org_unit_1 = OrgUnit.objects.create(name='unit_test1')
        self.org_unit_2 = OrgUnit.objects.create(name='unit_test2')
        self.org_unit_3 = OrgUnit.objects.create(name='unit_test3')

    def test_get_all_org_units(self):
        client = Client()
        response = client.get(self.all_org_units_url, HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        all_org_units = OrgUnit.objects.all()
        serializer = OrgUnitSerializer(all_org_units, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self) -> None:
        OrgUnit.objects.all().delete()
        super().tearDown()
