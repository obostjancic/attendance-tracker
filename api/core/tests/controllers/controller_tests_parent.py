from django.test import TestCase
from django.utils import timezone

from core.models import Person, Device, PersonDevice


class ControllerTest(TestCase):
    def setUp(self):
        self.teacher = Person.objects.create(first_name="first_name_teacher", last_name="last_name_teacher",
                                             external_id="teacher_id", email="pmf_ogi_blabla@gmail.com")
        self.student = Person.objects.create(first_name="first_name_student", last_name="last_name_student",
                                             external_id="student_id", email="email_student@gmail.com")

        self.device_teacher = Device.objects.create(device_external_id='123')
        self.device_student = Device.objects.create(device_external_id='student_external_id')
        self.person_device_teacher = PersonDevice.objects.create(person=self.teacher,
                                                                 device=self.device_teacher,
                                                                 valid_from=timezone.now())
        self.person_device_student = PersonDevice.objects.create(person=self.student,
                                                                 device=self.device_student,
                                                                 valid_from=timezone.now())
        self.person_not_connected = Person.objects.create(first_name='Ognjen', last_name='Bostjancic',
                                                          email='ognjen.bostjancic@gmail.com', external_id='9876468')

    def tearDown(self):
        PersonDevice.objects.all().delete()
        Person.objects.all().delete()
        Device.objects.all().delete()
