import datetime
from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from core.models import QrCode, Course, OrgUnit, Lecture, Classroom, GpsCoordinate, Person, Device, PersonDevice, \
    PersonLecture, PersonCourse
from core.tests.controllers.controller_tests_parent import ControllerTest

"""
    Unit tests for REST controller containing lecture endpoint.
    Author: Emilija Zdilar  14-11-2018
"""


class TestPostAttendLecture (TestCase):
    def setUp(self):
        self.person1 = Person.objects.create(first_name="first_name1", last_name="last_name_1", external_id="id1",
                                             email="email1@gmail.com")
        self.device1 = Device.objects.create(device_external_id='123')
        self.person_device1 = PersonDevice.objects.create(person=self.person1,
                                                          device=self.device1,
                                                          valid_from=timezone.now())
        self.person2 = Person.objects.create(first_name="first_name2", last_name="last_name_2", external_id="id2",
                                             email="email2@gmail.com")
        self.device2 = Device.objects.create(device_external_id='12345')
        self.person_device2 = PersonDevice.objects.create(person=self.person2,
                                                          device=self.device2,
                                                          valid_from=timezone.now())
        self.person3 = Person.objects.create(first_name="first_name3", last_name="last_name_3", external_id="id3",
                                             email="email3@gmail.com")
        self.device3 = Device.objects.create(device_external_id='123457')
        self.person_device3 = PersonDevice.objects.create(person=self.person3,
                                                          device=self.device3,
                                                          valid_from=timezone.now())
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.person3_course = PersonCourse.objects.create(person=self.person3, course=self.course, role="A")
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)

        self.qr_code_valid = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                                   valid_to=datetime.datetime(2025, 1, 3, 12, 59, 59))
        self.qr_code_invalid = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 3, 12, 14, 59),
                                                     valid_to=datetime.datetime(2018, 1, 3, 12, 59, 59))

        self.lecture1 = Lecture.objects.create(course=self.course, qr_code=self.qr_code_valid, type="L",
                                               classroom=self.classroom, person_limit=20)
        self.lecture2 = Lecture.objects.create(course=self.course, qr_code=self.qr_code_invalid, type="L",
                                               classroom=self.classroom, person_limit=20)

    def testPostAttendLectureValid(self):
        client = Client()
        response = client.post('/api/core/lecture/attend/?device_id={}'.format(self.device1.device_external_id),
                               data={
                                   "qr_code": self.qr_code_valid.pk,
                                   "location": {
                                       "latitude": 43.8544671,
                                       "longitude": 18.3966332
                                   }
                               },
                               content_type="application/json",
                               HTTP_DEVICE_ID=self.device1.device_external_id
                               )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def testPostAttendLectureInvalidLecture(self):
        client = Client()
        response = client.post('/api/core/lecture/attend/?device_id={}'.format(self.device1.device_external_id),
                               data={
                                   "qr_code": self.qr_code_invalid.pk,
                                   "location": {
                                       "latitude": 43.8544671,
                                       "longitude": 18.3966332
                                   }
                               },
                               content_type="application/json",
                               HTTP_DEVICE_ID=self.device1.device_external_id
                               )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def testPostAttendLectureInvalidDevice(self):
        client = Client()
        response = client.post('/api/core/lecture/attend/?device_id=invalidDeviceId',
                               data={
                                   "qr_code": self.qr_code_valid.pk,
                                   "location": {
                                       "latitude": 43.8544671,
                                       "longitude": 18.3966332
                                   }
                               },
                               content_type="application/json",
                               HTTP_DEVICE_ID="invalidDeviceId"
                               )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def testPostAttendLectureInvalidAlreadyAttends(self):
        client = Client()
        response1 = client.post('/api/core/lecture/attend/?device_id={}'.format(self.device2.device_external_id),
                                data={
                                   "qr_code": self.qr_code_valid.pk,
                                   "location": {
                                        "latitude": 43.8544671,
                                        "longitude": 18.3966332
                                    }
                                },
                                content_type="application/json",
                                HTTP_DEVICE_ID=self.device2.device_external_id
                                )
        response2 = client.post('/api/core/lecture/attend/?device_id={}'.format(self.device2.device_external_id),
                                data={
                                   "qr_code": self.qr_code_valid.pk,
                                   "location": {
                                        "latitude": 43.8544671,
                                        "longitude": 18.3966332
                                    }
                               },
                               content_type="application/json",
                               HTTP_DEVICE_ID=self.device2.device_external_id
                                )
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.status_code, status.HTTP_400_BAD_REQUEST)

    def testPostAttendLectureInvalidAssistant(self):
        client = Client()
        response = client.post('/api/core/lecture/attend/?device_id={}'.format(self.device3.device_external_id),
                               data={
                                   "qr_code": self.qr_code_valid.pk,
                                   "location": {
                                       "latitude": 43.8544671,
                                       "longitude": 18.3966332
                                   }
                               },
                               content_type="application/json",
                               HTTP_DEVICE_ID=self.device3.device_external_id
                               )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def tearDown(self):
        PersonLecture.objects.all().delete()
        PersonCourse.objects.all().delete()
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        PersonDevice.objects.all().delete()
        Device.objects.all().delete()
        Person.objects.all().delete()


class TestLectureAttendanceList(ControllerTest):
    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.person_course = PersonCourse.objects.create(person=self.teacher, course=self.course, role="T")
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                             valid_to=datetime.datetime(2020, 1, 1, 12, 59, 59))
        self.valid_lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code, type="L",
                                                    classroom=self.classroom, person_limit=20)
        self.student_1 = Person.objects.create(first_name="first_name17", last_name="last_name_17", external_id="id17",
                                               email="email17@gmail.com")
        self.student_2 = Person.objects.create(first_name="first_name18", last_name="last_name_18", external_id="id18",
                                               email="email18@gmail.com")
        self.student_1_course = PersonCourse.objects.create(person=self.student_1, course=self.course, role="S")
        self.student_2_course = PersonCourse.objects.create(person=self.student_2, course=self.course, role="S")
        self.student_2_lecture = PersonLecture.objects.create(person=self.student_2, lecture=self.valid_lecture,
                                                              coordinates=self.gps_coordinates, attendance_type="PR",
                                                              created_at=datetime.datetime(2018, 1, 1, 12, 59, 59))

    def test_get_lecture_attendance_list(self):
        client = Client()
        response = client.get(
            reverse('core:lecture_attendance_list',
                    kwargs={'lecture_id': self.valid_lecture.pk}),
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self):
        PersonLecture.objects.all().delete()
        PersonCourse.objects.all().delete()
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        PersonDevice.objects.all().delete()
        super().tearDown()


