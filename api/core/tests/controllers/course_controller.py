from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status

from core.models import Course, Person, QrCode, Lecture, PersonLecture, OrgUnit, Classroom, GpsCoordinate, PersonCourse
from core.serializers import CourseSerializer
from core.tests.controllers.controller_tests_parent import ControllerTest

"""
    Unit tests for REST controller containing course endpoint.
    Created by MelihaK on 1/11/2018.
"""


class TestGetAllCourses(ControllerTest):

    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')

    def test_get_all_courses(self):
        client = Client()
        response = client.get(reverse('core:get_all_courses'), HTTP_DEVICE_ID=self.device_teacher.device_external_id)

        all_courses = Course.objects.all()
        serializer = CourseSerializer(all_courses, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self) -> None:
        Course.objects.all().delete()
        super().tearDown()


class TestGetSingleCourse(ControllerTest):

    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')

    def test_get_valid_single_course(self):
        client = Client()
        response = client.get(reverse('core:get_single_course', kwargs={'course_id': self.course.pk}),
                              HTTP_DEVICE_ID=self.device_teacher.device_external_id)

        single_course = Course.objects.get(pk=self.course.pk)
        single_course_serializer = CourseSerializer(single_course)

        self.assertEqual(response.data, single_course_serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_course(self):
        client = Client()
        response = client.get(reverse('core:get_single_course', kwargs={'course_id': 8000}),
                              HTTP_DEVICE_ID=self.device_teacher.device_external_id)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def tearDown(self) -> None:
        Course.objects.all().delete()
        super().tearDown()


class TestDeleteSingleCourse(ControllerTest):

    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')

    def test_valid_delete_course(self):
        client = Client()
        response = client.delete(reverse('core:delete_course', kwargs={'course_id': self.course.pk}),
                                 HTTP_DEVICE_ID=self.device_teacher.device_external_id)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_course(self):
        client = Client()
        response = client.delete(reverse('core:delete_course', kwargs={'course_id': 9999}),
                                 HTTP_DEVICE_ID=self.device_teacher.device_external_id)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def tearDown(self) -> None:
        Course.objects.all().delete()
        super().tearDown()


class TestPostCourse(ControllerTest):

    def setUp(self):
        super().setUp()
        self.course = Course(name='Softverski inzenjering')

    def test_create_valid_lecture(self):
        client = Client()
        response = client.post(reverse('core:post_course'),
                               data={"name": self.course.name},
                               content_type="application/json",
                               HTTP_DEVICE_ID=self.device_teacher.device_external_id
                               )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(response.data['id'])
        self.assertEqual(response.data['name'], 'Softverski inzenjering')

    def tearDown(self) -> None:
        Course.objects.all().delete()
        super().tearDown()


class TestUpdateCourse(ControllerTest):

    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.updated_course = Course(name='Softverski inzenjering update')

    def test_update_valid_course(self):
        client = Client()
        response = client.put(
            reverse('core:put_course', kwargs={'course_id': self.course.pk}),
            data={"name": self.updated_course.name},
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.course.id)
        self.assertEqual(response.data['name'], 'Softverski inzenjering update')

    def tearDown(self) -> None:
        Course.objects.all().delete()
        super().tearDown()

# class TestGetStudentAttendancePercentage(TestCase):
#
#     def setUp(self):
#         self.course = Course.objects.create(name='Testni predmet')
#
#         self.student = Person.objects.create(first_name="first_name1", last_name="last_name_1", external_id="id1",
#                                               email="email1@gmail.com")
#
#         self.qr_code1 = QrCode.objects.create(valid_from=timezone.now(), valid_to=None)
#         self.qr_code2 = QrCode.objects.create(valid_from=timezone.now(), valid_to=None)
#
#         self.org_unit = OrgUnit.objects.create(name='Testni fakultet')
#         self.gps_coordinate = GpsCoordinate.objects.create(lat=40.741895, lon=-73.989308, alt=0)
#         self.classroom = Classroom.objects.create(name='Testna ucionica', coordinates=self.gps_coordinate,
#                                                   org_unit=self.org_unit)
#
#         self.lecture1 = Lecture.objects.create(course=self.course, qr_code=self.qr_code1,
#                                                type='L', classroom=self.classroom, person_limit=None)
#         self.lecture2 = Lecture.objects.create(course=self.course, qr_code=self.qr_code2,
#                                                type='E', classroom=self.classroom, person_limit=None)
#
#         self.student_lecture1 = PersonLecture.objects.create(person=self.student, lecture=self.lecture1,
#                                                              attendance_type='PR', created_at=timezone.now(),
#                                                              coordinates=self.gps_coordinate)
#         self.student_lecture2 = PersonLecture.objects.create(person=self.student, lecture=self.lecture2,
#                                                              attendance_type='PR', created_at=timezone.now(),
#                                                              coordinates=self.gps_coordinate)
#
#     def test_get_student_attendance_percentage(self):
#         client = Client()
#         response = client.get(reverse('core:course-attendance-percentage'))
#
#         attendance_percentage = get_student_attendance_percentage(self.student)
#
#         self.assertEqual(response.data, attendance_percentage)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#
#     def tearDown(self) -> None:
#         PersonLecture.objects.all().delete()
#         PersonCourse.objects.all().delete()
#         Lecture.objects.all().delete()
#         QrCode.objects.all().delete()
#         Classroom.objects.all().delete()
#         GpsCoordinate.objects.all().delete()
#         Course.objects.all().delete()
#         OrgUnit.objects.all().delete()
