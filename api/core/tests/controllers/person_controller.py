from django.test import Client
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from core.models import Person, PersonDevice, Device, Course, PersonCourse
from core.serializers import PersonSerializer
from core.services import person_service
from core.tests.controllers.controller_tests_parent import ControllerTest

"""
    Unit tests for REST controller containing person endpoint.
    Author: Emilija Zdilar  14-11-2018
"""


class TestGetAllPersons(ControllerTest):

    def setUp(self):
        super().setUp()
        self.all_persons_url = reverse('core:get_all_persons')
        Person.objects.create(first_name='Ognjen', last_name='Bostjancic', email='ognjen.bostjancic@gmail.com',
                              external_id='274/5264/M-17')
        Person.objects.create(first_name='Meliha', last_name='Kurtagic', email='melihakurtagic@gmail.com',
                              external_id='269/5320/M-17')
        Person.objects.create(first_name='Lejla', last_name='Dzananovic', email='lejlaadzananovic@hotmail.com',
                              external_id='265/5260/M-17')

    def test_get_all_persons(self):
        client = Client()
        response = client.get(self.all_persons_url, HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        all_persons = Person.objects.all()
        serializer = PersonSerializer(all_persons, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self) -> None:
        super().tearDown()


class TestGetSinglePerson(ControllerTest):

    def setUp(self):
        super().setUp()
        self.person1 = Person.objects.create(first_name='Ognjen', last_name='Bostjancic',
                                             email='ognjen.bostjancic@gmail.com', external_id='274/5264/M-17')
        self.person2 = Person.objects.create(first_name='Meliha', last_name='Kurtagic',
                                             email='melihakurtagic@gmail.com', external_id='269/5320/M-17')
        self.person3 = Person.objects.create(first_name='Lejla', last_name='Dzananovic',
                                             email='lejlaadzananovic@hotmail.com', external_id='265/5260/M-17')

    def test_get_valid_single_person(self):
        client = Client()
        response = client.get(
            reverse('core:get_single_person', kwargs={'person_id': self.person2.pk}),
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )
        single_person = Person.objects.get(pk=self.person2.pk)
        single_person_serializer = PersonSerializer(single_person)

        self.assertEqual(response.data, single_person_serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_person(self):
        client = Client()
        response = client.get(
            reverse('core:get_single_person', kwargs={'person_id': 800}),
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def tearDown(self) -> None:
        super().tearDown()


class TestPostPerson(ControllerTest):
    VALID_PERSON = ('Valid', 'Person', 'valid_person@gmail.com', '274/5264/M-18_unique')
    INVALID_PERSON = ('', 988978, 'ognjen.bostjancic@gmail.com', '274/5264/M-17')

    def setUp(self):
        super().setUp()
        self.person_valid = Person(first_name=self.VALID_PERSON[0], last_name=self.VALID_PERSON[1],
                                   email=self.VALID_PERSON[2], external_id=self.VALID_PERSON[3])

        self.person_invalid = Person(first_name=self.INVALID_PERSON[0], last_name=self.INVALID_PERSON[1],
                                     email=self.INVALID_PERSON[2], external_id=self.INVALID_PERSON[3])

    def test_create_valid_person(self):

        client = Client()
        vs_person = PersonSerializer(self.person_valid)
        response = client.post(
            reverse('core:post_person'),
            data={"first_name": vs_person.data["first_name"],
                  "last_name": vs_person.data["last_name"],
                  "email": vs_person.data["email"],
                  "external_id": vs_person.data["external_id"]
                  },
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_person(self):

        client = Client()
        ivs_person = PersonSerializer(self.person_invalid)
        response = client.post(
            reverse('core:post_person'),
            data={"first_name": ivs_person.data["first_name"],
                  "last_name": ivs_person.data["last_name"],
                  "email": ivs_person.data["email"],
                  "external_id": ivs_person.data["external_id"]
                  },
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def tearDown(self) -> None:
        super().tearDown()


class TestGetPersonDevice(ControllerTest):

    def setUp(self):
        super().setUp()
        self.person = Person.objects.create(first_name='Ognjen', last_name='Bostjancic',
                                            email='ognjen.bostjancic@gmail.com', external_id='274/5264/M-17')
        self.device = Device.objects.create(device_external_id='ognjenTel')
        self.valid_from = timezone.now()
        self.valid_to = None
        self.person_device = PersonDevice.objects.create(person=self.person, device=self.device,
                                                         valid_from=self.valid_from, valid_to=self.valid_to)

    def test_get_device_by_valid_id(self):
        client = Client()
        response = client.get(reverse('core:get_person_for_device',
                                      kwargs={'device_external_id': self.person_device.device.device_external_id}),
                              HTTP_DEVICE_ID=self.device_teacher.device_external_id)
        person = Person.objects.get(pk=self.person_device.person.pk)
        person_serializer = PersonSerializer(person)

        self.assertEqual(response.data, person_serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_device_by_invalid_id(self):
        self.assertRaises(ValueError, person_service.get_by_device, 'device_id')

    def tearDown(self) -> None:
        super().tearDown()


class TestGetPersonInfo(ControllerTest):

    def setUp(self):
        super().setUp()

        self.course = Course.objects.create(name='Softverski inzenjering')
        PersonCourse.objects.create(person=self.student, course=self.course, role="S")

    def test_get_device_by_valid_id(self):
        client = Client()
        response = client.get(reverse('core:person_info',),
                              HTTP_DEVICE_ID=self.device_student.device_external_id)
        person_course = PersonCourse.objects.filter(person=self.student).first()

        self.assertEqual(response.data['roles'], [person_course.role])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self) -> None:
        PersonCourse.objects.all().delete()
        super().tearDown()