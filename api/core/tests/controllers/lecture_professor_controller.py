import datetime
from django.test import Client
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from core.models import QrCode, Course, OrgUnit, Lecture, Classroom, GpsCoordinate, Person, Device, PersonDevice, \
    PersonCourse, PersonLecture
from core.tests.controllers.controller_tests_parent import ControllerTest


class TestStartLecture(ControllerTest):
    def setUp(self):
        super().setUp()
        """
        self.teacher = Person.objects.create(first_name="first_name", last_name="last_name", external_id="id",
                                             email="email1@gmail.com")
        self.student = Person.objects.create(first_name="first_name_student", last_name="last_name_student",
                                             external_id="student_id", email="email1student@gmail.com")

        self.device_teacher = Device.objects.create(device_external_id='123')
        self.device_student = Device.objects.create(device_external_id='student_external_id')
        self.person_device_teacher = PersonDevice.objects.create(person=self.teacher,
                                                                 device=self.device_teacher,
                                                                 valid_from=timezone.now())
        self.person_device_student = PersonDevice.objects.create(person=self.student,
                                                                 device=self.device_student,
                                                                 valid_from=timezone.now())
        """
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.course_1 = Course.objects.create(name='Kompjuterska vizija')
        self.person_course_1_teacher = PersonCourse.objects.create(person=self.teacher, course=self.course_1, role='T')
        self.time = timezone.now()
        self.qr_code = QrCode.objects.create(valid_from=self.time, valid_to=self.time + datetime.timedelta(minutes=100))
        self.lecture_started = Lecture.objects.create(course=self.course_1, qr_code=self.qr_code, type="L",
                                                      classroom=self.classroom, person_limit=20)
        self.teacher_2 = Person.objects.create(first_name="first_name_teacher2", last_name="last_name_teacher2",
                                               external_id="teacher_id2", email="email_teacher2@gmail.com")
        self.device_teacher_2 = Device.objects.create(device_external_id='123222')
        self.person_device_teacher_2 = PersonDevice.objects.create(person=self.teacher_2,
                                                                   device=self.device_teacher_2,
                                                                   valid_from=timezone.now())
        self.person_course_teacher_2 = PersonCourse.objects.create(person=self.teacher_2, course=self.course, role='T')

    def test_post_start_lecture_valid(self):
        client = Client()
        response = client.post(
            reverse('core:start_lecture'),
            data={
                "course": self.course.pk,
                "classroom": self.classroom.pk,
                "person_limit": 25,
                "duration": 10
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher_2.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_start_lecture_invalid_already_started(self):
        client = Client()
        response = client.post(
            reverse('core:start_lecture'),
            data={
                "course": self.course_1.pk,
                "classroom": self.classroom.pk,
                "person_limit": 25,
                "duration": 10
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_start_lecture_invalid_forbidden(self):
        client = Client()
        response = client.post(
            reverse('core:start_lecture'),
            data={
                "course": self.course.pk,
                "classroom": self.classroom.pk,
                "person_limit": 20,
                "duration": 10
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_student.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_start_lecture_invalid_bad_request(self):
        client = Client()
        response = client.post(
            reverse('core:start_lecture'),
            data={
                "course": 545678,
                "classroom": self.classroom.pk,
                "person_limit": 20,
                "duration": 10
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def tearDown(self):
        PersonCourse.objects.all().delete()
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        PersonDevice.objects.all().delete()
        Device.objects.all().delete()
        Person.objects.all().delete()
        super().tearDown()


class TestCurrentLecture(ControllerTest):
    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.person_course = PersonCourse.objects.create(person=self.teacher, course=self.course, role="T")
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                             valid_to=datetime.datetime(2020, 1, 1, 12, 59, 59))

        self.current_lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code, type="L",
                                                      classroom=self.classroom, person_limit=20)

    def test_get_current_lecture(self):
        client = Client()
        response = client.get(
           reverse('core:get_current_lecture',),
           content_type="application/json",
           HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_no_current_lecture(self):
        client = Client()
        response = client.get(
            reverse('core:get_current_lecture', ),
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_student.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def tearDown(self):
        Lecture.objects.all().delete()
        PersonCourse.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        super().tearDown()


class TestCloseLecture(ControllerTest):
    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.person_course = PersonCourse.objects.create(person=self.teacher, course=self.course, role="T")
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                             valid_to=datetime.datetime(2020, 1, 1, 12, 59, 59))

        self.lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code, type="L",
                                              classroom=self.classroom, person_limit=20)

    def test_close_lecture_valid(self):
        client = Client()
        response = client.put(
            reverse('core:close_lecture', kwargs={'lecture_id': self.lecture.pk}),
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_close_lecture_invalid(self):
        client = Client()
        response = client.put(
            reverse('core:close_lecture', kwargs={'lecture_id': self.lecture.pk}),
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_student.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def tearDown(self):
        Lecture.objects.all().delete()
        PersonCourse.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        super().tearDown()


class TestSendQrCodeEmail(ControllerTest):
    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.person_course_teacher = PersonCourse.objects.create(person=self.teacher, course=self.course, role='T')
        self.org_unit = OrgUnit.objects.create(name='PMF')

        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                             valid_to=datetime.datetime(2020, 1, 1, 12, 59, 59))
        self.valid_lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code, type="L",
                                                    classroom=self.classroom, person_limit=20)

    def test_send_qr_code_mail_valid(self):
        client = Client()
        response = client.post(
            reverse('core:send_qr_code_to_mail'),
            data={
                "qr_code": self.qr_code.pk
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_send_qr_code_mail_forbidden(self):
        client = Client()
        response = client.post(
            reverse('core:send_qr_code_to_mail'),
            data={
                "qr_code": self.qr_code.pk
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_student.device_external_id,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def tearDown(self):
        PersonCourse.objects.all().delete()
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        PersonDevice.objects.all().delete()
        Device.objects.all().delete()
        Person.objects.all().delete()
        super().tearDown()


class ManuallySetAttendance(ControllerTest):
    def setUp(self):
        super().setUp()
        self.person_1 = Person.objects.create(first_name="first_name_1", last_name="last_name_1",
                                             external_id="id_1",
                                             email="email_1@gmail.com")
        self.person_2 = Person.objects.create(first_name="first_name_2", last_name="last_name_2",
                                             external_id="id_2",
                                             email="email_2@gmail.com")
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)

        self.qr_code_valid = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                                   valid_to=datetime.datetime(2020, 1, 1, 14, 59, 59))

        self.lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code_valid, type="L",
                                              classroom=self.classroom, person_limit=20)

        self.person_course = PersonCourse.objects.create(role="S", course=self.course, person=self.person_1)
        self.person_course = PersonCourse.objects.create(role="S", course=self.course, person=self.person_2)
        self.person_course_teacher = PersonCourse.objects.create(person=self.teacher, course=self.course, role='T')
        self.person_1_lecture = PersonLecture.objects.create(attendance_type='QP',
                                                             created_at=datetime.datetime(2018, 1, 3, 12, 14, 59),
                                                             coordinates=self.gps_coordinates, lecture=self.lecture,
                                                             person=self.person_1)
        self.person_2_lecture = PersonLecture(attendance_type='AB',
                                              created_at=datetime.datetime(2018, 1, 3, 12, 14, 59),
                                              coordinates=self.gps_coordinates, lecture=self.lecture,
                                              person=self.person_2)

    def test_update_attendance_present_student(self):
        client = Client()
        response = client.put(
            reverse('core:manually_set_attendance'),
            data={
                "lecture": self.lecture.pk,
                "student": self.person_1.pk,
                "attendance_type": "PR"
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_attendance_absent_student(self):
        client = Client()
        response = client.put(
            reverse('core:manually_set_attendance'),
            data={
                "lecture": self.lecture.pk,
                "student": self.person_2.pk,
                "attendance_type": "PR"
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_attendance_invalid(self):
        client = Client()
        response = client.put(
            reverse('core:manually_set_attendance'),
            data={
                "lecture": self.lecture.pk,
                "student": self.student.pk,
                "attendance_type": "PR"
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_teacher.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_attendance_unauthorized(self):
        client = Client()
        response = client.put(
            reverse('core:manually_set_attendance'),
            data={
                "lecture": self.lecture.pk,
                "student": self.person_1.pk,
                "attendance_type": "PR"
            },
            content_type="application/json",
            HTTP_DEVICE_ID=self.device_student.device_external_id
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def tearDown(self):
        PersonCourse.objects.all().delete()
        PersonLecture.objects.all().delete()
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        super().tearDown()


