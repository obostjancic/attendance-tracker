from datetime import timedelta

from django.test import TestCase
from django.utils import timezone

from core.models import Device, Course, QrCode, GpsCoordinate, OrgUnit, Classroom, Lecture, Person, \
    PersonLecture, PersonDevice, PersonCourse

"""
    Unit tests for database models.
    Author: obostjancic 26-10-2018
"""


class DeviceModelTest(TestCase):

    def setUp(self) -> None:
        Device.objects.create(device_external_id="device1Id")
        Device.objects.create(device_external_id="device2Id")

    def test___str__(self):
        device = Device.objects.get(device_external_id="device1Id")
        self.assertEqual(device.__str__(), "Device. ID: {}. External device ID: device1Id".format(device.pk))

    def test___repr__(self):
        device = Device.objects.get(device_external_id="device2Id")
        self.assertEqual(device.__repr__(), "Device. ID: {}. External device ID: device2Id".format(device.pk))

    def tearDown(self) -> None:
        Device.objects.all().delete()


class CourseModelTest(TestCase):

    def setUp(self) -> None:
        Course.objects.create(name="course1")
        Course.objects.create(name="course2")

    def test___str__(self):
        course = Course.objects.filter(name="course1").first()
        self.assertEqual(course.__str__(), "Course. ID: {}. Name: course1".format(course.pk))

    def test___repr__(self):
        course = Course.objects.filter(name="course2").first()
        self.assertEqual(course.__repr__(), "Course. ID: {}. Name: course2".format(course.pk))

    def tearDown(self) -> None:
        Course.objects.all().delete()


class QrCodeModelTest(TestCase):

    VALID_FROM = timezone.now()
    VALID_TO = VALID_FROM + timedelta(days=1)

    def setUp(self) -> None:
        QrCode.objects.create(valid_from=self.VALID_FROM)
        QrCode.objects.create(valid_from=self.VALID_FROM, valid_to=self.VALID_TO)

    def test___str__(self):
        qr_code = QrCode.objects.filter(valid_to__isnull=True).first()
        self.assertEqual(qr_code.__str__(), "QR code. ID: {}. Valid from: {} to: {}".
                         format(qr_code.pk, self.VALID_FROM, None))

    def test___repr__(self):
        qr_code = QrCode.objects.filter(valid_to__isnull=False).first()
        self.assertEqual(qr_code.__repr__(), "QR code. ID: {}. Valid from: {} to: {}".
                         format(qr_code.pk, self.VALID_FROM, self.VALID_TO))

    def tearDown(self) -> None:
        QrCode.objects.all().delete()


class GpsCoordinateModelTest(TestCase):
    PMF_LOC = (43.854223, 18.394573, 531.0)
    BBI_LOC = (43.858246, 18.416669, 541.0)

    PMF_OUTPUT = "GPS Coordinate. ID: {}. Lat: 43.854223, lon: 18.394573, alt: 531.0"
    BBI_OUTPUT = "GPS Coordinate. ID: {}. Lat: 43.858246, lon: 18.416669, alt: 541.0"

    def setUp(self) -> None:
        GpsCoordinate.objects.create(lat=self.PMF_LOC[0], lon=self.PMF_LOC[1], alt=self.PMF_LOC[2])
        GpsCoordinate.objects.create(lat=self.BBI_LOC[0], lon=self.BBI_LOC[1], alt=self.BBI_LOC[2])

    def test___str__(self):
        gps_coord = GpsCoordinate.objects.filter(lat=self.PMF_LOC[0]).first()
        self.assertEqual(gps_coord.__str__(), self.PMF_OUTPUT.format(gps_coord.pk))

    def test___repr__(self):
        gps_coord = GpsCoordinate.objects.filter(lat=self.BBI_LOC[0]).first()
        self.assertEqual(gps_coord.__repr__(), self.BBI_OUTPUT.format(gps_coord.pk))

    def tearDown(self) -> None:
        GpsCoordinate.objects.all().delete()


class OrgUnitModelTest(TestCase):

    def setUp(self) -> None:
        OrgUnit.objects.create(name="orgUnit1")
        OrgUnit.objects.create(name="orgUnit2")

    def test___str__(self):
        org_unit = OrgUnit.objects.filter(name="orgUnit1").first()
        self.assertEqual(org_unit.__str__(), "Organisational unit. ID: {}. Name: orgUnit1".format(org_unit.pk))

    def test___repr__(self):
        org_unit = OrgUnit.objects.filter(name="orgUnit2").first()
        self.assertEqual(org_unit.__repr__(), "Organisational unit. ID: {}. Name: orgUnit2".format(org_unit.pk))

    def tearDown(self) -> None:
        OrgUnit.objects.all().delete()


class ClassroomModelTest(TestCase):

    PMF_LOC = (43.854223, 18.394573, 531.0)
    CLASSROOM_OUTPUT = "Classroom. ID: {}. Name: classroom1. Location: " \
                       "GPS Coordinate. ID: {}. Lat: 43.854223, lon: 18.394573, alt: 531.0. " \
                       "Organisational unit. ID: {}. Name: orgUnit1"

    def setUp(self) -> None:
        self.gps_coord = GpsCoordinate.objects.create(lat=self.PMF_LOC[0], lon=self.PMF_LOC[1], alt=self.PMF_LOC[2])
        self.org_unit = OrgUnit.objects.create(name="orgUnit1")

        Classroom.objects.create(name="classroom1", coordinates=self.gps_coord, org_unit=self.org_unit)

    def test___str__(self):
        classroom = Classroom.objects.filter(name="classroom1").first()
        self.assertEqual(classroom.__str__(), self.CLASSROOM_OUTPUT.
                         format(classroom.pk, self.gps_coord.pk, self.org_unit.pk))

    def test___repr__(self):
        classroom = Classroom.objects.filter(name="classroom1").first()
        self.assertEqual(classroom.__repr__(), self.CLASSROOM_OUTPUT.
                         format(classroom.pk, self.gps_coord.pk, classroom.pk))

    def tearDown(self) -> None:
        Classroom.objects.all().delete()
        OrgUnit.objects.all().delete()
        GpsCoordinate.objects.all().delete()


class LectureModelTest(TestCase):

    VALID_FROM = timezone.now()
    VALID_TO = VALID_FROM + timedelta(days=1)

    PMF_LOC = (43.854223, 18.394573, 531.0)
    LECTURE_OUTPUT_1 = "Lecture. ID: {}. Type: L"

    def setUp(self) -> None:

        course = Course.objects.create(name="course1")
        qr_code = QrCode.objects.create(valid_from=self.VALID_FROM, valid_to=self.VALID_TO)

        gps_coord = GpsCoordinate.objects.create(lat=self.PMF_LOC[0], lon=self.PMF_LOC[1], alt=self.PMF_LOC[2])
        org_unit = OrgUnit.objects.create(name="orgUnit1")
        classroom = Classroom.objects.create(name="classroom1", coordinates=gps_coord, org_unit=org_unit)

        Lecture.objects.create(course=course, qr_code=qr_code, type="L", classroom=classroom)

    def test___str__(self):
        lecture = Lecture.objects.filter(type="L").first()
        self.assertEqual(lecture.__str__()[:23], self.LECTURE_OUTPUT_1.format(lecture.pk))

    def tearDown(self) -> None:
        Lecture.objects.all().delete()
        Classroom.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        GpsCoordinate.objects.all().delete()


class PersonModelTest(TestCase):

    def setUp(self) -> None:
        Person.objects.create(first_name="name1", last_name="lname1", external_id="index1", email="name1@test.com")
        Person.objects.create(first_name="name2", last_name="lname2", external_id="index2", email="name2@test.com")

    def test___str__(self):
        person = Person.objects.get(external_id="index1")
        self.assertEqual(person.__str__(), "Person. ID: {}, name1 lname1, index1, name1@test.com".format(person.pk))

    def test___repr__(self):
        person = Person.objects.get(external_id="index2")
        self.assertEqual(person.__repr__(), "Person. ID: {}, name2 lname2, index2, name2@test.com".format(person.pk))

    def tearDown(self) -> None:
        Person.objects.all().delete()


class PersonLectureModelTest(TestCase):

    VALID_FROM = timezone.now()
    VALID_TO = VALID_FROM + timedelta(days=1)

    PMF_LOC = (43.854223, 18.394573, 531.0)
    PERSON_LECTURE_OUTPUT = "Attendee: Person."

    def setUp(self) -> None:

        course = Course.objects.create(name="course1")
        qr_code = QrCode.objects.create(valid_from=self.VALID_FROM, valid_to=self.VALID_TO)

        gps_coord = GpsCoordinate.objects.create(lat=self.PMF_LOC[0], lon=self.PMF_LOC[1], alt=self.PMF_LOC[2])
        org_unit = OrgUnit.objects.create(name="orgUnit1")
        classroom = Classroom.objects.create(name="classroom1", coordinates=gps_coord, org_unit=org_unit)

        lecture = Lecture.objects.create(course=course, qr_code=qr_code, type="L", classroom=classroom)
        person = Person.objects.create(first_name="name1", last_name="lname1",
                                       external_id="index1", email="name1@test.com")

        PersonLecture.objects.create(person=person, lecture=lecture, created_at=self.VALID_FROM,
                                     coordinates=gps_coord, attendance_type="PR")

    def test___str__(self):
        person_lecture = PersonLecture.objects.filter(created_at=self.VALID_FROM).first()
        self.assertEqual(person_lecture.__str__()[:17], self.PERSON_LECTURE_OUTPUT)

    def tearDown(self) -> None:
        PersonLecture.objects.all().delete()
        Person.objects.all().delete()
        Lecture.objects.all().delete()
        Classroom.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        GpsCoordinate.objects.all().delete()


class PersonDeviceModelTest(TestCase):
    NOW = timezone.now()
    TOMORROW = NOW + timedelta(days=1)

    OUTPUT_1 = "Person. ID: {}, name1 lname1, index1, name1@test.com owns a Device. ID: {}." \
               " External device ID: device1Id."
    OUTPUT_2 = "Person. ID: {}, name1 lname1, index1, name1@test.com owns a Device. ID: {}. " \
               "External device ID: device2Id."

    def setUp(self) -> None:
        self.person = Person.objects.create(first_name="name1", last_name="lname1",
                                            external_id="index1", email="name1@test.com")
        self.device_1 = Device.objects.create(device_external_id="device1Id")
        self.device_2 = Device.objects.create(device_external_id="device2Id")

        PersonDevice.objects.create(person=self.person, device=self.device_1, valid_from=self.NOW,
                                    valid_to=self.TOMORROW)
        PersonDevice.objects.create(person=self.person, device=self.device_2, valid_from=self.TOMORROW)

    def test___str__(self):
        person_device = PersonDevice.objects.filter(device_id__device_external_id="device1Id").first()
        self.assertEqual(person_device.__str__()[:len(self.OUTPUT_1)-2],
                         self.OUTPUT_1.format(self.person.pk, self.device_1.pk))

    def test___repr__(self):
        person_device = PersonDevice.objects.filter(device_id__device_external_id="device2Id").first()
        self.assertEqual(person_device.__repr__()[:len(self.OUTPUT_2)-2],
                         self.OUTPUT_2.format(self.person.pk, self.device_2.pk))

    def tearDown(self) -> None:
        PersonDevice.objects.all().delete()
        Person.objects.all().delete()
        Device.objects.all().delete()


class PersonCourseModelTest(TestCase):
    NOW = timezone.now()
    TOMORROW = NOW + timedelta(days=1)

    OUTPUT_1 = "Person. ID: {}, name1 lname1, index1, name1@test.com has a S role on a Course. ID: {}. Name: course1"
    OUTPUT_2 = "Person. ID: {}, name1 lname1, index1, name1@test.com has a T role on a Course. ID: {}. Name: course2"

    def setUp(self) -> None:
        self.person = Person.objects.create(first_name="name1", last_name="lname1",
                                       external_id="index1", email="name1@test.com")
        self.course_1 = Course.objects.create(name="course1")
        self.course_2 = Course.objects.create(name="course2")

        PersonCourse.objects.create(person=self.person, course=self.course_1, role="S")
        PersonCourse.objects.create(person=self.person, course=self.course_2, role="T")

    def test___str__(self):
        person_course = PersonCourse.objects.filter(role="S").first()
        self.assertEqual(person_course.__str__(), self.OUTPUT_1.format(self.person.pk, self.course_1.pk))

    def test___repr__(self):
        person_course = PersonCourse.objects.filter(role="T").first()
        self.assertEqual(person_course.__repr__(), self.OUTPUT_2.format(self.person.pk, self.course_2.pk))

    def tearDown(self) -> None:
        PersonCourse.objects.all().delete()
        Person.objects.all().delete()
        Course.objects.all().delete()
