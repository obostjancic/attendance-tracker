"""
    Unit tests for classroom service.
    Author: Emilija Zdilar 18-12-2018
"""
from django.test import TestCase

from core.models import OrgUnit, GpsCoordinate, Classroom
from core.services.classroom_service import get_by_org_unit


class TestClassroomService(TestCase):
    def setUp(self):
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.org_unit_2 = OrgUnit.objects.create(name='ETF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.classroom_2 = Classroom.objects.create(name='728', coordinates=self.gps_coordinates,
                                                    org_unit=self.org_unit_2)

    def test_get_by_org_unit(self):
        classrooms_org_unit = get_by_org_unit(self.org_unit)
        classrooms_org_unit_query = Classroom.objects.filter(org_unit=self.org_unit)
        self.assertCountEqual(classrooms_org_unit, classrooms_org_unit_query)

    def tearDown(self):
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        OrgUnit.objects.all().delete()

