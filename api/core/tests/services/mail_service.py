"""
    Unit tests for mail service
    Author: Emilija Zdilar 23-12-2018
"""
from datetime import datetime, timedelta
from unittest.mock import patch

from django.test import TestCase

from core.models import QrCode, Person, Course
from core.services.mail_service import mail_qr_code


class TestMailService(TestCase):
    def setUp(self):
        self.qr_code = QrCode.objects.create(valid_from=datetime.now(),
                                             valid_to=datetime.now() + timedelta(minutes=100))
        self.teacher = Person.objects.create(first_name="first_name", last_name="last_name", external_id="id",
                                             email="pmf___blabla@gmail.com")
        self.course = self.course = Course.objects.create(name='Softverski inzenjering')

    def test_send_email(self):
        self.assertEqual(mail_qr_code(self.qr_code, self.course, self.teacher), "Success")

    def tearDown(self):
        QrCode.objects.all().delete()
        Person.objects.all().delete()
        Course.objects.all().delete()


