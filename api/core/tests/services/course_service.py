from django.test import TestCase
from django.utils import timezone

from core.services.course_service import get_by_person, is_enrolled, get_by_person_role, link_person_to_course, \
    get_or_create_course_by_name, get_course_role_for_person, get_student_attendance_percentage, \
    course_attendance_statistics
from core.models import Person, Course, PersonCourse, Lecture, QrCode, PersonLecture, OrgUnit, GpsCoordinate, Classroom

"""
    Unit tests for course_service.
    Author: Lejla Dzananovic 11-11-2018
    Author: Emilija Zdilar 01-12-2018
"""


class TestGetByPerson(TestCase):

    def setUp(self):

        self.person1 = Person.objects.create(first_name="first_name1", last_name="last_name_1", external_id="id1",
                                             email="email1@gmail.com")

        self.course1 = Course.objects.create(name="course1")
        self.course2 = Course.objects.create(name="course2")
        self.course3 = Course.objects.create(name="course3")

        PersonCourse.objects.create(person=self.person1, course=self.course1, role="S")
        PersonCourse.objects.create(person=self.person1, course=self.course2, role="S")
        PersonCourse.objects.create(person=self.person1, course=self.course3, role="S")

    def test_get_by_person_courses(self):
        get_by_person1_courses = get_by_person(self.person1)
        get_by_person1_courses_query = Course.objects.filter(personcourse__person=self.person1)
        self.assertCountEqual(get_by_person1_courses, get_by_person1_courses_query)

    def tearDown(self) -> None:
        PersonCourse.objects.all().delete()
        Person.objects.all().delete()
        Course.objects.all().delete()


class TestGetByPersonRole(TestCase):

    def setUp(self):

        self.person1 = Person.objects.create(first_name="first_name1", last_name="last_name_1", external_id="id1",
                                             email="email1@gmail.com")
        self.person2 = Person.objects.create(first_name="first_name2", last_name="last_name_2", external_id="id2",
                                             email="email2@gmail.com")
        self.course1 = Course.objects.create(name="course1")
        self.course2 = Course.objects.create(name="course2")
        self.course3 = Course.objects.create(name="course3")
        self.course4 = Course.objects.create(name="course4")
        self.course5 = Course.objects.create(name="course5")
        self.course6 = Course.objects.create(name="course6")

        PersonCourse.objects.create(person=self.person1, course=self.course1, role="S")
        PersonCourse.objects.create(person=self.person1, course=self.course2, role="S")
        PersonCourse.objects.create(person=self.person1, course=self.course3, role="T")
        PersonCourse.objects.create(person=self.person1, course=self.course4, role="S")
        PersonCourse.objects.create(person=self.person2, course=self.course1, role="S")
        PersonCourse.objects.create(person=self.person2, course=self.course2, role="T")
        PersonCourse.objects.create(person=self.person2, course=self.course5, role="S")
        PersonCourse.objects.create(person=self.person2, course=self.course6, role="T")

        self.student_role = 'S'
        self.teacher_role = 'T'
        self.invalid_role = 'Y'

    def test_get_by_teacher_role_courses(self):
        teacher_role_courses = get_by_person_role(self.person1, self.teacher_role)
        teacher_role_courses_query = Course.objects.filter(personcourse__person=self.person1,
                                                           personcourse__role=self.teacher_role)
        self.assertCountEqual(teacher_role_courses_query, teacher_role_courses)

    def test_get_by_student_role_courses(self):
        student_role_courses = get_by_person_role(self.person2, self.student_role)
        student_role_courses_query = Course.objects.filter(personcourse__person=self.person2,
                                                           personcourse__role=self.student_role)
        self.assertCountEqual(student_role_courses_query, student_role_courses)

    def test_invalid_role(self):
        self.assertRaises(ValueError)

    def tearDown(self) -> None:
        PersonCourse.objects.all().delete()
        Person.objects.all().delete()
        Course.objects.all().delete()


class TestEnrollment (TestCase):
    def setUp(self):
        self.person1 = Person.objects.create(first_name="first_name1", last_name="last_name_1", external_id="id1",
                                             email="email1@gmail.com")
        self.person2 = Person.objects.create(first_name="first_name2", last_name="last_name_2", external_id="id2",
                                             email="email2@gmail.com")
        self.person3 = Person.objects.create(first_name="first_name3", last_name="last_name_3", external_id="id3",
                                             email="email3@gmail.com")
        self.course1 = Course.objects.create(name="course1")
        self.person_course1 = PersonCourse.objects.create(person=self.person1, course=self.course1, role="S")
        self.person_course3 = PersonCourse(person=self.person3, course=self.course1, role="S")

    def test_person_is_enrolled(self):
        self.assertEqual(is_enrolled(self.person1, self.course1), self.person_course1)

    def test_person_is_not_enrolled(self):
        self.assertEqual(is_enrolled(self.person2, self.course1), None)

    def test_enroll_person(self):
        link_person_to_course(self.person3, self.course1, 'S')
        self.assertNotEqual(is_enrolled(self.person3, self.course1), None)

    def tearDown(self) -> None:
        PersonCourse.objects.all().delete()
        Person.objects.all().delete()
        Course.objects.all().delete()


class TestGetOrCreateCourseByName(TestCase):

    def setUp(self):
        self.course1 = Course.objects.create(name='course1')

    def test_get_course_by_name(self):
        course = get_or_create_course_by_name('course1')
        course_query = Course.objects.filter(name='course1').first()

        self.assertEqual(course, course_query)

    def test_create_course_by_name(self):
        course_query = Course.objects.filter(name='course2').first()
        course = get_or_create_course_by_name('course2')

        self.assertEqual(course.name, 'course2')
        self.assertIsNone(course_query)

    def tearDown(self) -> None:
        PersonCourse.objects.all().delete()
        Course.objects.all().delete()
        Person.objects.all().delete()


class TestGetCourseRoleForPerson(TestCase):

    def setUp(self):        
        self.course1 = Course.objects.create(name="course1")
        self.person1 = Person.objects.create(first_name="first_name1", last_name="last_name_1", external_id="id1",
                                             email="email1@gmail.com")
        self.person2 = Person.objects.create(first_name="first_name2", last_name="last_name_2", external_id="id2",
                                             email="email2@gmail.com")
        self.person_course1 = PersonCourse.objects.create(person=self.person1, course=self.course1, role="T")

    def test_get_course_role_for_teacher(self):
        self.assertEqual("T", get_course_role_for_person(self.person1, self.course1))

    def test_get_course_role_invalid(self):
        self.assertEqual("", get_course_role_for_person(self.person2, self.course1))

    def tearDown(self):
        PersonCourse.objects.all().delete()
        Course.objects.all().delete()
        Person.objects.all().delete()


class TestStudentAttendancePercentage(TestCase):

    def setUp(self):
        self.course1 = Course.objects.create(name='Testni predmet')

        self.person1 = Person.objects.create(first_name="first_name1", last_name="last_name_1", external_id="id1",
                                             email="email1@gmail.com")
        self.person2 = Person.objects.create(first_name="first_name2", last_name="last_name_2", external_id="id2",
                                             email="email2@gmail.com")

        PersonCourse.objects.create(person=self.person2, course=self.course1, role="S")
        PersonCourse.objects.create(person=self.person1, course=self.course1, role="S")

        self.qr_code1 = QrCode.objects.create(valid_from=timezone.now(), valid_to=None)
        self.qr_code2 = QrCode.objects.create(valid_from=timezone.now(), valid_to=None)

        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)

        self.lecture1 = Lecture.objects.create(course=self.course1, qr_code=self.qr_code1,
                                               type='L', classroom=self.classroom, person_limit=100)
        self.lecture2 = Lecture.objects.create(course=self.course1, qr_code=self.qr_code2,
                                               type='E', classroom=self.classroom, person_limit=100)

        self.student1_lecture1 = PersonLecture.objects.create(person=self.person1, lecture=self.lecture1,
                                                              attendance_type='PR', created_at=timezone.now(),
                                                              coordinates=self.gps_coordinates)
        self.student2_lecture1 = PersonLecture.objects.create(person=self.person2, lecture=self.lecture1,
                                                              attendance_type='PR', created_at=timezone.now(),
                                                              coordinates=self.gps_coordinates)
        self.student2_lecture2 = PersonLecture.objects.create(person=self.person2, lecture=self.lecture2,
                                                              attendance_type='PR', created_at=timezone.now(),
                                                              coordinates=self.gps_coordinates)

    def test_student_attendance_percentage(self):
        self.assertEqual(len(course_attendance_statistics(self.course1, 'L')), 2)

    def tearDown(self) -> None:
        PersonLecture.objects.all().delete()
        PersonCourse.objects.all().delete()
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()


class TestCourseAttendanceStatistics(TestCase):

    def setUp(self):
        self.course1 = Course.objects.create(name='Testni predmet')

        self.person1 = Person.objects.create(first_name="first_name1", last_name="last_name_1", external_id="id1",
                                             email="email1@gmail.com")
        self.person2 = Person.objects.create(first_name="first_name2", last_name="last_name_2", external_id="id2",
                                             email="email2@gmail.com")

        PersonCourse.objects.create(person=self.person2, course=self.course1, role="S")
        PersonCourse.objects.create(person=self.person1, course=self.course1, role="S")

        self.qr_code1 = QrCode.objects.create(valid_from=timezone.now(), valid_to=None)
        self.qr_code2 = QrCode.objects.create(valid_from=timezone.now(), valid_to=None)

        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)

        self.lecture1 = Lecture.objects.create(course=self.course1, qr_code=self.qr_code1,
                                               type='L', classroom=self.classroom, person_limit=100)
        self.lecture2 = Lecture.objects.create(course=self.course1, qr_code=self.qr_code2,
                                               type='E', classroom=self.classroom, person_limit=100)

        self.student1_lecture1 = PersonLecture.objects.create(person=self.person1, lecture=self.lecture1,
                                                              attendance_type='PR', created_at=timezone.now(),
                                                              coordinates=self.gps_coordinates)
        self.student2_lecture1 = PersonLecture.objects.create(person=self.person2, lecture=self.lecture1,
                                                              attendance_type='PR', created_at=timezone.now(),
                                                              coordinates=self.gps_coordinates)
        self.student2_lecture2 = PersonLecture.objects.create(person=self.person2, lecture=self.lecture2,
                                                              attendance_type='PR', created_at=timezone.now(),
                                                              coordinates=self.gps_coordinates)

    def test_student_attendance_percentage(self):
        self.assertEqual(len(get_student_attendance_percentage(self.person1, 'L')), 1)
        self.assertEqual(len(get_student_attendance_percentage(self.person2, 'E')), 1)

    def tearDown(self) -> None:
        PersonLecture.objects.all().delete()
        PersonCourse.objects.all().delete()
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
