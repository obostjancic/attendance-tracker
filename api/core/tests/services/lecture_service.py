import datetime

from django.test import TestCase
from django.utils import timezone

from core.models import OrgUnit, GpsCoordinate, Classroom, QrCode, Course, Lecture, Person, PersonLecture, \
    PersonCourse, Device, PersonDevice
from core.serializers import LectureSerializer, PersonLectureSerializer
from core.services.lecture_service import get_by_course, get_current, attend_lecture, \
    get_lecture_type_for_person_role, lecture_already_started, generate_lecture_and_qr_code, \
    already_attends_lecture, manually_set_attendance_type

"""
    Unit tests for lecture_service.
    Author: Lejla Dzananovic 28-11-2018
    Author: Emilija Zdilar 01-12-2018
"""


class TestGetByCourse(TestCase):

    def setUp(self):
        self.course1 = Course.objects.create(name="course1")

    def test_get_by_course(self):
        get_by_course1 = get_by_course(self.course1)
        get_by_course1_query = Lecture.objects.filter(course__id=self.course1.id)
        self.assertCountEqual(get_by_course1, get_by_course1_query)

    def tearDown(self) -> None:
        Course.objects.all().delete()


class TestGetCurrent(TestCase):

    def setUp(self):

        self.person1 = Person.objects.create(first_name="first_name1", last_name="last_name_1", external_id="id1",
                                             email="email1@gmail.com")
        self.course = Course.objects.create(name='Kompjuterska vizija')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.853750, lon=18.395194, alt=531.0)
        self.classroom = Classroom.objects.create(name='426', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime.now(),
                                             valid_to=datetime.datetime.now())

        self.lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code, type="L",
                                              classroom=self.classroom, person_limit=5)
        PersonLecture.objects.create(attendance_type='PR', created_at=datetime.datetime.now(),
                                     coordinates=self.gps_coordinates, lecture=self.lecture, person=self.person1)

    def test_get_current(self):

        get_current_for_person1 = get_current(self.person1)
        get_current_for_person1_query = Lecture.objects.filter(personlecture__person=self.person1,
                                                               qr_code__valid_from__lte=datetime.datetime.now(),
                                                               qr_code__valid_to__gte=datetime.datetime.now())

        self.assertCountEqual(get_current_for_person1, get_current_for_person1_query)

    def tearDown(self) -> None:
        PersonLecture.objects.all().delete()
        Lecture.objects.all().delete()
        Classroom.objects.all().delete()
        OrgUnit.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        QrCode.objects.all().delete()
        Person.objects.all().delete()
        Course.objects.all().delete()


class TestAttendLecture(TestCase):
    def setUp(self):
        self.person1 = Person.objects.create(first_name="first_name1", last_name="last_name_1",
                                             external_id="id1",
                                             email="email1@gmail.com")
        self.person2 = Person.objects.create(first_name="first_name2", last_name="last_name_2",
                                             external_id="id2",
                                             email="email2@gmail.com")
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.person_course = PersonCourse.objects.create(person=self.person2, course=self.course, role="T")
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)

        self.qr_code_valid = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                                   valid_to=datetime.datetime(2020, 1, 1, 14, 59, 59))
        self.qr_code_invalid = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 3, 12, 14, 59),
                                                     valid_to=datetime.datetime(2018, 1, 3, 12, 59, 59))

        self.lecture1 = Lecture.objects.create(course=self.course, qr_code=self.qr_code_valid, type="L",
                                               classroom=self.classroom, person_limit=20)
        self.lecture2 = Lecture.objects.create(course=self.course, qr_code=self.qr_code_invalid, type="L",
                                               classroom=self.classroom, person_limit=20)
        self.allowed_radius = 5

    def test_attend_lecture_valid(self):
        return_data = attend_lecture(self.person1, self.qr_code_valid, self.allowed_radius, self.gps_coordinates)
        person_lecture = PersonLecture.objects.get(person=self.person1, lecture__qr_code=self.qr_code_valid)
        self.assertEqual(return_data.get("data"), PersonLectureSerializer(person_lecture).data)

    def test_attend_lecture_invalid(self):
        self.assertRaises(ValueError, attend_lecture, self.person1, self.qr_code_invalid,
                          self.allowed_radius, self.gps_coordinates)

    def test_teacher_attends_lecture(self):
        self.assertRaises(ValueError, attend_lecture, self.person2, self.qr_code_valid,
                          self.allowed_radius, self.gps_coordinates)

    def test_already_attends_lecture(self):
        self.assertEqual(False, already_attends_lecture(self.person1, self.qr_code_valid))

    def tearDown(self) -> None:
        PersonLecture.objects.all().delete()
        PersonCourse.objects.all().delete()
        Lecture.objects.all().delete()
        Classroom.objects.all().delete()
        OrgUnit.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        QrCode.objects.all().delete()
        Person.objects.all().delete()
        Course.objects.all().delete()


class TestStartLecture(TestCase):
    def setUp(self):
        self.teacher = Person.objects.create(first_name="first_name", last_name="last_name", external_id="id",
                                             email="email1@gmail.com")
        self.student = Person.objects.create(first_name="first_name_student", last_name="last_name_student",
                                             external_id="student_id", email="email1student@gmail.com")

        self.device_teacher = Device.objects.create(device_external_id='123')
        self.device_student = Device.objects.create(device_external_id='student_external_id')
        self.person_device_teacher = PersonDevice.objects.create(person=self.teacher,
                                                                 device=self.device_teacher,
                                                                 valid_from=timezone.now())
        self.person_device_student = PersonDevice.objects.create(person=self.student,
                                                                 device=self.device_student,
                                                                 valid_from=timezone.now())
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.person_course_teacher = PersonCourse.objects.create(person=self.teacher, course=self.course, role='T')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)
        self.course_1 = Course.objects.create(name='Kompjuterska vizija')
        self.person_course_1_teacher = PersonCourse.objects.create(person=self.teacher, course=self.course_1, role='T')
        self.time = timezone.now()
        self.qr_code = QrCode.objects.create(valid_from=self.time, valid_to=self.time + datetime.timedelta(minutes=100))
        self.lecture_started = Lecture.objects.create(course=self.course_1, qr_code=self.qr_code, type="L",
                                                      classroom=self.classroom, person_limit=20)

        self.roles = ("T", "A", "S")
        self.course_inexistent = Course(name='inexistent')
        self.lecture_data = {
            "course_id": self.course.pk,
            "type": "L",
            "classroom_id": self.classroom.pk,
            "person_limit": 76
        }

    def test_get_lecture_type_for_person_role(self):
        lecture_types_list = []
        for i in self.roles:
            lecture_types_list.append(get_lecture_type_for_person_role(i))
        self.assertEqual(("L", "E", ""), tuple(lecture_types_list))

    def test_generate_lecture_and_qr_code(self):
        generated_data = generate_lecture_and_qr_code(self.course, "L", self.classroom, 10, 76).get("data")
        lecture = Lecture.objects.get(qr_code=generated_data["qr_code"]["id"])
        self.assertEqual(LectureSerializer(lecture).data, generated_data)

    def test_lecture_is_already_started(self):
        self.assertEqual(lecture_already_started(self.course_1, "L"), True)

    def test_lecture_is_not_already_started(self):
        self.assertEqual(lecture_already_started(self.course_inexistent, "L"), False)

    def tearDown(self):
        PersonCourse.objects.all().delete()
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        PersonDevice.objects.all().delete()
        Device.objects.all().delete()
        Person.objects.all().delete()


class ManuallySetAttendanceType(TestCase):
    def setUp(self):
        self.person_1 = Person.objects.create(first_name="first_name_1", last_name="last_name_1",
                                             external_id="id_1",
                                             email="email_1@gmail.com")
        self.person_2 = Person.objects.create(first_name="first_name_2", last_name="last_name_2",
                                             external_id="id_2",
                                             email="email_2@gmail.com")
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)

        self.qr_code_valid = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                                   valid_to=datetime.datetime(2020, 1, 1, 14, 59, 59))

        self.lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code_valid, type="L",
                                              classroom=self.classroom, person_limit=20)

        self.person_course = PersonCourse.objects.create(role="S", course=self.course, person=self.person_1)
        self.person_course = PersonCourse.objects.create(role="S", course=self.course, person=self.person_2)
        self.person_1_lecture = PersonLecture.objects.create(attendance_type='QP',
                                                             created_at=datetime.datetime(2018, 1, 3, 12, 14, 59),
                                                             coordinates=self.gps_coordinates, lecture=self.lecture,
                                                             person=self.person_1)
        self.person_2_lecture = PersonLecture(attendance_type='AB',
                                              created_at=datetime.datetime(2018, 1, 3, 12, 14, 59),
                                              coordinates=self.gps_coordinates, lecture=self.lecture,
                                              person=self.person_2)

    def test_update_attendance_type_existing_record(self):
        person_lecture_manually_updated = manually_set_attendance_type(self.lecture, self.person_1, 'PR')
        self.assertEquals(person_lecture_manually_updated.get("data", "")["attendance_type"], 'PR')

    def test_update_attendance_type_and_save_to_db(self):
        manually_set_attendance_type(self.lecture, self.person_2, 'PR')
        person_lecture_from_db = PersonLecture.objects.filter(lecture=self.lecture, person=self.person_2).first()
        self.assertEquals(person_lecture_from_db.attendance_type, 'PR')

    def tearDown(self):
        PersonCourse.objects.all().delete()
        PersonLecture.objects.all().delete()
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        GpsCoordinate.objects.all().delete()
        Course.objects.all().delete()
        OrgUnit.objects.all().delete()
        Person.objects.all().delete()






