import datetime
from django.test import TestCase
from django.utils import timezone

from core.models import Person, Course, PersonCourse, OrgUnit, GpsCoordinate, Classroom, QrCode, Lecture, PersonLecture
from core.services.person_service import get_persons_by_course, get_students_by_lecture, get_or_create_person_by_name, \
    get_roles
from core.utils.constants import PLACEHOLDER_EXTERNAL_ID_SUFFIX, PLACEHOLDER_EMAIL_SUFFIX

"""
    Unit tests for person_service.
    Author: Emilija Zdilar 09-11-2018
"""


class TestGetPersonsByCourse(TestCase):

    def setUp(self):
        self.person1 = Person.objects.create(first_name="name1", last_name="lname1", external_id="index1",
                                             email="name1@test.com")
        self.person2 = Person.objects.create(first_name="name2", last_name="lname2", external_id="index2",
                                             email="name2@test.com")
        self.person3 = Person.objects.create(first_name="name3", last_name="lname3", external_id="neki_id",
                                             email="name3@test.com")
        self.course1 = Course.objects.create(name="course1")
        self.invalid_course = Course(name="course66")

        PersonCourse.objects.create(person=self.person1, course=self.course1, role="S")
        PersonCourse.objects.create(person=self.person2, course=self.course1, role="S")
        PersonCourse.objects.create(person=self.person3, course=self.course1, role="T")

        self.student_role = 'S'
        self.teacher_role = 'T'
        self.invalid_role = 'Y'

    def test_get_students_by_course(self):
        students_course1 = get_persons_by_course(self.course1, self.student_role)
        students_query_course1 = Person.objects.filter(
            personcourse__course__id=self.course1.pk, personcourse__role=self.student_role)
        self.assertCountEqual(students_query_course1, students_course1)

    def test_get_teachers_by_course(self):
        teachers_course1 = get_persons_by_course(self.course1, self.teacher_role)
        teachers_query_course1 = Person.objects.filter(
            personcourse__course__id=self.course1.pk, personcourse__role=self.teacher_role)
        self.assertCountEqual(teachers_query_course1, teachers_course1)

    def test_get_everyone_by_course(self):
        everyone_course1 = get_persons_by_course(self.course1)
        everyone_query_course1 = Person.objects.filter(personcourse__course__id=self.course1.pk)
        self.assertCountEqual(everyone_query_course1, everyone_course1)

    def test_invalid_role(self):
        self.assertRaises(ValueError)

    def tearDown(self) -> None:
        PersonCourse.objects.all().delete()
        Person.objects.all().delete()
        Course.objects.all().delete()


class TestGetStudentsByLecture(TestCase):
    maxDiff = None

    def setUp(self):
        self.person1 = Person.objects.create(first_name="name1", last_name="lname1", external_id="index1",
                                             email="name1@test.com")
        self.person2 = Person.objects.create(first_name="name2", last_name="lname2", external_id="index2",
                                             email="name2@test.com")
        self.person3 = Person.objects.create(first_name="name3", last_name="lname3", external_id="neki_id",
                                             email="name3@test.com")
        self.course = Course.objects.create(name='Softverski inzenjering')
        self.org_unit = OrgUnit.objects.create(name='PMF')
        self.gps_coordinates = GpsCoordinate.objects.create(lat=43.854028, lon=18.395167, alt=0)
        self.gps_coordinates_student = GpsCoordinate.objects.create(lat=43.854011, lon=18.395112, alt=0)
        self.classroom = Classroom.objects.create(name='428', coordinates=self.gps_coordinates,
                                                  org_unit=self.org_unit)

        self.qr_code = QrCode.objects.create(valid_from=datetime.datetime(2018, 1, 1, 12, 14, 59),
                                             valid_to=datetime.datetime(2018, 1, 1, 12, 59, 59))

        self.lecture = Lecture.objects.create(course=self.course, qr_code=self.qr_code, type="L",
                                              classroom=self.classroom, person_limit=20)

        self.person1_lecture = PersonLecture.objects.create(attendance_type='PR', created_at=timezone.now(),
                                                            coordinates=self.gps_coordinates_student,
                                                            lecture=self.lecture, person=self.person1
                                                            )

        self.person2_lecture = PersonLecture.objects.create(attendance_type='AB', created_at=timezone.now(),
                                                            coordinates=self.gps_coordinates_student,
                                                            lecture=self.lecture, person=self.person2
                                                            )

    def test_get_students_by_lecture(self):
        students_lecture1 = get_students_by_lecture(self.lecture)
        students_query_lecture1 = Person.objects.filter(personlecture__lecture=self.lecture)
        self.assertCountEqual(students_query_lecture1, students_lecture1)

    def tearDown(self) -> None:
        PersonLecture.objects.all().delete()
        Lecture.objects.all().delete()
        QrCode.objects.all().delete()
        Classroom.objects.all().delete()
        OrgUnit.objects.all().delete()
        Person.objects.all().delete()
        Course.objects.all().delete()


class TestGetOrCreatePersonByName(TestCase):

    def setUp(self):
        self.person1 = Person.objects.create(first_name="name1", last_name="lname1", external_id="index1",
                                             email="name1@test.com")

    def test_get_person_by_name(self):
        person = get_or_create_person_by_name('name1 lname1')
        person_query = Person.objects.filter(first_name='name1', last_name='lname1').first()

        self.assertEqual(person, person_query)

    def test_create_person_by_name(self):
        person_query = Person.objects.filter(first_name='name2', last_name='lname2').first()
        person = get_or_create_person_by_name('name2 lname2')

        self.assertEqual(person.first_name, 'name2')
        self.assertEqual(person.last_name, 'lname2')
        self.assertEqual(person.external_id, 'name2lname2{}'.format(PLACEHOLDER_EXTERNAL_ID_SUFFIX))
        self.assertEqual(person.email, 'name2lname2{}'.format(PLACEHOLDER_EMAIL_SUFFIX))
        self.assertIsNone(person_query)

    def tearDown(self) -> None:
        Person.objects.all().delete()


class TestGetPersonRoles(TestCase):

    def setUp(self):
        self.person1 = Person.objects.create(first_name="name1", last_name="lname1", external_id="index1",
                                             email="name1@test.com")
        self.person2 = Person.objects.create(first_name="name2", last_name="lname2", external_id="index2",
                                             email="name2@test.com")
        self.course = Course.objects.create(name='Softverski inzenjering')

        PersonCourse.objects.create(person=self.person1, course=self.course, role="S")
        PersonCourse.objects.create(person=self.person1, course=self.course, role="T")
        PersonCourse.objects.create(person=self.person2, course=self.course, role="S")

    def test_get_roles(self):
        person1_roles = get_roles(self.person1)
        person2_roles = get_roles(self.person2)

        self.assertEqual(len(person1_roles), 2)
        self.assertTrue("S" in person1_roles)
        self.assertTrue("T" in person1_roles)
        self.assertEqual(len(person2_roles), 1)
        self.assertTrue("S" in person2_roles)

    def tearDown(self) -> None:
        PersonCourse.objects.all().delete()
        Person.objects.all().delete()
        Course.objects.all().delete()

