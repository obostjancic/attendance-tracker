from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND
from rest_framework.views import APIView

from ..models import OrgUnit
from ..serializers import OrgUnitSerializer

from core.utils.constants import RESPONSE_ERROR, RESPONSE_ORG_UNIT_NOT_FOUND

"""
REST controller containing org_unit endpoint.
Author: Emilija Zdilar on 25-10-2018
"""


class OrgUnitCRUD(APIView):
    """
    Class based view. CRUD operations for org_unit table.
    """
    def get(self, req: Request, org_unit_id: int = None) -> Response:
        """
        GET request.
        Args:
            req: incoming request
            org_unit_id: Primary key in table org_unit, optional

        Returns:
            If primary key is specified, returns OrgUnit object with that
                primary key, if it exists. Otherwise, it returns  HTTP status 404.
            Else, returns a list of all OrgUnit objects.
        """
        if org_unit_id is not None:

            try:
                units = OrgUnit.objects.get(pk=org_unit_id)
                serializer = OrgUnitSerializer(units)

            except OrgUnit.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_ORG_UNIT_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            units = OrgUnit.objects.all()
            serializer = OrgUnitSerializer(units, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)
