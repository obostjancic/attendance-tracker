from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND
from rest_framework.views import APIView

from ..models import GpsCoordinate
from ..serializers import GpsCoordinateSerializer

from core.utils.constants import RESPONSE_ERROR, RESPONSE_GPS_NOT_FOUND

"""
REST controller containing gps_coordinate endpoint.
Author: Emilija Zdilar on 25-10-2018
"""


class GpsCoordinateCRUD(APIView):
    """"
    Class based view. CRUD operations for gps_coordinate table.
    """

    def get(self, req: Request, gps_coordinate_id: int = None) -> Response:
        """
        Args:
            req: incoming request
            gps_coordinate_id:  Primary key in table gps_coordinate, optional

        Returns:
            If primary key is specified, returns GpsCoordinate object with that
                primary key, if it exists. Otherwise, it returns  HTTP status 404.
            Else, returns a list of all GpsCoordinate objects.
        """
        if gps_coordinate_id is not None:

            try:
                coordinates = GpsCoordinate.objects.get(pk=gps_coordinate_id)
                serializer = GpsCoordinateSerializer(coordinates)

            except GpsCoordinate.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_GPS_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            coordinates = GpsCoordinate.objects.all()
            serializer = GpsCoordinateSerializer(coordinates, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)
