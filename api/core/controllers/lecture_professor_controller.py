import datetime

from django.db import DataError
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST, HTTP_201_CREATED,\
    HTTP_403_FORBIDDEN
from rest_framework.views import APIView
from core.services.course_service import get_course_role_for_person, get_course_for_qr_code
from core.services.lecture_service import generate_lecture_and_qr_code, \
    get_lecture_type_for_person_role, get_all_students_for_lecture, get_current_lecture, manually_set_attendance_type, \
    get_present_students_for_lecture
from core.services.mail_service import mail_qr_code
from core.utils.constants import CURRENT_OPEN_LECTURE_ERROR_MESSAGE, RESPONSE_ERROR, RESPONSE_MESSAGE_FORBIDDEN, \
    RESPONSE_CURRENT_LECTURE, RESPONSE_LECTURE_FORBIDDEN, RESPONSE_LECTURE_NOT_FOUND, RESPONSE_ERROR_MAIL_SENDING, \
    RESPONSE_MAIL_SENT_SUCCESSFULLY, ERROR_STUDENT_COURSE
from ..models import Lecture, QrCode, Course, Classroom, Person
from ..serializers import LectureSerializer, PersonLectureSimpleSerializer, \
    QrCodeSerializer

"""
REST controller containing lecture endpoint.
Author: Emilija Zdilar on 25-10-2018
        Lejla Dzananovic on 1-1-2019
"""


class StartLecture(APIView):

    def post(self, req: Request) -> Response:
        """
        POST request. Received should be an id of a course, classroom, duration of a qr_code and
        a person limit, optionally. (Custom authentication middleware) get_by_device identifies
        the person device is registered to. Firstly, get_course_role_for_person method is used to
        identify persons role in a course. Then get_lecture_type_for_person_role method is used
        to decide if the lecture is of Lecture or Exercise type. We look at three cases:
        1. Person is a teacher on a given course - Person role is "T" -> Lecture type is "L"
        2. Person is an assistant on a given course - Person role is "A" -> lecture type is "E"
        3. None of the above - Person is not authorized to perform the requested operation.
           Response with status 403 is returned.
        Then the get_current_lecture method is used to check if the lecture has already been
        started for that person. The way to do this is to check if there already exists a lecture
        belonging to a course that person is teaching, whose qr code is at that moment still valid.
        In that case, response with status 400 is returned.
        After that, generate_lecture_and_qr_code method is used to start a lecture. It generates
        a qr code valid for a given duration of time, and returns a dict that contains necessary
        data for lecture object creation. A Lecture object is created after successful validation.
        Args:
            req: incoming request

        Returns: Saved Lecture object with assigned id as JSON and status 200, if
                object was successfully created. status 400 or 403 otherwise.
        """
        try:
            # TODO make a serializer for this, it's cleaner
            course = Course.objects.get(id=req.data['course'])
            classroom = Classroom.objects.get(id=req.data['classroom'])
            if 'person_limit' in req.data:
                person_limit = req.data['person_limit']
            else:
                person_limit = None

            lecture_duration = req.data['duration']

            person_role = get_course_role_for_person(req.user, course)
            lecture_type = get_lecture_type_for_person_role(person_role)

            if not lecture_type:
                return Response(data={RESPONSE_ERROR: RESPONSE_MESSAGE_FORBIDDEN}, status=HTTP_403_FORBIDDEN)

            if get_current_lecture(req.user) is not None:
                return Response(data={RESPONSE_ERROR: RESPONSE_CURRENT_LECTURE}, status=HTTP_400_BAD_REQUEST)

            generated_lecture = generate_lecture_and_qr_code(course, lecture_type, classroom,
                                                             lecture_duration, person_limit)
            if generated_lecture.get("successful") is True:
                return Response(data=generated_lecture.get("data", ""), status=HTTP_201_CREATED)
            else:
                return Response(data=generated_lecture.get("errors", ""), status=HTTP_400_BAD_REQUEST)

        except(Course.DoesNotExist, Classroom.DoesNotExist,  ValueError, TypeError) as ex:
            return Response(data={RESPONSE_ERROR: str(ex)}, status=HTTP_400_BAD_REQUEST)


class LectureAttendanceList(APIView):

    def get(self, req: Request, lecture_id: int) -> Response:
        """
        GET request that, for a given lecture, returns the list of students together with
        their attendance status. Received should be an id of a lecture. Custom authentication
        middleware identifies the person device is belongs to. get_course_role_for_person
        method is used to check if a person has A / T role in a course that given lecture
        is a part of. In case the person is authorized, get_all_students_for_lecture service
        is used to get the list of students and their attendance statuse, including absent
        students. Otherwise, the request is denied.
        Args:
            req: incoming request
            lecture_id: id of a lecture for which we want the list of students

        Returns: Response with a list of PersonLecture objects as json and status 200 in case
        of success. Response with status 403 if a person is not authorized to perform the
        requested operation and 400 otherwise.

        """
        try:
            lecture = Lecture.objects.get(id=lecture_id)
            course_role = get_course_role_for_person(req.user, lecture.course)
            if course_role not in ['T', 'A']:
                return Response(data={RESPONSE_ERROR: RESPONSE_MESSAGE_FORBIDDEN}, status=HTTP_403_FORBIDDEN)
            else:
                list_of_students = get_all_students_for_lecture(lecture)
                serializer = PersonLectureSimpleSerializer(list_of_students, many=True)
                return Response(data=serializer.data, status=HTTP_200_OK)
        except(Lecture.DoesNotExist, ValueError, TypeError) as ex:
            return Response(data={RESPONSE_ERROR: str(ex)}, status=HTTP_400_BAD_REQUEST)


class LectureAttendeesList(APIView):

    def get(self, req: Request, lecture_id: int) -> Response:
        """
        GET request that, for a given lecture, returns the list of only present students.
        Received should be an id of a lecture. Custom authentication middleware identifies
        the person device is belongs to. get_course_role_for_person method is used to check
        if a person has A / T role in a course that given lecture is a part of. In case the
        person is authorized, get_present_students_for_lecture service is used to get the
        list of students and their attendance statuses, including absent students. Otherwise,
        the request is denied.
        Args:
            req: incoming request
            lecture_id: id of a lecture for which we want the list of students

        Returns: Response with a list of PersonLecture objects as json and status 200 in case
        of success. Response with status 403 if a person is not authorized to perform the
        requested operation and 400 otherwise.

        """
        try:
            lecture = Lecture.objects.get(id=lecture_id)
            course_role = get_course_role_for_person(req.user, lecture.course)
            if course_role not in ['T', 'A']:
                return Response(data={RESPONSE_ERROR: RESPONSE_MESSAGE_FORBIDDEN}, status=HTTP_403_FORBIDDEN)
            else:
                list_of_students = get_present_students_for_lecture(lecture)
                serializer = PersonLectureSimpleSerializer(list_of_students, many=True)
                return Response(data=serializer.data, status=HTTP_200_OK)
        except(Lecture.DoesNotExist, ValueError, TypeError) as ex:
            return Response(data={RESPONSE_ERROR: str(ex)}, status=HTTP_400_BAD_REQUEST)


class CurrentLecture(APIView):

    def get(self, req: Request)-> Response:
        """
        GET request. Custom authentication middleware identifies the person device
        is registered to. get_current_lecture method is used to return a Lecture
        object that represents current, still valid lecture, if there is such.

        Args:
            req: incoming request

        Returns: current lecture as JSON and status 200, if there exists a current
        lecture and status 404, otherwise.
        """
        lecture = get_current_lecture(req.user)
        if lecture is not None:
            serializer = LectureSerializer(lecture)
            return Response(data=serializer.data, status=HTTP_200_OK)
        else:
            return Response(data={RESPONSE_ERROR: CURRENT_OPEN_LECTURE_ERROR_MESSAGE}, status=HTTP_404_NOT_FOUND)


class CloseLecture(APIView):

    def put(self, req: Request, lecture_id: int) -> Response:
        """
        PUT request. Custom authentication middleware identifies the person device
        is registered to. get_current_lecture method is used to return a Lecture
        object that represents current, still valid lecture, if there is such. Otherwise
        request will be denied. If there exists one, we check that the received id matches
        the current lecture before closing it. To close the lecture means to change valid_to
        field of a qr_code generated for that lecture to current time.
        Args:
            req: incoming request
            lecture_id: id of a lecture to be closed

        Returns: closed lecture as JSON and status 200, in above explained case,
                 and status 400, otherwise.
        """
        try:
            lecture = Lecture.objects.get(pk=lecture_id)
            current_lecture = get_current_lecture(req.user)

            if current_lecture is None:
                return Response(data={RESPONSE_ERROR: CURRENT_OPEN_LECTURE_ERROR_MESSAGE}, status=HTTP_400_BAD_REQUEST)
            elif current_lecture.pk != lecture_id:
                return Response(data={RESPONSE_ERROR: RESPONSE_LECTURE_FORBIDDEN}, status=HTTP_400_BAD_REQUEST)

            serializer = QrCodeSerializer(lecture.qr_code, data={'valid_to': datetime.datetime.now()})
            if serializer.is_valid():
                serializer.save()
                return Response(data=LectureSerializer(lecture).data, status=HTTP_200_OK)
            else:
                return Response(data=LectureSerializer(lecture).errors, status=HTTP_400_BAD_REQUEST)
        except Lecture.DoesNotExist:
            return Response(data={RESPONSE_ERROR: RESPONSE_LECTURE_NOT_FOUND}, status=HTTP_400_BAD_REQUEST)


class MailQrCode(APIView):

    def post(self, req: Request) -> Response:
        """
        Post request. Received should be an ID of a qr code. Custom authentication middleware
        identifies the person device is belongs to. get_course_role_for_person method is used
        to check if a person has A/T role in a course for which a lecture  with received qr
        code has been started. In case the person is authorized, mail_qr_code service is used
        to send an email. Otherwise, the request is denied. An email consists  of an attached
        image of a QR code. The recipient address is the email teacher / assistant entered at
        registration.

        Args:
            req: incoming request

        Returns: Response with status 200 if an email has been sent successfully,
        response with status 403 if a person is not authorized to perform the
        requested operation and 400 otherwise.

        """
        try:
            qr_code = QrCode.objects.get(id=req.data['qr_code'])
            course = get_course_for_qr_code(qr_code)
            course_role = get_course_role_for_person(req.user, course)
            if course_role not in ['T', 'A']:
                return Response(data={RESPONSE_ERROR: RESPONSE_MESSAGE_FORBIDDEN}, status=HTTP_403_FORBIDDEN)
            else:
                result = mail_qr_code(qr_code, course, req.user)
                if result is 'Success':
                    return Response(data=RESPONSE_MAIL_SENT_SUCCESSFULLY, status=HTTP_200_OK)
                else:
                    return Response(data={RESPONSE_ERROR: RESPONSE_ERROR_MAIL_SENDING}, status=HTTP_400_BAD_REQUEST)
        except (QrCode.DoesNotExist, ValueError, TypeError) as ex:
            return Response(data={RESPONSE_ERROR: str(ex)}, status=HTTP_400_BAD_REQUEST)


class ManuallySetAttendance(APIView):

    def put(self, req: Request) -> Response:
        """
        Put request. Received should be an ID of a student and a lecture. Custom authentication middleware
        identifies the person device is belongs to. get_course_role_for_person method is used to check if
        the user is indeed a teacher for the course our lecture belongs to. If that is not the case, user
        is not authorized to perform the requested operation and response with status 403 is returned. Same
        method is also used to check if a student is enrolled in said course. In that case, response with
        status 400 is returned. manually_set_attendance_type method is used to create new PersonLecture
        record (PersonLecture object that originates from absence list whose attendance status has not been
        changes) or to update existing one (at one point student was present or questionably absent). Object
        is returned after successful validation along with status 200.
        Args:
            req: incoming request

        Returns: Response with status 200 if an attendance has been manually set successfully,
        response with status 403 if a person is not authorized to perform the
        requested operation and 400 otherwise.
        """
        try:
            lecture = Lecture.objects.get(id=req.data['lecture'])
            student = Person.objects.get(id=req.data['student'])
            new_attendance_status = req.data['attendance_type']

            user_course_role = get_course_role_for_person(req.user, lecture.course)
            if user_course_role not in ['T', 'A']:
                return Response(data={RESPONSE_ERROR: RESPONSE_MESSAGE_FORBIDDEN}, status=HTTP_403_FORBIDDEN)

            student_course_role = get_course_role_for_person(student, lecture.course)
            if student_course_role is not 'S':
                return Response(data={RESPONSE_ERROR: ERROR_STUDENT_COURSE}, status=HTTP_400_BAD_REQUEST)

            person_lecture = manually_set_attendance_type(lecture, student, new_attendance_status)

            if person_lecture.get("successful") is True:
                return Response(data=person_lecture.get("data", ""), status=HTTP_200_OK)
            else:
                return Response(data=person_lecture.get("errors", ""), status=HTTP_400_BAD_REQUEST)

        except (Person.DoesNotExist, Lecture.DoesNotExist, ValueError, TypeError, KeyError, DataError) as ex:
            return Response(data={RESPONSE_ERROR: str(ex)}, status=HTTP_400_BAD_REQUEST)

