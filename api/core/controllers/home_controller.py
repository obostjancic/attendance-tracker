"""
    REST controller containing home endpoint.
    Author: obostjancic 18-10-2018
"""

from django.http import HttpResponse, HttpRequest
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView


class HomeView(APIView):

    def get(self, req: HttpRequest) -> HttpResponse:
        """
        GET request - used as a home route for the application.

        Returns:
            HttpResponse with status code 200 and a welcome message.
        """
        welcome_msg = "Welcome to attendance tracker app"
        # lecture = Lecture()
        # lecture.qr_code = QrCode()
        # lecture.qr_code.id = "neki random id"
        # lecture.course = Course()
        # lecture.course.name = "Neki random predmet"
        # mail_qr_code(lecture, "ognjenTel")
        return HttpResponse(welcome_msg, status=HTTP_200_OK)
