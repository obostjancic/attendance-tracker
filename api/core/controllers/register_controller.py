from django.db import transaction, DatabaseError, IntegrityError
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from core.services.register_service import validate_and_create_user, validate_and_create_device, \
    update_current_person_device_records, user_and_device_already_connected
from ..models import Person, Device
from ..serializers import PersonDeviceSerializer

from core.utils.constants import RESPONSE_ERROR

"""
REST controller containing register endpoint.
Author: Emilija Zdilar on 29-10-2018
"""


class RegisterCRUD(APIView):
    """
    Class based View. - Registration
    """
    def post(self, req: Request) -> Response:
        """
        POST Request.
        We look at several cases:
            1. New user and new device:
                (e.g. new student)
                Person and Device object are saved after successful validation,
                Response with status 400 otherwise. New PersonDevice object is
                added, valid from the moment of creation.
            2. Existing user and new device:
                (e.g. student bought a new phone)
                Device object is saved after successful validation, Response with status 400 otherwise.
                New PersonDevice object is added, valid from the moment of creation. Existing PersonDevice
                record valid_to field is updated to current moment.
            3. Existing user and existing device:
                (e.q. phone is back from repair shop)
                If the device is still active, request is denied. Otherwise -
                New PersonDevice object is added, valid from the moment of creation. Existing PersonDevice
                record valid_to field is updated to current moment.
            4. New user and existing device:
                (e.g. new student got a phone from an older sibling student)
                Person object is saved after successful validation, Response with status 400 otherwise.
                New PersonDevice object is added, valid from the moment of creation. Existing PersonDevice
                record valid_to field is updated to current moment.

        Args:
            req: incoming request

        Returns: Saved PersonDevice object with assigned id as JSON and status 201, if
                 object was successfully created. status 400 otherwise

        """
        # TODO make a serializer for this, it's cleaner
        try:
            person_data = req.data['person']
            device_data = req.data['device']
            user_created = validate_and_create_user(person_data)

            if user_created.get("successful") is False:
                return Response(data=user_created.get("errors", ""), status=HTTP_400_BAD_REQUEST)

            device_created = validate_and_create_device(device_data)
            if device_created.get("successful") is False:
                return Response(data=device_created.get("errors", ""), status=HTTP_400_BAD_REQUEST)
            try:
                with transaction.atomic():
                    person_id = Person.objects.select_for_update().get(external_id=person_data['external_id']).id
                    device_id = Device.objects.select_for_update().get(device_external_id=device_data['device_external_id']).id

                    existing_person_device = user_and_device_already_connected(person_id, device_id)
                    if existing_person_device is not None:
                        return Response(data=PersonDeviceSerializer(existing_person_device).data, status=HTTP_201_CREATED)

                    update_current_person_device_records(person_id, device_id)

                    update_current_person_device_records(person_id, device_id)

                    reg_person_device = {
                        "person_id": person_id,
                        "device_id": device_id
                    }

                    serializer = PersonDeviceSerializer(data=reg_person_device)

                    if serializer.is_valid():
                        serializer.save()
                        return Response(data=serializer.data, status=HTTP_201_CREATED)

                return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)
            except (DatabaseError, IntegrityError) as ex:
                return Response(data={RESPONSE_ERROR: str(ex)}, status=HTTP_400_BAD_REQUEST)
        except Exception as ex:
            return Response(data={RESPONSE_ERROR: str(ex)}, status=HTTP_400_BAD_REQUEST)
