from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_201_CREATED, HTTP_400_BAD_REQUEST

from core.services.person_service import get_roles
from ..models import Person
from ..serializers import PersonSerializer
from core.services import person_service
from core.utils.constants import RESPONSE_ERROR, RESPONSE_PERSON_NOT_FOUND, ERROR_MISSING_DEVICE_ID

"""
REST controller containing person endpoint.
Author: Emilija Zdilar on 25-10-2018
"""


class PersonCRUD(APIView):
    """
    Class based view. CRUD operations for table person
    """

    def get(self, req: Request, person_id: int = None) -> Response:

        """
        GET request.

        Args:
            req: incoming request
            person_id: Primary key in table person, optional

        Returns: If primary key is specified, returns Person object with that
                    primary key, if it exists. Otherwise, it returns  HTTP status 404.
                 Else, returns list of all Person objects.

        """
        if person_id is not None:

            try:
                persons = Person.objects.get(pk=person_id)
                serializer = PersonSerializer(persons)

            except Person.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_PERSON_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            persons = Person.objects.all()
            serializer = PersonSerializer(persons, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)

    def post(self, req: Request) -> Response:
        """
        POST request.
        Args:
            req: incoming request

        Returns: Saved Person object with assigned id as JSON and status 201, if
                 object was successfully created. status 400 otherwise

        """

        serializer = PersonSerializer(data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=HTTP_201_CREATED)

        return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

    def put(self, req: Request, person_id: int) -> Response:
        """
        PUT request.
        Args:
            req: incoming request
            person_id: primary key in a person table

        Returns: Updated Person object as JSON and status 200, if object was
                 successfully updated. Code 400 otherwise.
        """
        try:
            person = Person.objects.get(pk=person_id)
            serializer = PersonSerializer(person, data=req.data)

            if serializer.is_valid():
                serializer.save()
                return Response(data=serializer.data, status=HTTP_200_OK)

            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)
        except Person.DoesNotExist:
            return Response(data=RESPONSE_PERSON_NOT_FOUND, status=HTTP_400_BAD_REQUEST)


class PersonDeviceCRUD(APIView):

    def get(self, req: Request, device_external_id: int) -> Response:
        """
        GET request.

        Args:
            req: incoming request
            device_external_id: android id

        Returns:
            Person that device with passed id belongs to.
        """
        if device_external_id is not None:

            person = person_service.get_by_device(device_external_id)
            serializer = PersonSerializer(person)
        else:
            return Response(data={RESPONSE_ERROR: ERROR_MISSING_DEVICE_ID}, status=HTTP_400_BAD_REQUEST)
        return Response(data=serializer.data, status=HTTP_200_OK)


class PersonInfo(APIView):
    """
    Class based view. Get request for basic info about a person
    """
    def get(self, req: Request) -> Response:
        """
        GET request.
        Args:
            req: Initial HTTP request that should contain Device-Id header

        Returns:
            HTTP response containing a dictionary with person object and a
            list of roles that this person has across all courses.
        """
        return Response(data={'person': PersonSerializer(req.user).data,
                              'roles': get_roles(req.user)}, status=HTTP_200_OK)
