from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from ..models import Device
from ..serializers import DeviceSerializer

from core.utils.constants import RESPONSE_ERROR, RESPONSE_DEVICE_NOT_FOUND
"""
REST controller containing device endpoint.
Author: Emilija Zdilar on 01-11-2018
"""


class DeviceCRUD(APIView):
    """
    Class based view. CRUD operations for table device
    """

    def get(self, req: Request, device_id: int = None) -> Response:

        """
        GET request.

        Args:
            req: incoming request
            device_id: Primary key in table device, optional

        Returns: If primary key is specified, returns Device object with that
                    primary key, if it exists. Otherwise, it returns  HTTP status 404.
                 Else, returns list of all Device objects.

        """
        if device_id is not None:

            try:
                devices = Device.objects.get(pk=device_id)
                serializer = DeviceSerializer(devices)

            except Device.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_DEVICE_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            devices = Device.objects.all()
            serializer = DeviceSerializer(devices, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)

    def post(self, req: Request) -> Response:
        """
        POST request.
        Args:
            req: incoming request

        Returns: Saved Device object with assigned id as JSON and status 201, if
                 object was successfully created. status 400 otherwise

        """

        serializer = DeviceSerializer(data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=HTTP_201_CREATED)

        return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

    def put(self, req: Request, device_id: int) -> Response:
        """
        PUT request.
        Args:
            req: incoming request
            device_id: primary key in a device table

        Returns: Updated Device object as JSON and status 200, if object was
                 successfully updated. Code 400 otherwise.
        """
        try:
            device = Device.objects.get(pk=device_id)
            serializer = DeviceSerializer(device, data=req.data)

            if serializer.is_valid():
                serializer.save()
                return Response(data=serializer.data, status=HTTP_200_OK)

            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)
        except Device.DoesNotExist:
            return Response(data=RESPONSE_DEVICE_NOT_FOUND, status=HTTP_400_BAD_REQUEST)
