from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND
from rest_framework.views import APIView

from core.services.classroom_service import get_by_org_unit
from core.utils.constants import RESPONSE_ERROR, RESPONSE_CLASSROOM_NOT_FOUND
from ..models import Classroom, OrgUnit
from ..serializers import ClassroomSerializer

"""
REST controller containing classroom endpoint.
Author: Emilija Zdilar on 25-10-2018
"""


class ClassroomCRUD (APIView):
    """
    Class based view. CRUD operations for classroom table.
    """

    def get(self, req: Request, classroom_id: int = None) -> Response:
        """
        GET request.
        Args:
            req: incoming request
            classroom_id: primary key in table classroom, optional

        Returns: If primary key is specified, returns Classroom object with that
                    primary key, if it exists. Otherwise, it returns  HTTP status 404.
                 Else, returns a list of all Classroom objects.
        """
        if classroom_id is not None:

            try:
                classrooms = Classroom.objects.get(pk=classroom_id)
                serializer = ClassroomSerializer(classrooms)
            except Classroom.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_CLASSROOM_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            if 'org_unit' in req.query_params:
                org_unit = OrgUnit.objects.get(pk=req.query_params.get('org_unit'))
                classrooms = get_by_org_unit(org_unit)
                serializer = ClassroomSerializer(classrooms, many=True)
            else:
                classrooms = Classroom.objects.all()
                serializer = ClassroomSerializer(classrooms, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)
