import pandas
from django.contrib import messages
from django.shortcuts import render

from core.services.importer_service import import_xlsx_data


def xlsx_import(request):
    if request.method == 'POST' and request.FILES['selected_file']:
        try:
            uploaded_file = request.FILES['selected_file']
            import_xlsx_data(pandas.ExcelFile(uploaded_file))
            messages.add_message(request, messages.SUCCESS, 'File successfully imported!')
        except:
            messages.add_message(request, messages.ERROR, 'There has been an error!')

    return render(request, 'importer.html')
