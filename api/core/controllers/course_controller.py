from typing import List

from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST,\
    HTTP_204_NO_CONTENT, HTTP_201_CREATED

from core.services.course_service import get_student_attendance_percentage, get_by_person_role, \
    course_attendance_statistics
from ..models import Course
from ..serializers import CourseSerializer

from core.utils.constants import RESPONSE_ERROR, RESPONSE_COURSE_NOT_FOUND, RESPONSE_DELETED_COURSE, ERROR_MISSING_ROLE, \
    ERROR_MISSING_COURSE_ID

"""
    Created by MelihaK on 1/11/2018.
    REST controller containing course endpoint.
"""


class CourseCRUD(APIView):
    """"
    Class based view. CRUD operations for course table.
    """

    def get(self, req: Request, course_id: int = None) -> Response:
        """
        GET request.
        Args:
            req: incoming request
            course_id: Primary key in table course, optional

        Returns: If primary key is specified, returns Course object with that
                 primary key, if it exists. Otherwise, it returns HTTP status 404.
                 Else, returns a list of all Course objects.

        """
        if course_id is not None:
            try:
                course = Course.objects.get(pk=course_id)
                serializer = CourseSerializer(course)

            except Course.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_COURSE_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            courses = Course.objects.all()
            serializer = CourseSerializer(courses, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)

    def post(self, req: Request) -> Response:
        """
        POST request.
        Args:
            req: incoming request

        Returns: saved Course object with assigned id as JSON and status 201, if
                 object was successfully created; status 400 otherwise

        """

        serializer = CourseSerializer(data=req.data)

        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=HTTP_201_CREATED)

        return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

    def put(self, req: Request, course_id: int) -> Response:
        """
        PUT request.
        Args:
            req: incoming request
            course_id: primary key in a course table

        Returns: updated Course object as JSON and status 200, if object was
                 successfully updated; code 400 otherwise
        """
        try:
            course = Course.objects.get(pk=course_id)
            serializer = CourseSerializer(course, data=req.data)

            if serializer.is_valid():
                serializer.save()
                return Response(data=serializer.data, status=HTTP_200_OK)

            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)
        except Course.DoesNotExist:
            return Response(data=RESPONSE_COURSE_NOT_FOUND, status=HTTP_400_BAD_REQUEST)

    def delete(self, req: Request, course_id: int)->Response:
        """
        DELETE request.
        Args:
            req: incoming request
            course_id: primary key in course table

        Returns: status 204 if the object with passed primary key
                 was deleted successfully, status 404 otherwise
        """

        course = Course.objects.filter(pk=course_id).first()

        if course is not None:
            course.delete()
            return Response(data=RESPONSE_DELETED_COURSE, status=HTTP_204_NO_CONTENT)
        else:
            return Response(data={RESPONSE_ERROR: RESPONSE_COURSE_NOT_FOUND}, status=HTTP_404_NOT_FOUND)


class GetCoursesByPersonAndRole(APIView):
    """"
    Class based view. CRUD operations for course table.
    """

    def get(self, request: Request, role: str) -> Response:
        """

        Args:
            request: incoming request
            role: Person role on a course

        Returns:

        """
        if role is not None:
            courses = get_by_person_role(request.user, role)
            serializer = CourseSerializer(courses, many=True)

            return Response(data=serializer.data, status=HTTP_200_OK)

        else:
            return Response(data={RESPONSE_ERROR: ERROR_MISSING_ROLE}, status=HTTP_400_BAD_REQUEST)

            
class StudentCourseAttendance(APIView):
    """"
    Class based view - student courses attendance info.
    """

    def get(self, req: Request, lecture_type: str) -> Response:
        """
        GET request.
        Args:
            req: incoming request
            lecture_type: lecture or exercise flag

        Returns: attendance percentage info for student courses
        """
        student = req.user
        attendance_percentage = get_student_attendance_percentage(student, lecture_type)

        return Response(data=attendance_percentage, status=HTTP_200_OK)


class CourseAttendanceStatistics(APIView):

    """
    Class based view - course attendance statistics info.
    """

    def get(self, req: Request, course_id: int, lecture_type: str) -> Response:

        """
        GET request.
        Args:
            req: incoming request
            course_id: course for which attendance statistics is calculated
            lecture_type: lecture or exercise flag

        Returns: course attendance statistics info (format:
        student name, number of lectures student attended / number of lectures for that course, attendance percentage)

        """

        if course_id is not None:
            try:
                course = Course.objects.get(pk=course_id)
                attendance_statistics = course_attendance_statistics(course, lecture_type)

                return Response(data=attendance_statistics, status=HTTP_200_OK)

            except Course.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_COURSE_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            return Response(data={RESPONSE_ERROR: ERROR_MISSING_COURSE_ID}, status=HTTP_400_BAD_REQUEST)
