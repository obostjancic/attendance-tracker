
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK,  HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from core.services.lecture_service import attend_lecture, already_attends_lecture
from ..models import QrCode, GpsCoordinate
from core.utils.constants import RESPONSE_ERROR, ALREADY_ATTENDS_LECTURE
from ..serializers import PersonLectureSerializer

"""
REST controller containing lecture endpoint.
Author: Emilija Zdilar on 25-10-2018
        Lejla Dzananovic on 1-1-2019
"""


class AttendLecture(APIView):

    def post(self, req: Request) -> Response:
        """
            POST request.
            Received should be an id of a QR code and of a device. First, get_by_device
            method is used to identify the owner of a given device.
            We look at two cases:
                1. Student attends the lecture for the first time (not yet enrolled in a course).
                2. Student has already attended at least one of the previous lectures from the same
                   course.
            attend_lecture method is used to enroll the student, if needed, and to add return person lecture data
            A PersonLecture object is created after successful validation.

            Args:
                req: incoming request
            Returns:
                Saved PersonLecture object with assigned id as JSON and status 200, if
                object was successfully created. status 400 otherwise.
        """
        try:
            # TODO make a serializer for this, it's cleaner
            qr_code = QrCode.objects.get(id=req.data['qr_code'])
            allowed_radius = 5

            if 'location' in req.data:
                student_location = GpsCoordinate.objects.create(lat=req.data['location']['latitude'],
                                                                lon=req.data['location']['longitude'], alt=531)
            else:
                return Response(status=HTTP_400_BAD_REQUEST)

            if already_attends_lecture(req.user, qr_code) is True:
                return Response(data={RESPONSE_ERROR: ALREADY_ATTENDS_LECTURE}, status=HTTP_400_BAD_REQUEST)

            attend_lecture_details = attend_lecture(req.user, qr_code, allowed_radius, student_location)

            if attend_lecture_details.get("successful") is True:
                return Response(data=attend_lecture_details.get("data", ""), status=HTTP_200_OK)
            else:
                return Response(data=attend_lecture_details.get("errors", ""), status=HTTP_400_BAD_REQUEST)

        except (QrCode.DoesNotExist, ValueError, TypeError) as ex:
            return Response(data={RESPONSE_ERROR: str(ex)}, status=HTTP_400_BAD_REQUEST)
