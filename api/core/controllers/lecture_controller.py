from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST, \
    HTTP_204_NO_CONTENT, HTTP_201_CREATED
from rest_framework.views import APIView

from core.services.course_service import get_course_role_for_person
from core.services.lecture_service import get_by_course, get_by_course_and_type, get_lecture_type_for_person_role
from ..models import Lecture,  Course
from ..serializers import LectureSerializer

from core.utils.constants import RESPONSE_ERROR, RESPONSE_LECTURE_NOT_FOUND, RESPONSE_COURSE_NOT_FOUND,\
    RESPONSE_DELETED_LECTURE, ERROR_MISSING_COURSE_ID
"""
REST controller containing lecture endpoint.
Author: Emilija Zdilar on 25-10-2018
"""


class LectureCRUD(APIView):
    """
    class based view. CRUD operations for table lecture.
    """

    def get(self, req: Request, lecture_id: int = None) -> Response:
        """
        GET request.
        Args:
            req: incoming request
            lecture_id: primary key in lecture table, optional

        Returns: If primary key is specified, returns Lecture object with that
                    primary key, if it exists. Otherwise, it returns  HTTP status 404.
                 Else, returns a list of all Lecture objects.
        """
        if lecture_id is not None:
            try:
                lectures = Lecture.objects.get(pk=lecture_id)
                serializer = LectureSerializer(lectures)
            except Lecture.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_LECTURE_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            lectures = Lecture.objects.all()
            serializer = LectureSerializer(lectures, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)

    def post(self, req: Request) -> Response:
        """
        POST request.
        Args:
            req: incoming request

        Returns: Saved Lecture object with assigned id as JSON and status 201, if
                 object was successfully created. status 400 otherwise

        """
        serializer = LectureSerializer(data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=HTTP_201_CREATED)
        return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

    def put(self, req: Request, lecture_id: int) -> Response:
        """
        PUT request.
        Args:
            req: incoming request
            lecture_id: primary key in a lecture table

        Returns: Updated Lecture object as JSON and status 200, if object was
                 successfully updated. Code 400 otherwise.
        """

        lecture = Lecture.objects.get(pk=lecture_id)

        serializer = LectureSerializer(lecture, data=req.data)

        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=HTTP_200_OK)

        return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

    def delete(self, req: Request, lecture_id: int)-> Response:
        """
        DELETE request.
        Args:
            req: incoming request
            lecture_id: primary key in lecture table

        Returns: status 204 if the object with passed Primary key
        was deleted successful, status 404 otherwise
        """

        lecture = Lecture.objects.filter(pk=lecture_id).first()

        if lecture is not None:
            lecture.delete()
            return Response(data=RESPONSE_DELETED_LECTURE, status=HTTP_204_NO_CONTENT)
        else:
            return Response(data={RESPONSE_ERROR: RESPONSE_LECTURE_NOT_FOUND}, status=HTTP_404_NOT_FOUND)


class LecturesByCourse(APIView):

    def get(self, req: Request, course_id: int) -> Response:

        if course_id is not None:

            try:
                course = Course.objects.get(pk=course_id)

                person_role = get_course_role_for_person(req.user, course)
                lecture_type = get_lecture_type_for_person_role(person_role)

                lectures = get_by_course_and_type(course, lecture_type)
                serializer = LectureSerializer(lectures, many=True)

            except Course.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_COURSE_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            return Response(data={RESPONSE_ERROR: ERROR_MISSING_COURSE_ID}, status=HTTP_400_BAD_REQUEST)

        return Response(data=serializer.data, status=HTTP_200_OK)


