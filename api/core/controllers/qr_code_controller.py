from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND
from rest_framework.views import APIView

from ..models import QrCode
from ..serializers import QrCodeSerializer

from core.utils.constants import RESPONSE_ERROR, RESPONSE_QR_CODE_NOT_FOUND

"""
REST controller containing qr_code endpoint.
Author: Emilija Zdilar on 25-10-2018
"""


class QrCodeCRUD(APIView):
    """"
    Class based view. CRUD operations for qr_code table.
    """

    def get(self, req: Request, qr_code_id: int = None) -> Response:
        """
        GET request.
        Args:
            req: incoming request
            qr_code_id: Primary key in table qr_code, optional

        Returns: If primary key is specified, returns QrCode object with that
                    primary key, if it exists. Otherwise, it returns  HTTP status 404.
                 Else, returns a list of all QrCode objects.

        """
        if qr_code_id is not None:

            try:
                qr_codes = QrCode.objects.get(pk=qr_code_id)
                serializer = QrCodeSerializer(qr_codes)

            except QrCode.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_QR_CODE_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            qr_codes = QrCode.objects.all()
            serializer = QrCodeSerializer(qr_codes, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)
