from django.http import HttpResponse
from django.views.generic import View
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST

from core.models import Course, Person
from core.services.reporting_service import get_attendance_by_course_report, get_student_attendance_percentage_report, \
    get_attendants_for_lectures_report
from core.utils.constants import I18N_ATTENDANCE_REPORT_FILENAME_PREFIX_BS, XLSX_FORMAT, XLSX_CONTENT_TYPE, \
    I18N_ATTENDANTS_REPORT_FILENAME_PREFIX_BS, RESPONSE_ERROR, RESPONSE_COURSE_NOT_FOUND, ERROR_MISSING_COURSE_ID

"""
    REST controller containing reporting service endpoints.
    Author: melihak 04-12-2018
"""


class AttendanceByCourseView(View):
    """
        Class based View - lectures attendance info.
    """

    def get(self, req: Request, course_id: int, lecture_type: str) -> HttpResponse:
        """
        Args:
            req: incoming request
            course_id: primary key in course table
            lecture_type: lecture or exercise flag

        Returns:
            xlsx file with attendance info for lectures (for course with passed id)
        """
        if course_id is not None:
            try:
                course = Course.objects.get(pk=course_id)
                output = get_attendance_by_course_report(course, lecture_type)
                # TODO make a serializer for this, it's cleaner
                filename = "{}_{}{}".format(I18N_ATTENDANCE_REPORT_FILENAME_PREFIX_BS,
                                            course.name.replace(" ", "_"), XLSX_FORMAT)
                response = HttpResponse(output, content_type=XLSX_CONTENT_TYPE)
                response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)

                return response

            except Course.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_COURSE_NOT_FOUND},status=HTTP_404_NOT_FOUND)

        else:
            return Response(status=HTTP_400_BAD_REQUEST)


class AttendancePercentageByCourseView(View):
    """
        Class based View - courses attendance info.
    """

    def get(self, req: Request, lecture_type: str) -> HttpResponse:
        """
        Args:
            req: incoming request
            lecture_type: lecture or exercise flag

        Returns:
            xlsx file with attendance percentage info for student courses
        """

        person = req.user
        output = get_student_attendance_percentage_report(person, lecture_type)

        filename = "{}_{}{}".format(I18N_ATTENDANCE_REPORT_FILENAME_PREFIX_BS,
                                    '{}_{}'.format(person.first_name, person.last_name), XLSX_FORMAT)
        response = HttpResponse(output, content_type=XLSX_CONTENT_TYPE)
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)

        return response


class AttendantsByLecturesView(View):
    """
        Class based View - attendants by lectures.
    """

    def get(self, req: Request, course_id: int, lecture_type: str) -> HttpResponse:
        """
        Args:
            req: incoming request
            course_id: course for which lectures attendants are returned
            lecture_type: lecture or exercise flag

        Returns: xlsx file with attendants by lectures info
        """

        if course_id is not None:
            try:
                course = Course.objects.get(pk=course_id)
                output = get_attendants_for_lectures_report(course, lecture_type)

                filename = "{}_{}{}".format(I18N_ATTENDANTS_REPORT_FILENAME_PREFIX_BS,
                                            course.name.replace(" ", "_"), XLSX_FORMAT)
                response = HttpResponse(output, content_type=XLSX_CONTENT_TYPE)
                response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)

                return response

            except Course.DoesNotExist:
                return Response(data={RESPONSE_ERROR: RESPONSE_COURSE_NOT_FOUND}, status=HTTP_404_NOT_FOUND)

        else:
            return Response(data={RESPONSE_ERROR: ERROR_MISSING_COURSE_ID}, status=HTTP_400_BAD_REQUEST)
