from django.urls import path

from core.controllers.classroom_controller import ClassroomCRUD
from core.controllers.course_controller import CourseCRUD, GetCoursesByPersonAndRole, CourseAttendanceStatistics
from core.controllers.course_controller import StudentCourseAttendance
from core.controllers.gps_coordinate_controller import GpsCoordinateCRUD
from core.controllers.lecture_controller import LectureCRUD, LecturesByCourse
from core.controllers.lecture_student_controller import AttendLecture
from core.controllers.lecture_professor_controller import StartLecture, LectureAttendanceList, CurrentLecture, \
    CloseLecture, \
    MailQrCode, ManuallySetAttendance, LectureAttendeesList
from core.controllers.org_unit_controller import OrgUnitCRUD
from core.controllers.person_controller import PersonCRUD, PersonDeviceCRUD
from core.controllers.person_controller import PersonInfo
from core.controllers.qr_code_controller import QrCodeCRUD
from core.controllers.register_controller import RegisterCRUD
from core.controllers.reporting_controller import AttendanceByCourseView, AttendancePercentageByCourseView,\
    AttendantsByLecturesView

"""
Defines URLs of exposed REST endpoints.
"""
app_name = "core"

urlpatterns = [

    path('lecture/<int:lecture_id>/', LectureCRUD.as_view(), name='get_single_lecture'),
    path('lecture/<int:lecture_id>/', LectureCRUD.as_view(), name='put_lecture'),
    path('lecture/<int:lecture_id>/', LectureCRUD.as_view(), name='delete_lecture'),

    path('lecture/', LectureCRUD.as_view(), name='get_all_lectures'),
    path('lecture/', LectureCRUD.as_view(), name='post_lecture'),
    path('lecture/attend/', AttendLecture.as_view(), name='attend_lecture'),
    path('lecture/start/', StartLecture.as_view(), name='start_lecture'),
    path('lecture/course/<int:course_id>/', LecturesByCourse.as_view(), name='get_lectures_for_course'),
    path('lecture/students/<int:lecture_id>/', LectureAttendanceList.as_view(), name='lecture_attendance_list'),
    path('lecture/attendees/<int:lecture_id>/', LectureAttendeesList.as_view(), name='lecture_attendees_list'),
    path('lecture/current/', CurrentLecture.as_view(), name='get_current_lecture'),
    path('lecture/close/<int:lecture_id>/', CloseLecture.as_view(), name='close_lecture'),

    path('person/', PersonCRUD.as_view(), name='get_all_persons'),
    path('person/', PersonCRUD.as_view(), name='post_person'),
    path('person/info/', PersonInfo.as_view(), name='person_info'),

    path('person/<int:person_id>/', PersonCRUD.as_view(), name='get_single_person'),
    path('person-device/<str:device_external_id>/', PersonDeviceCRUD.as_view(), name='get_person_for_device'),

    path('classroom/', ClassroomCRUD.as_view(), name='get_all_classrooms'),
    path('classroom/<int:classroom_id>/', ClassroomCRUD.as_view(), name='get_single_classroom'),

    path('qr_code/', QrCodeCRUD.as_view(), name='get_all_qr_codes'),
    path('qr_code/mail/', MailQrCode.as_view(), name='send_qr_code_to_mail'),

    path('gps_coordinate/', GpsCoordinateCRUD.as_view(), name='get_all_gps_coordinates'),
    path('org_unit/', OrgUnitCRUD.as_view(), name='get_all_org_units'),

    path('course/', CourseCRUD.as_view(), name='get_all_courses'),
    path('course/', CourseCRUD.as_view(), name='post_course'),
    path('course/<int:course_id>/', CourseCRUD.as_view(), name='get_single_course'),
    path('course/<int:course_id>/', CourseCRUD.as_view(), name='put_course'),
    path('course/<int:course_id>/', CourseCRUD.as_view(), name='delete_course'),
    path('course/<str:role>/', GetCoursesByPersonAndRole.as_view(), name='get_courses_by_role'),
    path('course/attendance/statistics/<int:course_id>/<str:lecture_type>/', CourseAttendanceStatistics.as_view(),
         name='attendance_statistics'),
    path('course/attendance/percentage/<str:lecture_type>/', StudentCourseAttendance.as_view(),
         name='course-attendance-percentage'),
    path('register/', RegisterCRUD.as_view(), name='register'),
    path('report/attendance/course/<int:course_id>/<str:lecture_type>/', AttendanceByCourseView.as_view(),
         name='attendance_by_course'),
    path('report/attendance/percentage/<str:lecture_type>/', AttendancePercentageByCourseView.as_view(),
         name='get_attendance_percentage_report'),
    path('report/attendants/lectures/<int:course_id>/<str:lecture_type>/', AttendantsByLecturesView.as_view(),
         name='attendants_by_lectures'),
    path('lecture/student/attendance/', ManuallySetAttendance.as_view(),
         name='manually_set_attendance'),
]
