"""
    Service that contains all methods relevant to person object
    Author: obostjancic 21-10-2018
    Author: Emilija Zdilar 09-11-2018
"""
import re
from datetime import datetime
from typing import List

from core.models import Person, PersonDevice, Course, Lecture, PersonLecture, PersonCourse
from core.utils.constants import PLACEHOLDER_EXTERNAL_ID_SUFFIX, PLACEHOLDER_EMAIL_SUFFIX, ROLE_TYPES


def get_by_device(device_id: str) -> Person:
    """
    Identifies person object from passed device id, thus returning the person
    that device with this id currently belong to.
    Args:
        device_id: Unique external id of the device

    Returns:
        Person that currently uses this device.
    """

    person_device = PersonDevice.objects.filter(device__device_external_id=device_id,
                                                valid_from__lte=datetime.now(),
                                                valid_to__isnull=True).first()

    if person_device is not None:
        return person_device.person
    else:
        raise ValueError("This device id is not tied to any person in the system!")


def get_or_create_by_name(first_name: str, last_name: str):
    """
    Finds and returns a person if it exists in the database. Matches by first name and
    last name. Inserts a new person and returns it if it does not exist.
    Args:
        first_name: Person's first name
        last_name: Person's last name

    Returns:
        Persisted person object with specified first and last name
    """

    person = Person.objects.filter(first_name=first_name, last_name=last_name).first()

    if person is not None:
        return person

    return Person.objects.create(first_name=first_name, last_name=last_name, email='', external_id=first_name.lower())


def get_persons_by_course(course: Course, role: str = None) -> List[Person]:
    """
    Method that takes two parameters:  a course and an optional role parameter.
    Method returns the list of people related to that course as per preference
    - teachers, students, assistants,or unspecified. Steps wee need to take are:
        1. check that the given Course exists
        2. check that the role parameter, if specified, is valid
        3. Return the list of students
    Args:
        course: course object
        role: 'S' - Student 'T' - teacher

    Returns: list of students enrolled into specified course
    """

    if role is not None:
        if role not in ROLE_TYPES:
            raise ValueError("Invalid role type. Expected one of: %s" % ROLE_TYPES)
        else:
            list_of_persons = Person.objects.filter(personcourse__course=course,
                                                    personcourse__role=role)
    else:
        list_of_persons = Person.objects.filter(personcourse__course=course)

    return list_of_persons


def get_attendances_by_lecture(lecture: Lecture) -> List[PersonLecture]:
    """
    A service that takes a lecture and returns all students
    along with their attendance statuses
    Args:
        lecture: lecture object, specified lecture

    Returns: list of persons with their attendance status

    """
    return PersonLecture.objects.filter(lecture__id=lecture.pk)


def get_attendees_by_lecture(lecture: Lecture) -> List[PersonLecture]:
    """
    A service that takes a lecture and returns present students
    Args:
        lecture: lecture object, specified lecture

    Returns: list of persons with PR status

    """
    return PersonLecture.objects.filter(lecture__id=lecture.pk, attendance_type='PR')


def get_students_by_lecture(lecture: Lecture) -> List[Person]:
    """
    A service that takes a lecture and returns all students
    along with their attendance statuses
    Args:
        lecture: lecture object, specified lecture

    Returns: list of persons with their attendance status

    """
    return Person.objects.filter(personlecture__lecture=lecture)


def get_or_create_person_by_name(full_name: str) -> Person:
    """
    NOTE: THIS IS ONLY FOR IMPORTER SERVICE. PLEASE DO NOT USE IT ANYWHERE ELSE!

    Takes the full name of a person, splits it into first name and last name, then checks the database
    if a person with this name already exists or not. If the person exists, retrieves the person form the database.
    Else creates a person with passed full name and placeholder email and external id that are defined as constants.
    Args:
        full_name: String consisting of a first and last name of a person. Separated by whitespace.

    Returns:
        Persisted person object.
    """
    first_name, last_name = full_name.split(' ', 1)

    person = Person.objects.filter(first_name=first_name, last_name=last_name).first()

    if person is None:
        formatted_full_name = re.sub(r'\W+', '', full_name).lower()

        placeholder_external_id = formatted_full_name + PLACEHOLDER_EXTERNAL_ID_SUFFIX
        placeholder_email = formatted_full_name + PLACEHOLDER_EMAIL_SUFFIX

        return Person.objects.create(first_name=first_name, last_name=last_name,
                                     external_id=placeholder_external_id, email=placeholder_email)

    return person


def get_roles(person: Person) -> List[str]:
    """
    Gets the list of roles passed person has across all courses it has links to.
    Args:
        person: Person object for which we want the list of roles

    Returns:
        List containing roles for this person (as strings)
    """
    person_courses = PersonCourse.objects.filter(person=person)
    roles = []

    for person_course in person_courses:
        if person_course.role not in roles:
            roles.append(person_course.role)

    return roles
