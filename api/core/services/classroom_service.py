"""
Service that contains all methods relevant to classroom object.
Author: Emilija Zdilar 18.12.2018.
"""
from typing import List

from core.models import OrgUnit, Classroom


def get_by_org_unit(org_unit: OrgUnit) -> List[Classroom]:
    """
    Helper method that returns all classrooms for a given organisational unit.
    Args:
        org_unit: organisational unit, e.g. PMF

    Returns: List of all classrooms that belong to that unit

    """
    return Classroom.objects.filter(org_unit=org_unit)
