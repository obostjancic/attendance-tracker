from typing import Tuple, List

import pandas

from core.services.person_service import get_or_create_person_by_name
from core.services.course_service import get_or_create_course_by_name, link_person_to_course
from core.utils.constants import DOC_TITLE, ASSISTANT, TEACHER


def import_xlsx_data(xlsx_file: pandas.ExcelFile) -> None:
    """
    Takes an excel file, reads it and parses course names and teacher names from it. Gets or creates these objects
    form the database and links them accordingly
    Args:
        xlsx_file: Excel file that contains links between teachers and courses that they teach

    """
    if xlsx_file is None:
        return

    dataframe = pandas.read_excel(xlsx_file)

    idx = 0
    while idx < len(dataframe):
        row = dataframe.iloc[idx]
        if str(row[0]).isnumeric():

            teacher = get_or_create_person_by_name(get_teacher_name(row))
            teacher_role = get_role(row)
            course_names, idx = get_courses_names(dataframe, idx)

            for course_name in course_names:
                course = get_or_create_course_by_name(course_name)
                link_person_to_course(teacher, course, teacher_role)

            idx -= 1
        idx += 1


def get_teacher_name(row) -> str:
    """
    From a row in file returns a name of the teacher if it is there
    Args:
        row: row in a pandas dataframe (Excel file)

    Returns:
        Name of the teacher
    """
    try:
        comma = row[1].index(',')
        return row[1][:comma]
    except ValueError:
        try:
            newline = row[1].index('\n')
            return row[1][:newline]
        except ValueError:
            return row[1]


def get_role(row) -> str:
    """
        From a row in file returns the role of the teacher based on his/hers academic title
        Args:
            row: row in a pandas dataframe (Excel file)

        Returns:
            Role of the teacher
    """
    title = str(row[2])[:6]

    if title == DOC_TITLE:
        return TEACHER
    else:
        return ASSISTANT


def get_courses_names(dataframe, row_idx) -> Tuple[List[str], int]:
    """
    From pandas dataframe extracts courses names and moves the row index to
    Args:
        dataframe: ExcelFile
        row_idx: index of current row

    Returns:
        list of courses and an updated row index.
    """

    courses = []
    row = dataframe.iloc[row_idx]
    course_name = row[5].strip()
    courses.append(course_name)
    row_idx += 1
    row = dataframe.iloc[row_idx]

    while not str(row[0]).isnumeric() and len(str(row[5])) > 3:
        course_name = row[5].strip()
        courses.append(course_name)
        row_idx += 1
        row = dataframe.iloc[row_idx]

    return courses, row_idx
