"""
    Service that contains all methods relevant to registration
    Author: Emilija Zdilar  03-11-2018
"""
from django.db.models import Q
from core.models import Person, Device, PersonDevice
from core.serializers import PersonSerializer, DeviceSerializer, timezone
from typing import Dict, Union

from core.utils.validation_details import ValidationDetails


def validate_and_create_user(person_data: Dict[str, str]) -> Dict[str, Union[bool, Dict[str, str]]]:
    """
       Helper method that adds a new user after validation
    Does nothing if user already exists, since it was
    already validated.
    Args:
        person_data: request data related to a person

    Returns: dict with data and success status

    """
    if not Person.objects.filter(external_id=person_data["external_id"]).exists():
        person_serializer = PersonSerializer(data=person_data)
        if person_serializer.is_valid():
            person_serializer.save()
            person_validation_details = ValidationDetails(True, person_serializer.data, None)
        else:
            person_validation_details = ValidationDetails(False, None, person_serializer.errors)
    else:
        person_validation_details = ValidationDetails(True, None, None)

    return vars(person_validation_details)


def validate_and_create_device(device_data: Dict[str, str]) -> Dict[str, Union[bool, Dict[str, str]]]:
    """
    Helper method that adds a new device after validation
    Does nothing if device already exists, since it was
    already validated.
    Args:
        device_data: request data related to a person

    Returns: dict with data and success status
    """

    if not Device.objects.filter(device_external_id=device_data['device_external_id']).exists():
        device_serializer = DeviceSerializer(data=device_data)
        if device_serializer.is_valid():
            device_serializer.save()
            device_validation_details = ValidationDetails(True, device_serializer.data, None)
        else:
            device_validation_details = ValidationDetails(False, None, device_serializer.errors)
    else:
        device_validation_details = ValidationDetails(True, None, None)

    return vars(device_validation_details)


def user_and_device_already_connected(person_id: int, device_id: int) -> Union[PersonDevice, None]:
    try:
        person_device = PersonDevice.objects.get(person=person_id, device=device_id, valid_to=None)
        return person_device
    except PersonDevice.DoesNotExist:
        return None


def update_current_person_device_records(person_id: int, device_id: int) -> None:
    """
    Helper method that makes sure person only has only one
    active device at the time.
    Args:
        person_id: primary key of a person
        device_id: primary key of a device

    Returns: nothing
    """

    try:
        existing_record = PersonDevice.objects.\
            get(Q(person=person_id, valid_to=None) | Q(device=device_id, valid_to=None))
        existing_record.valid_to = timezone.now()
        existing_record.save()
    except PersonDevice.DoesNotExist:
        pass
