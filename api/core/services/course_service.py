"""
    Service that contains all methods relevant to course object
    Author: lejladzananovic 10-11-2018
    Author: obostjancic 01-12-2018
    Author: MelihaK 8-12-2018
"""

from typing import List, Dict

from core.models import Person, Course, PersonCourse
from core.models import QrCode, Lecture
from core.services import lecture_service, person_service
from core.utils.percentage import formatted_percentage
from core.utils.constants import ALL_ROLE_TYPES, TEACHER, ASSISTANT, STUDENT


def get_by_person(person: Person)->List[Course]:
    """
    Args:
       person object

    Returns:
       list of courses for specific person
    """

    return Course.objects.filter(personcourse__person=person)


def get_by_person_role(person: Person, role: str) -> List[Course]:
    """
    Args:
         person object and role parameter

    Returns:
         list of courses for specific person with specific role
    """

    if role is not None and role not in ALL_ROLE_TYPES:
        raise ValueError("Invalid role type. Expected one of: %s" % ALL_ROLE_TYPES)

    if role == TEACHER or role == ASSISTANT:
        return Course.objects.filter(personcourse__person=person, personcourse__role__in=[ASSISTANT, TEACHER])
    else:
        return Course.objects.filter(personcourse__person=person, personcourse__role=role)


def is_enrolled(person: Person, course: Course) -> PersonCourse:
    """
    Helper method that checks if a person is enrolled in a course

    Args:
        person: a student
        course: a course

    Returns: PersonCourse object if student is enrolled, None otherwise

    """
    return PersonCourse.objects.filter(person=person, course=course).first()


def link_person_to_course(person: Person, course: Course, role: str) -> PersonCourse:
    """
    Helper method that creates a new PersonCourse object with a passed role,
    saves it to database and returns created object.
    Args:
        person: A person (student or teacher)
        course: A course
        role: Role that this person will have on this course.
    Returns:
        created PersonCourse object
    """
    existing_link = PersonCourse.objects.filter(person=person, course=course, role=role).first()

    if existing_link is not None:
        return existing_link

    return PersonCourse.objects.create(person=person, course=course, role=role)


def get_or_create_course_by_name(course_name: str) -> Course:
    """
    NOTE: THIS IS ONLY FOR IMPORTER SERVICE. PLEASE DO NOT USE IT ANYWHERE ELSE!

    Takes a name of a course, and checks the database if it already exists there. If it does then returns this
    persisted course. Else creates a new course object, saves it to the database, and then returns it.
    Args:
        course_name: Name of a course.

    Returns:
        Persisted course object.
    """
    course = Course.objects.filter(name=course_name).first()

    if course is None:
        return Course.objects.create(name=course_name)

    return course


def get_course_role_for_person(person: Person, course: Course) -> str:
    """
    Helper method that returns the role of a person in a given course.
    We look at three cases:
    1. Person is a teacher -> string "T" that signifies a teacher role.
    2. Person is an assistant -> string "A" that signifies an assistant role.
    3. record in PersonCourse does not exist, or Person is a student -> empty string.
    Args:
        person: A given person
        course: A given course

    Returns: String that indicates the role of a person in a course in case he is Teacher
             or an Assistant, and empty string otherwise.
    """
    person_course = PersonCourse.objects.filter(person=person, course=course).first()
    if person_course is not None:
        return person_course.role
    else:
        return ""


def get_student_attendance_percentage(student: Person, lecture_type: str) -> List[Dict[str, str]]:
    """
    Args:
        student: student for whom courses attendance percentage is calculated
        lecture_type: lecture or exercise flag

    Returns: list containing courses along with its attendance percentage
    """
    course_attendance_percentage = []
    courses = get_by_person_role(student, STUDENT)

    for course in courses:
        lectures = lecture_service.get_by_course_and_type(course, lecture_type)

        attendance = 0
        for lecture in lectures:
            attendants = person_service.get_students_by_lecture(lecture)

            if student in attendants:
                attendance += 1

        if len(lectures) != 0:
            course_attendance_percentage.append({'course': course.name,
                                                 'total': '{}/{}'.format(attendance, len(lectures)),
                                                 'attendance': formatted_percentage(attendance, len(lectures))})
        else:
            course_attendance_percentage.append({'course': course.name,
                                                 'total': '{}/{}'.format(attendance, 0),
                                                 'attendance': '0%'})

    return course_attendance_percentage


def get_course_for_qr_code(qr_code: QrCode) -> Course:
    lecture = Lecture.objects.filter(qr_code=qr_code).first()
    return lecture.course


def course_attendance_statistics(course: Course, lecture_type: str) -> List:

    """
    Args:
        course: course for which attendance statistics is calculated
        lecture_type: lecture or exercise type

    Returns: course attendance statistics info (format:
        student name, number of lectures student attended / number of lectures for that course, attendance percentage)
    """

    lectures = lecture_service.get_by_course_and_type(course, lecture_type)
    students = person_service.get_persons_by_course(course, 'S')
    attendance_statistics = []

    for student in students:
        attendance_counter = 0
        student_attendance = ['{} {}'.format(student.first_name, student.last_name)]

        for lecture in lectures:
            attendants = person_service.get_students_by_lecture(lecture)

            if student in attendants:
                attendance_counter += 1

        student_attendance.append('{}/{}'.format(attendance_counter, len(lectures)))
        student_attendance.append(formatted_percentage(attendance_counter, len(lectures)))

        attendance_statistics.append(student_attendance)

    return attendance_statistics
