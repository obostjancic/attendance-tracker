import io
from io import BytesIO
from typing import Tuple, List

import xlsxwriter
from xlsxwriter import Workbook
from xlsxwriter.worksheet import Worksheet

from core.models import Course, Person
from core.services import course_service
from core.services.lecture_service import get_by_course, get_by_course_and_type
from core.services.person_service import get_persons_by_course, get_students_by_lecture
from core.utils.constants import REPORT_PRESENT_CHAR, REPORT_ABSENT_CHAR, EMPTY_STRING, PERCENTAGE, I18N_TOTAL_BS, \
    I18N_STUDENT_BS, I18N_INDEX
from core.utils.percentage import formatted_percentage

"""
    File containing reporting service business logic.
    Author: melihak 04-12-2018
"""


def get_attendance_by_course(course: Course, lecture_type: str):
    """
    Args:
        course: course for which attendance is "calculated"
        lecture_type: lecture or exercise flag

    Returns: attendance info and statistics for passed course
    """

    attendance_by_course, lectures_dates = [], [EMPTY_STRING]
    lectures = get_by_course_and_type(course, lecture_type)
    students = get_persons_by_course(course, 'S')

    for lecture in lectures:
        lectures_dates.append("{}".format(lecture.qr_code.valid_from.date().strftime('%d.%m.%Y.')))

    lectures_dates.append(I18N_TOTAL_BS)
    lectures_dates.append(PERCENTAGE)
    attendance_by_course.append(lectures_dates)

    for student in students:
        attendance_counter = 0
        student_attendance = ['{} {}'.format(student.first_name, student.last_name)]

        for lecture in lectures:
            attendants = get_students_by_lecture(lecture)

            if student not in attendants:
                student_attendance.append(REPORT_ABSENT_CHAR)
            else:
                student_attendance.append(REPORT_PRESENT_CHAR)
                attendance_counter += 1

        student_attendance.append('{}/{}'.format(attendance_counter, len(lectures)))

        if len(lectures) > 0:
            student_attendance.append(formatted_percentage(attendance_counter, len(lectures)))
        else:
            student_attendance.append('{}{}'.format(0, PERCENTAGE))

        attendance_by_course.append(student_attendance)

    return len(lectures_dates) - 2, attendance_by_course


def get_attendants_for_lectures(course: Course, lecture_type: str) -> List:

    """
    Args:
        course: course object for which the list of lectures is being fetched from the database
        lecture_type: lecture or exercise flag

    Returns: list of lectures (lecture date) and attendants (first name, last name and external id) info
    """

    lectures = get_by_course_and_type(course, lecture_type)
    lectures_attendants = []

    for lecture in lectures:
        lecture_attendants = [['{}'.format(lecture.qr_code.valid_from.date().strftime('%d.%m.%Y.')), '']]

        for person in get_students_by_lecture(lecture):
            lecture_attendants.append(['{} {}'.format(person.first_name, person.last_name),
                                       '{}'.format(person.external_id)])

        lectures_attendants.append(lecture_attendants)

    return lectures_attendants


def get_attendance_by_course_report(course: Course, lecture_type: str) -> BytesIO:
    """
    Defines report content and design.
    Args:
        course: course for which attendance is "calculated"
        lecture_type: lecture or exercise flag
    Returns:
         BytesIO object representation of an xlsx file
    """

    output = io.BytesIO()

    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()

    format_center = workbook.add_format({'align': 'center'})
    format_vertical = workbook.add_format({'rotation': 90, 'align': 'center'})
    format_bold = workbook.add_format({'bold': True})
    workbook, worksheet = apply_formatting(workbook, worksheet)

    lecture_count, attendance_matrix = get_attendance_by_course(course, lecture_type)

    worksheet.write('A1', course.name, format_bold)
    worksheet.write('A2', '')

    for row_idx, row in enumerate(attendance_matrix):
        if row_idx == 0:
            worksheet.write_row(row_idx + 2, 0, row, format_vertical)
        else:
            worksheet.write_row(row_idx + 2, 0, row)

    worksheet.set_column(0, 0, width=get_max_name_length(attendance_matrix))
    worksheet.set_column(1, lecture_count, width=2.2, cell_format=format_center)

    workbook.close()
    output.seek(0)

    return output


def get_student_attendance_percentage_report(student: Person, lecture_type: str) -> BytesIO:
    """
    Defines report format and design
    Args:
        student: Person object for which the report will be generated
        lecture_type: lecture or exercise flag

    Returns:
        BytesIO object representation of an xlsx file
    """

    output = io.BytesIO()

    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()

    format_bold = workbook.add_format({'bold': True})
    workbook, worksheet = apply_formatting(workbook, worksheet)

    student_attendance_percentage = course_service.get_student_attendance_percentage(student, lecture_type)

    worksheet.write('A1', '{} {}'.format(student.first_name, student.last_name), format_bold)
    worksheet.write('A2', EMPTY_STRING)

    for row_idx, row in enumerate(student_attendance_percentage):
        worksheet.write_row(row_idx + 2, 0, list(row.values()))

    worksheet.set_column(0, 0, width=get_max_course_name_length(student_attendance_percentage))

    workbook.close()
    output.seek(0)

    return output


def get_attendants_for_lectures_report(course: Course, lecture_type: str) -> BytesIO:
    """
    Defines report content and design.

    Args:
        course: course object for which the list of lectures is being fetched from the database
        lecture_type: lecture or exercise flag

    Returns: BytesIO object representation of an xlsx file
    """

    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    format_bold = workbook.add_format({'bold': True})

    attendants_by_lectures = get_attendants_for_lectures(course, lecture_type)

    for lecture in attendants_by_lectures:

        counter = 0
        worksheet = workbook.add_worksheet()
        worksheet.write_row(counter, 0, [course.name], cell_format=format_bold)
        counter += 1

        for attendant in lecture:
            if counter == 2:
                worksheet.write_row(counter, 0, [''])
                counter += 1
                worksheet.write_row(counter, 0, [I18N_STUDENT_BS, I18N_INDEX], cell_format=format_bold)
                counter += 1
            elif counter == 1:
                worksheet.write_row(counter, 0, attendant, cell_format=format_bold)
                counter += 1
            else:
                worksheet.write_row(counter, 0, attendant)
                counter += 1

        worksheet.set_column(0, 0, width=30)
        worksheet.set_column(1, 1, width=15)

    workbook.close()
    output.seek(0)

    return output


def apply_formatting(book: Workbook, sheet: Worksheet) -> Tuple[Workbook, Worksheet]:
    """
    Helper method that defines and applies formats for cell styling
    Args:
        book: Workbook object, xlsx file
        sheet: Worksheet object, first sheet in a file

    Returns:
        workbook and a worksheet with applied formats
    """
    format_red = book.add_format({'bg_color': '#FFC7CE', 'font_color': '#9C0006', 'bold': True})
    format_green = book.add_format({'bg_color': '#C6EFCE', 'font_color': '#006100', 'bold': True})
    format_bold = book.add_format({'bold': True})

    sheet.conditional_format('B2:Z200', {'type': 'cell', 'criteria': '==', 'value': '"-"', 'format': format_red})
    sheet.conditional_format('B2:Z200', {'type': 'cell', 'criteria': '==', 'value': '"+"', 'format': format_green})
    sheet.conditional_format('B2:Z200', {'type': 'text', 'criteria': 'containing', 'value': '%', 'format': format_bold})
    sheet.conditional_format('B2:Z200', {'type': 'text', 'criteria': 'containing', 'value': '/', 'format': format_bold})

    return book, sheet


def get_max_name_length(attendance_matrix) -> int:
    longest_name = ''

    for row_idx, row in enumerate(attendance_matrix):
        if row_idx != 0 and len(row[0]) > len(longest_name):
            longest_name = row[0]

    return len(longest_name)


def get_max_course_name_length(dict_list) -> int:
    longest_name = ''

    for row in dict_list:
        if len(row['course']) > len(longest_name):
            longest_name = row['course']

    return len(longest_name)
