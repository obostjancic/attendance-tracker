"""
    Service that contains all methods relevant to sending/receiving e-mail
    Author: obostjancic 21-10-2018
            Emilija Zdilar 21-12-2018
            Lejla Dzananovic 31-12-2018
"""
import datetime
import string
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from smtplib import SMTP

from core.models import Course, Person, QrCode
from core.utils import qr_code

from core.utils.constants import EMAIL, PASSWORD, SUCCESS_MESSAGE, SMTP_MAIL, IMAGE, JPEG, FROM, TO, SUBJECT, \
    DATE_FORMAT, HEADER_BASE, HEADER_CONTENT_TRANSFER_ENCODING, CONTENT_DISPOSITION, QR_CODE, ATTACHMENT, PORT


def mail_qr_code(qr_code_for_sending: QrCode, course: Course, person: Person) -> string:
    """
    helper method that send an image of a Qr code to a person. The e-mail is sent from previously
    set up address attendancetrackerpmf@gmail.com. The address that the mail is sent is found from
    passed person object. qr_code util functions are used to generate image from passed QrCode object.
    image created from qr_code_for_sending is put in the attachment of an e-mail. Subject of the email
    consists of course name and current date.
    Args:
        qr_code_for_sending: QrCode object whose image will be generated and sent
        course: course that the lecture for which the qr code is generated is in
        person: teacher / assistant who made the request

    Returns: Success string if mail was sent, None otherwise.

    """

    to = person.email
    subject = course.name
    date = datetime.datetime.today().strftime(DATE_FORMAT)
    qr_code_image = qr_code.generate(qr_code_for_sending.id)
    message = MIMEMultipart()
    message[SUBJECT] = subject + " " + date
    message[FROM] = EMAIL
    message[TO] = to

    part = MIMEBase(IMAGE, JPEG)
    part.set_payload(qr_code_image)
    part.add_header(HEADER_CONTENT_TRANSFER_ENCODING, HEADER_BASE)
    part[CONTENT_DISPOSITION] = ATTACHMENT % QR_CODE
    message.attach(part)

    try:
        server = SMTP(SMTP_MAIL, PORT)
        server.ehlo()
        server.starttls()
        server.login(EMAIL, PASSWORD)
        server.sendmail(FROM, to, message.as_string())
        server.close()
        return SUCCESS_MESSAGE
    except:
        return None

