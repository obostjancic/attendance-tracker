"""
    Service that contains all methods relevant to lecture object
    Author: lejladzananovic 18.11.2018.
"""

import string

from core.models import Course, Lecture, Person, QrCode, Classroom, PersonLecture, GpsCoordinate, PersonCourse
from typing import List, Dict, Union
from datetime import datetime, timedelta

from core.services import course_service, person_service
from core.utils.location import calculate_coordinates_distance
from core.utils.constants import QUESTIONABLE_ATTENDANCE, PRESENT, STUDENT, LECTURE, EXERCISE, TEACHER, ASSISTANT, \
    EMPTY_STRING, QR_CODE_INVALID_ERROR_MESSAGE, CURRENT_OPEN_LECTURE_ERROR_MESSAGE, STUDENT_TYPE_ERROR_MESSAGE

from core.serializers import PersonLectureSerializer, LectureSerializer
from core.utils.validation_details import ValidationDetails


def get_current(person: Person)->Lecture:
    """
       Args:
          Person object
       Returns:
          current open lecture for person
    """

    current_lecture = Lecture.objects.filter(personlecture__person=person, qr_code__valid_from__lte=datetime.now(),
                                             qr_code__valid_to__gte=datetime.now())

    if current_lecture is not None:
        return current_lecture
    else:
        raise ValueError(CURRENT_OPEN_LECTURE_ERROR_MESSAGE)


def get_by_course(course: Course)->List[Lecture]:
    """
    Args:
       Course object
    Returns:
       list of lectures
    """
    return Lecture.objects.filter(course=course)


def get_by_course_and_type(course: Course, lecture_type: str) -> List[Lecture]:
    """
    Args:
       Course object, lecture type
    Returns:
       list of lectures
    """
    return Lecture.objects.filter(course=course, type=lecture_type)


def attend_lecture(person: Person, qr_code: QrCode, allowed_radius: int, location_student: GpsCoordinate = None)->Dict:
    """
    Helper method that makes sure person is enrolled in course and
    returns dict with necessary data for person_lecture object creation.
    Args:
        person: student who is attending lecture
        qr_code: qr code that is generated for a given lecture
        location_student: location of device connected toa student
        allowed_radius: maximal distance that is allowed for Present status

    Returns: dict with data
    """

    if qr_code.valid_to < datetime.now():
        raise ValueError(QR_CODE_INVALID_ERROR_MESSAGE)

    lecture = Lecture.objects.filter(qr_code=qr_code).first()
    course = Course.objects.filter(lecture=lecture).first()
    person_enrolled = course_service.is_enrolled(person, course)
    classroom = Classroom.objects.filter(lecture=lecture).first()
    location_classroom = classroom.coordinates

    if person_enrolled is None:
        course_service.link_person_to_course(person, course, STUDENT)
    elif person_enrolled.role == TEACHER or person_enrolled.role == ASSISTANT:
        raise ValueError(STUDENT_TYPE_ERROR_MESSAGE)

    if location_student is None:
        attendance_type = QUESTIONABLE_ATTENDANCE
    else:
        if calculate_coordinates_distance(location_student, location_classroom) <= allowed_radius:
            attendance_type = PRESENT
        else:
            attendance_type = QUESTIONABLE_ATTENDANCE

    attendance_serializer = PersonLectureSerializer(data={
            "person_id": person.pk,
            "lecture_id": lecture.pk,
            "created_at": datetime.now(),
            "coordinates_id": location_student.pk,
            "attendance_type": attendance_type
            })
    if attendance_serializer.is_valid():
        attendance_serializer.save()
        return vars(ValidationDetails(True, attendance_serializer.data, None))
    else:
        return vars(ValidationDetails(False, None, attendance_serializer.errors))


def already_attends_lecture(person: Person, qr_code: QrCode)->bool:
    """
    Helper method that checks if a student already has marked himself present.
    Args:
        person: a student
        qr_code: qr code for a given lecture

    Returns: True is student is already present, false otherwise

    """
    person_lecture = PersonLecture.objects.filter(person=person, lecture__qr_code=qr_code).first()
    if person_lecture is None:
        return False
    else:
        return True


def get_lecture_type_for_person_role(person_role: string) -> string:
    """
    Helper method that, for a given role of a person in a course,
    returns the type of lecture i.e. lecture ("L") or exercise ("E").
    Args:
        person_role: role of a person in a course
    Returns: "L" if person is a teacher, "E" if person is an assistant
             and empty string otherwise.
    """

    if person_role == TEACHER:
        lecture_type = LECTURE
    elif person_role == ASSISTANT:
        lecture_type = EXERCISE
    else:
        lecture_type = EMPTY_STRING
    return lecture_type


def generate_lecture_and_qr_code(course: Course, lecture_type: string, classroom: Classroom, duration: int,
                                 person_limit: int = None) -> Dict[str, Union[bool, Dict[str, str]]]:
    """
    Helper method that generates qr code that is valid from a current moment for a specified amount
    of time. After that lecture data is returned for further validation and, finally, object creation.
    Args:
        course: given course
        lecture_type: lecture or exercise
        classroom: given classroom
        duration: number that indicates for how many minutes is qr code valid
        person_limit: number that indicates max. number of people allowed to attend a lecture
    Returns: dict with necessary data for lecture to be generated.
    """
    current_time = datetime.now()
    qr_code = QrCode.objects.create(valid_from=current_time, valid_to=current_time + timedelta(minutes=duration))
    lecture_serializer = LectureSerializer(data={
        "course_id": course.pk,
        "qr_code_id": qr_code.pk,
        "type": lecture_type,
        "classroom_id": classroom.pk,
        "person_limit": person_limit
    })
    if lecture_serializer.is_valid():
        lecture_serializer.save()
        lecture_validation_details = ValidationDetails(True, lecture_serializer.data, None)
    else:
        lecture_validation_details = ValidationDetails(False, None, lecture_serializer.errors)

    return vars(lecture_validation_details)


def lecture_already_started(course: Course, lecture_type: string) -> bool:
    """
    Helper method that checks, for a given course and type of a lecture, if there
    already exists an "active" lecture, i.e the lecture whose qr code is still valid
    Args:
        course: given course
        lecture_type: lecture or exercise

    Returns: True if there already exists such lecture, False otherwise.
    """
    generated_lecture = Lecture.objects.filter(qr_code__valid_to__gte=datetime.now(), course=course,
                                               type=lecture_type).first()
    if generated_lecture is None:
        return False

    return True


def get_all_students_for_lecture(lecture: Lecture) -> List[PersonLecture]:
    list_of_present_students = person_service.get_attendances_by_lecture(lecture)

    list_of_absent_students = create_absence_list(Person.objects.filter(personcourse__course=lecture.course,
                                                                        personcourse__role=STUDENT), lecture)
    return list(list_of_present_students) + list_of_absent_students


def get_present_students_for_lecture(lecture: Lecture) -> List[PersonLecture]:
    list_of_present_students = person_service.get_attendees_by_lecture(lecture)
    return list(list_of_present_students)


def create_absence_list(all_students: List[Person], lecture: Lecture) -> List[PersonLecture]:
    absence_list = []
    for student in all_students:
        if student not in person_service.get_students_by_lecture(lecture):
            absence_list.append(PersonLecture(person=student, lecture=lecture, attendance_type='AB'))

    return absence_list


def get_current_lecture(person: Person) -> Lecture:
    """
    Helper method that for a given person, returns current lecture that he teaches, if there is one.
    A current lecture is a lecture whose qr_code is still valid at the moment.
    Args:
        person: teacher or assistant

    Returns: Lecture object or None
    """

    current_lecture = Lecture.objects.filter(qr_code__valid_to__gte=datetime.now(), course__personcourse__person=person,
                                             course__personcourse__role__in=[TEACHER, ASSISTANT]).first()
    return current_lecture


def manually_set_attendance_type(lecture: Lecture, student: Person, new_attendance_type: str) -> \
        Dict[str, Union[bool, Dict[str, str]]]:
    """
    This method sets manually the attendance type for a student by professor/assistant. The attendance_type
    can be confirmed (from QP to PR) or denied (from QP to AB). Professor can change attendance status from PR to AB,
    too e.g. in case when professor asks a student to leave the lecture.

    If person_lecture object came form presence list, record will be updated. If person_lecture object came from
    absence list, PersonLecture object will be created with new attendance status.

    Args:
        student: person object that represents a student
        lecture: current lecture
        new_attendance_type: PR/QP/AB

    Returns: PersonLecture object with new attendance type
    """
    person_lecture = PersonLecture.objects.filter(person=student, lecture=lecture).first()

    if person_lecture is None:
        person_lecture_serializer = PersonLectureSerializer(data={
            "person_id": student.pk,
            "lecture_id": lecture.pk,
            "created_at": datetime.now(),
            "coordinates_id": lecture.classroom.coordinates.pk,
            "attendance_type": new_attendance_type
        })
        if person_lecture_serializer.is_valid():
            person_lecture_serializer.save()
            attendance_details = ValidationDetails(True, person_lecture_serializer.data, None)
        else:
            attendance_details = ValidationDetails(False, None, person_lecture_serializer.errors)
    else:
        person_lecture.attendance_type = new_attendance_type
        person_lecture.save(update_fields=['attendance_type'])
        attendance_details = ValidationDetails(True, PersonLectureSerializer(person_lecture).data, None)

    return vars(attendance_details)

