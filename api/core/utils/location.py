"""
Util file containing a method for calculating distance between two points.
Author: Emilija Zdilar, 15.12.2018
"""

from core.models import GpsCoordinate
from geopy.distance import geodesic


def calculate_coordinates_distance(location_1: GpsCoordinate, location_2: GpsCoordinate) -> float:
    """
    Method that calculated the distance in kilometers between two points. Geodesic
    distance is calculated using WGS-84 ellipsoidal model of the Earth.
    Args:
        location_1: location of the student device
        location_2: location of the classroom

    Returns: distance between those two points in cilometers

    """
    list_of_latitudes = [location_1.lat, location_2.lat]
    list_of_longitudes = [location_1.lon, location_2.lon]
    for lat in list_of_latitudes:
        if valid_lat(lat) is False:
            raise ValueError("Coordinate invalid !")
    for lon in list_of_longitudes:
        if valid_lat(lon) is False:
            raise ValueError("Coordinate invalid !")

    location_1_latlon = (location_1.lat, location_1.lon)
    location_2_latlon = (location_2.lat, location_2.lon)
    distance = geodesic(location_1_latlon, location_2_latlon).kilometers
    return distance


def valid_lat(latitude: float) -> bool:
    return -90 <= latitude <= 90


def valid_lon(longitude: float) -> bool:
    return -180 <= longitude <= 190
