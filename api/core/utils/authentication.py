from django.contrib.auth.middleware import AuthenticationMiddleware
from django.http import JsonResponse
from rest_framework.request import Request
from rest_framework.status import HTTP_401_UNAUTHORIZED

from core.services import person_service

"""
    Custom authentication middleware. Based on custom request header 'Device-Id'
    Author: obostjancic 01-12-2018
"""


class DeviceIdAuthMiddleware(AuthenticationMiddleware):

    def process_request(self, request: Request):
        """
        Takes our custom 'Device-Id' header form the request and resolves a person based on that. If a device
        with this device id is tied to a person then it attaches that person to the request object for further use
        in controllers, and just like any other middleware it forwards this request to the target controller. If the
        header is not present or is invalid then returns HTTP response with status 401 (Unauthorized)
        Args:
            request: Original request object.

        Returns:
            None, but attaches the person to the request object. Returns HTTP status 401 in case of invalid header
        """
        if request.path == '/api/core/register/':
            return None

        elif request.path.startswith('/api/core/'):
            device_id = request.META.get('HTTP_DEVICE_ID')

            if device_id is None:
                return JsonResponse({'error': 'Missing Device-Id header'}, status=HTTP_401_UNAUTHORIZED)

            try:
                user = person_service.get_by_device(device_id)
            except ValueError:
                return JsonResponse({'error': 'Authentication header is invalid'},  status=HTTP_401_UNAUTHORIZED)

            user.is_active = True

            request.user = user
