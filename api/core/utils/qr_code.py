"""
    Util file wrapper for generating/reading QR codes.
    Author: obostjancic 21-10-2018
"""

import string
from io import BytesIO

import base64
import qrcode
from qrcode.image.pil import PilImage


def to_base_64(image: PilImage) -> string:
    """
    Simple converter that turns generated PilImage to base64
    encoded string. Currently used within mail_service
    Args:
        image: generated PilImage object

    Returns: base64 encoded string.

    """
    buffered = BytesIO()
    image.save(buffered, format="JPEG")
    return base64.b64encode(buffered.getvalue())


def generate(lecture_id: int) -> string:
    """
    QR code generator wrapper. Generates QR code and writes
    passed lecture_id into it. Then calls base64 encoder for this QR code
    Args:
        lecture_id: Id of the lecture QR code is being generated for.

    Returns:
        Base 64 encoded QR code containing lecture_id
    """
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    qr.add_data(lecture_id)
    qr.make(fit=True)

    return to_base_64(qr.make_image(fill_color="black", back_color="white"))
