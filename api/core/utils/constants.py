"""
    Application constants should be listed here.
    Author: obostjancic 21-10-2018
    Author: melihak 04-12-2018
"""

SENDGRID_API_KEY = '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'

PLACEHOLDER_EXTERNAL_ID_SUFFIX = 'PlaceholderPerson'
PLACEHOLDER_EMAIL_SUFFIX = '@placeholder.com'
# Reporting service constants
I18N_STUDENT_BS = 'Student'
I18N_INDEX = 'Index'
REPORT_PRESENT_CHAR = '+'
REPORT_ABSENT_CHAR = '-'
EMPTY_STRING = ''
I18N_ATTENDANCE_REPORT_FILENAME_PREFIX_BS = 'Prisustvo'
I18N_ATTENDANTS_REPORT_FILENAME_PREFIX_BS = 'Spisak prisutnih'
XLSX_FORMAT = '.xlsx'
I18N_TOTAL_BS = 'Ukupno'
PERCENTAGE = '%'
XLSX_CONTENT_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'

RESPONSE_ERROR = "error"
RESPONSE_MESSAGE_FORBIDDEN = "You are not authorized."
RESPONSE_CURRENT_LECTURE = "There is still an active lecture."
RESPONSE_CLASSROOM_NOT_FOUND = "Classroom does not exist."
RESPONSE_COURSE_NOT_FOUND = "Course does not exist."
RESPONSE_DEVICE_NOT_FOUND = "Device does not exist."
RESPONSE_GPS_NOT_FOUND = "Gps_coordinates do not exist."
RESPONSE_LECTURE_NOT_FOUND = "Lecture does not exist."
RESPONSE_ORG_UNIT_NOT_FOUND = "Organisation unit does not exist."
RESPONSE_PERSON_NOT_FOUND = "Person does not exist."
RESPONSE_QR_CODE_NOT_FOUND = "QR code does not exist."
RESPONSE_DELETED_COURSE = "The Course is deleted successfully !"
RESPONSE_DELETED_LECTURE = "The Lecture is deleted successfully !"
RESPONSE_LECTURE_FORBIDDEN = "You are not authorized for this lecture."
RESPONSE_ERROR_MAIL_SENDING = "Error in sending QR code. "
RESPONSE_MAIL_SENT_SUCCESSFULLY = "Mail sent successfully !"
QR_CODE_INVALID_ERROR_MESSAGE = "QR code is invalid!"
CURRENT_OPEN_LECTURE_ERROR_MESSAGE = "There is no current open lecture for this person"
STUDENT_TYPE_ERROR_MESSAGE = "Person is not a student."
ERROR_MISSING_LECTURE_ID = "Lecture_id must be specified."
ERROR_MISSING_COURSE_ID = "Course_id must be specified."
ERROR_MISSING_DEVICE_ID = "Device_id must be specified."
ALREADY_ATTENDS_LECTURE = "You already attend this lecture."
ERROR_MISSING_ROLE = "Role must be specified."
ERROR_STUDENT_COURSE = "Student is not enrolled in this course."

# person service
TEACHER = 'T'
STUDENT = 'S'

ROLE_TYPES = [TEACHER, STUDENT]

# constants for mail_service
# mail to professors sent from this email
EMAIL = 'attendancetrackerpmf@gmail.com'
PASSWORD = 'Matematika1-'
SUCCESS_MESSAGE = "Success"
SMTP_MAIL = "smtp.gmail.com"
IMAGE = 'image'
JPEG = 'jpeg'
SUBJECT = 'Subject'
FROM = 'From'
TO = 'To'
DATE_FORMAT = '%d-%m-%Y'
HEADER_CONTENT_TRANSFER_ENCODING = 'Content-Transfer-Encoding'
HEADER_BASE = 'base64'
CONTENT_DISPOSITION = 'Content-Disposition'
QR_CODE = "qr_code"
ATTACHMENT = 'attachment; filename="%s.jpeg"'
PORT = 587

# importer service

DOC_TITLE = 'Doktor'
ASSISTANT = 'A'

ALL_ROLE_TYPES = [TEACHER, ASSISTANT, STUDENT]
PRESENT = 'PR'
QUESTIONABLE_ATTENDANCE = 'QP'
LECTURE = "L"
EXERCISE = "E"
