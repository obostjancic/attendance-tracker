from typing import Union, Dict


class ValidationDetails(object):
    """
        Reusable class that contains validation details
        successful atribute contains information if user/device/lecture etc was successfuly validated/created-
        Data contains serializer data in case of success.
        Errors contain errors in case validation failed.
    """
    def __init__(self, successful: bool, data: Union[Dict[str, str], None], errors: Union[Dict[str, str], None]):
        self.successful = successful
        self.data = data
        self.errors = errors
