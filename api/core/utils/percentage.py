"""
Util file containing methods for calculating percentage
Author: obostjancic, 12-10-2018
"""


def formatted_percentage(count: int, total: int) -> str:
    percentage = round((count * 100)/total, 2)
    return '{}%'.format(str(percentage))
