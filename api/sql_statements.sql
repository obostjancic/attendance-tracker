/* Test data for attendance tracker app */

/* --- org_unit --- */
INSERT INTO org_unit(name) VALUES ('PMF');

/* --- course --- */
INSERT INTO course(name) VALUES ('Softverski inzenjering');
INSERT INTO course(name) VALUES ('Kompjuterska vizija');

/* --- person --- */
/* --- teachers --- */
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Esmir', 'Pilav', 'TeacherEsmir', 'esmir.pilav@gmail.com');
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Sead', 'Delalic', 'TeacherSead', 'sead.delalic@gmail.com');
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Elmedin', 'Selmanovic', 'TeacherElmedin', 'eslmedins@gmail.com');
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Harun', 'Hindija', 'TeacherHarun', 'harun.hindija@gmail.com');
/* --- students --- */
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Ognjen', 'Bostjancic', '274/5264/M-17', 'ognjen.bostjancic@gmail.com');
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Lejla', 'Bakija', '271/5252/M-17', 'lejla.bakija@gmail.com');
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Nudzejma', 'Imamovic', '276/5242/M-17', 'nudzejma.imamovic@hotmail.com');
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Amina', 'Lagumdzija', '270/5253/M-17', 'amina.lagumdzija.tkn@gmail.com');
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Meliha', 'Kurtagic', '269/5320/M-17', 'melihakurtagic@gmail.com');
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Lejla', 'Dzananovic', '265/5260/M-17', 'lejlaadzananovic@hotmail.com');
INSERT INTO person (first_name, last_name, external_id, email) VALUES ('Emilija', 'Zdilar', '261/5111/M-17', 'zdilar.emilija@hotmail.com');


/* --- device --- */
INSERT INTO device (device_external_id) VALUES ('lejlabTel');
INSERT INTO device (device_external_id) VALUES ('lejladzTel');
INSERT INTO device (device_external_id) VALUES ('melihaTel');
INSERT INTO device (device_external_id) VALUES ('emilijaTel');
INSERT INTO device (device_external_id) VALUES ('nudzejmaTel');
INSERT INTO device (device_external_id) VALUES ('aminaTel');
INSERT INTO device (device_external_id) VALUES ('ognjenTel');

/* --- person_device --- */
INSERT INTO person_device (valid_from, valid_to, device, person) VALUES ('2018-10-18 10:23:54', null, 1, 9);
INSERT INTO person_device (valid_from, valid_to, device, person) VALUES ('2018-10-18 10:23:54', null, 4, 10);

/* --- gps_coordinate ---*/

insert into gps_coordinate(lat, lon, alt) VALUES (43.854028, 18.395167, 531.0);
insert into gps_coordinate(lat, lon, alt) VALUES (43.854083, 18.394722, 531.0);
insert into gps_coordinate(lat, lon, alt) VALUES (43.853833, 18.394806, 531.0);
insert into gps_coordinate(lat, lon, alt) VALUES (43.854083, 18.394500, 531.0);
insert into gps_coordinate(lat, lon, alt) VALUES (43.854222, 18.394722, 531.0);
insert into gps_coordinate(lat, lon, alt) VALUES (43.853750, 18.395194, 531.0);

/* -- classroom --*/

insert into classroom(name, coordinates, org_unit)  VALUES ('428', 1, 1);
insert into classroom(name, coordinates, org_unit)  VALUES ('432', 2, 1);
insert into classroom(name, coordinates, org_unit)  VALUES ('RC', 3, 1);
insert into classroom(name, coordinates, org_unit)  VALUES ('419', 4, 1);
insert into classroom(name, coordinates, org_unit)  VALUES ('441', 5, 1);
insert into classroom(name, coordinates, org_unit)  VALUES ('426', 6, 1);



/*--person_course -- */

/* teacher */
insert into person_course(role, course, person) VALUES ('T', 1, 1);
insert into person_course(role, course, person) VALUES ('T', 2, 3);
insert into person_course(role, course, person) VALUES ('T', 1, 2);
insert into person_course(role, course, person) VALUES ('T', 2, 4);

/*students  */
insert into person_course(role, course, person) VALUES ('S', 1, 5);
insert into person_course(role, course, person) VALUES ('S', 1, 6);
insert into person_course(role, course, person) VALUES ('S', 1, 7);
insert into person_course(role, course, person) VALUES ('S', 1, 8);
insert into person_course(role, course, person) VALUES ('S', 1, 9);
insert into person_course(role, course, person) VALUES ('S', 1, 10);
insert into person_course(role, course, person) VALUES ('S', 1, 11);

insert into person_course(role, course, person) VALUES ('S', 2, 5);
insert into person_course(role, course, person) VALUES ('S', 2, 6);
insert into person_course(role, course, person) VALUES ('S', 2, 7);
insert into person_course(role, course, person) VALUES ('S', 2, 8);
insert into person_course(role, course, person) VALUES ('S', 2, 9);
insert into person_course(role, course, person) VALUES ('S', 2, 10);
insert into person_course(role, course, person) VALUES ('S', 2, 11);