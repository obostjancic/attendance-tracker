import * as constants from '../Constants';

class ValidateHelper {
    validate(text, type) {
        let validated;
        switch (type) {
            case constants.fieldTypes.email:
                validated = constants.validation.vemail.test(text);
                break;
            default:
                validated = constants.validation.alph.test(text);
                break;
        }
        return validated;
    }
}

export default new ValidateHelper();