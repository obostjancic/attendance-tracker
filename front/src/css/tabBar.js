import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    style: {
        backgroundColor: "#1f56af",
    },
    labelStyle: {
        fontSize: 15,
        fontWeight: 'bold',
    },
});