export const textStrings = {
    student: "Student",
    professor: "Profesor",
    send: "Pošalji",
    codeScan: "Skeniraj kod!",
    myAttendance: "Moja prisustva!",
    generate: "Generiši",
    app: "Attendance Tracker",
    registration: "Uspješno registovan/a",
    onScanMessage: "Uspješno skeniran QR kod",
    wrongQr: "Pogrešan QR kod! ",
    errorMessage: "Greška!",
    sendEmail: "Pošalji na e-mail",
    chooseClassroom: "Odaberite učionicu:",
    onSendEmail: "QR kod poslan na e-mail.",
    currentlyRegistered: "Trenutno registrovan kao: ",
    currentlyNotRegistered: "Trenutno niste registrovani!",
    studentStatus: "Trenutni status studenata",
    noCurrentLecture: "Nema trenutnog predavanja!",
    checkAttendance: "Potvrdi prisustvo",
    closeLecture: "Da li ste sigurni da želite zatvoriti predavanje?",
    accept: "Da",
    reject: "Ne",
    space: "  | ",
    close: "Zatvori",
    acceptMessage: "Status studenta promijenjen u prisutan.",
    rejectMessage: "Status studenta promijenjen u odsutan.",
    closeMessage: "Predavanje je zatvoreno.",
    total: "Ukupno",
    takeAList: "Preuzmi spisak",
    takeAStatisticList: "Preuzmi statistiku",
    noStudents: "Nema prijavljenih studenata",
    lectureAttendance: "Prisustvo na predavanju",
    exercisesAttendance: "Prisustvo na vježbama",
};

export const validation = {
    alph: /^([a-zA-ZčćžđšČĆŽĐŠ]+[' -]*)+$/,
    vemail: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
};
// vemail: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
export const navigationOptions = {
    professorTitle: "Registracija profesora",
    studentTitle: "Registracija studenta",
};

export const fieldTypes = {
    name: 'nameValidate',
    surname: 'surnameValidate',
    email: 'emailValidate',
};

export const placeholders = {
    name: "Ime",
    surname: "Prezime",
    email: "E - mail",
    code: "Kod",
    index: "Broj indeksa",
    duration: "Vrijeme trajanja koda",
    classroom: "Učionica",
    person_limit: 'Broj studenata',
};

export const navigation = {
    student: "Student",
    studentRegistration: "StudentRegistration",
    professorRegistration: "ProfessorRegistration",
    professor: "Professor",
    openLecture: "OpenLecture",
    openQrGenerator: "OpenQrGenerator",
    openCourseLectures: "CourseLectures",
    professorOpenCourse: "ProfessorOpenCourse",
};

export const lecture = {
    open: "Otvori",
    lectures: "Predavanja",
    students: "Studenti",
}

export const errors = {
    fetchPersonInfoFailed: "Nije moguće dobiti informacije o korisniku",
    registrationFailed: "Registracija nije uspjela! Molimo pokušajte ponovo.",
    scanningFailed: "Pogrešan QR kod! Molimo pokušajte ponovo.",
    attendanceListFetchFailed: "Neuspjelo ažuriranje liste prisustva!",
    lecturesListFetchFailed: "Neuspjelo ažuriranje liste predavanja!",
    sendEmailFailed: "Slanje QR koda na e-mail nije uspjelo! Molimo pokušajte ponovo.",
    startLectureFailed: "Otvaranje predavanja nije uspjelo! Nije moguće imati više od jednog predavanja u datom trenutku.",
    courseListFetchFailed: "Neuspjelo ažuriranje liste kurseva!",
    attendantStudentsListFetchFailed: "Neuspjelo ažuriranje liste prisutnih studenata!",
    classroomListFetchFailed: "Neuspjelo ažuriranje liste učionica!",
    acceptRejectMessageError: "Došlo je do greške pri promjeni statusa studenta!",
    closeMessageError: "Došlo je do greške pri zatvaranju predavanja!",

}

export const lectureStudents = "Studenti na ovom predavanju"
