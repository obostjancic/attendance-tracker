import React, {Component} from "react";
import PropTypes from "prop-types";
import componentsStyle from '../css/components';
import request from "../utils/request";
import {ScrollView, Text, View, Modal, Alert, TouchableOpacity} from "react-native";
import * as constants from "../Constants";

class CurrentStudentsStatus extends Component {
    constructor () {
        super();
        this.state = {
            attendanceList: [],
            modalVisible: false,
            currentIdx: -1,
        };

        this.onPressSendDataPR = this.pressSendDataPR.bind(this);
        this.onPressSendDataAB = this.pressSendDataAB.bind(this);

    }

    componentDidMount() {
        request.get('/core/lecture/students/' + this.props.lecture_id + '/')
            .then((response) => {
                this.setState({
                    attendanceList: response.data,
                })
            }).catch((error) => {
                this.setState({ error: error.response });
                console.log(error.response);
            });
    }

    componentDidUpdate(prevProps, prevState) {
        request.get('/core/lecture/students/' + this.props.lecture_id + '/')
            .then((response) => {
                if (prevState.attendanceList !== response.data) {
                    this.setState({
                        attendanceList: response.data,
                    })
                }
            }).catch((error) => {
                this.setState({ error: error.response });
                console.log(error.response);
            });
    }

    render () {
        if (this.props.lecture_id === undefined) {
            return (
                <View style={componentsStyle.container}>
                    <View style={componentsStyle.responsiveBox}>
                        <Text style={componentsStyle.noLecture}>{constants.textStrings.noCurrentLecture}</Text>
                    </View>
                </View>
            );
        }
        if (this.state.attendanceList.length === 0) {
            return (
                <View style={componentsStyle.container}>
                    <Text style={componentsStyle.textHeader}>{this.props.course_name}</Text>
                    <View style={componentsStyle.responsiveText}>
                        <Text style={componentsStyle.noLecture}>{constants.textStrings.noStudents}</Text>
                    </View>
                </View>
            );
        }
        let attendes = null;
        if (this.state.attendanceList) {
            attendes = this.state.attendanceList.map((attend, idx) => {
                let type = attend.attendance_type;
                let color = "#000";
                switch (type) {
                    case "PR":
                        color = "#039124";
                        break;
                    case "AB":
                        color = "#910012";
                        break;
                    default:
                        color = "#b9be15";
                        break
                }
                return (
                    <View key={idx} style={componentsStyle.attendanceList}>
                        <TouchableOpacity style={componentsStyle.attendanceList} onPress={() => {
                            this.setModalVisible(true, idx);
                        }}>
                            <Text style={componentsStyle.lectureName}>{attend.person.first_name} {attend.person.last_name} </Text>
                            <Text style={this.attendeesStyle(color)}/>
                        </TouchableOpacity>
                    </View>
                )
            })
        }

        let style = this.state.modalVisible ? componentsStyle.scrollAttendanceContainerOp : componentsStyle.scrollAttendanceContainer
        let person = this.state.currentIdx !== -1 ? this.state.attendanceList[this.state.currentIdx].person.first_name + " " + this.state.attendanceList[this.state.currentIdx].person.last_name : "";
        return (
            <ScrollView style={style}>
                <View style={componentsStyle.scrollView}>
                    <Text style={componentsStyle.textHeader}>{this.props.course_name}</Text>
                    {attendes}
                    <Modal animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                        }}>
                        <View style={componentsStyle.modal}>
                            <View>
                                <Text style={componentsStyle.textHeader}>{constants.textStrings.checkAttendance}</Text>
                                <Text style={componentsStyle.font}>{person}</Text>
                                <View style={componentsStyle.accept}>
                                    <Text style={componentsStyle.font} onPress={this.onPressSendDataPR}>{constants.textStrings.accept}</Text>
                                    <Text style={componentsStyle.font}>{constants.textStrings.space}</Text>
                                    <Text style={componentsStyle.font} onPress={this.onPressSendDataAB}>{constants.textStrings.reject}</Text>
                                    <Text style={componentsStyle.font}>{constants.textStrings.space}</Text>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setModalVisible(!this.state.modalVisible, -1);
                                        }}>
                                        <Text style={componentsStyle.font}>{constants.textStrings.close}</Text>
                                    </TouchableOpacity>
                                </View>


                            </View>
                        </View>
                    </Modal>
                </View>
            </ScrollView>
        );
    }

    pressSendDataPR() {
        request.put('/core/lecture/student/attendance/', {
            attendance_type: "PR",
            lecture: this.props.lecture_id,
            student: this.state.attendanceList[this.state.currentIdx].person.id,
        })
            .then((response) => {
                Alert.alert(constants.textStrings.app, constants.textStrings.acceptMessage)
                this.setModalVisible(!this.state.modalVisible, -1);
                console.log(response.data)
            }).catch((error) => {
                this.setState({ error: error.response })
                Alert.alert(constants.textStrings.app, constants.errors.acceptRejectMessageError)
                console.log(error.response);
            })
    }

    pressSendDataAB() {
        request.put('/core/lecture/student/attendance/', {
            attendance_type: "AB",
            lecture: this.props.lecture_id,
            student: this.state.attendanceList[this.state.currentIdx].person.id,
        })
            .then((response) => {
                Alert.alert(constants.textStrings.app, constants.textStrings.rejectMessage)
                this.setModalVisible(!this.state.modalVisible, -1);
                console.log(response.data)
            }).catch((error) => {
                this.setState({ error: error.response })
                Alert.alert(constants.textStrings.app, constants.errors.sendEmailFailed)
                console.log(error.response);
            })

    }

    setModalVisible(visible, idx) {
        this.setState({
            modalVisible: visible,
            currentIdx: idx,
        });
    }

    attendeesStyle = (color) => {
        return {
            backgroundColor: color,
            width: 20,
            height: 20,
            borderRadius: 10,
            flexGrow: 0,
            flexShrink: 0,
        }
    }
}

CurrentStudentsStatus.propTypes = {
    navigation: PropTypes.object,
    lecture_id: PropTypes.number,
    course_name: PropTypes.string,
};

export default CurrentStudentsStatus;