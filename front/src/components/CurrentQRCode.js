import React, {Component} from "react";
import PropTypes from "prop-types";
import componentsStyle from '../css/components';
import {Alert, Text, Modal, View} from "react-native";
import * as constants from "../Constants";
import QRCode from "react-native-qrcode";
import request from "../utils/request";

class CurrentQRCode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            qr_code_id: props.qr_code_id,
            course_name: '',
            modalVisible: false,
        };

        this.onPressClose = this.pressClose.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.qr_code_id !== this.props.qr_code_id) {
            this.setState({
                qr_code_id: this.props.qr_code_id,
            })
        }
    }

    render () {
        if (this.state.qr_code_id === undefined) {
            return (
                <View style={componentsStyle.container}>
                    <View style={componentsStyle.responsiveBox}>
                        <Text style={componentsStyle.noLecture}>{constants.textStrings.noCurrentLecture}</Text>
                    </View>
                </View>
            );
        }
        let style = this.state.modalVisible ? componentsStyle.containerOp : componentsStyle.container;
        return (
            <View style={style}>
                <View style={componentsStyle.responsiveBox}>
                    <Text style={componentsStyle.textHeader}>{this.props.course_name}</Text>
                    <QRCode style={componentsStyle.qrCodeOpenStyle}
                        value={this.props.qr_code_id.toString()}
                        size={200}
                        bgColor="black"
                        fgColor='white' />
                    <Text style={componentsStyle.buttonTextQROpen} onPress={() => this.props.openCurrentStudentsStatus()}>{constants.textStrings.studentStatus}</Text>
                    <Text style={componentsStyle.buttonTextClose} onPress={() => this.setModalVisible(true)}>{constants.textStrings.close}</Text>
                    <Modal animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}>
                        <View style={componentsStyle.modal}>
                            <View>
                                <Text style={componentsStyle.textHeader}>{constants.textStrings.closeLecture}</Text>
                                <View style={componentsStyle.accept}>
                                    <Text style={componentsStyle.font} onPress={this.onPressClose}>{constants.textStrings.accept}</Text>
                                    <Text style={componentsStyle.font}>{constants.textStrings.space}</Text>
                                    <Text style={componentsStyle.font} onPress={() => this.setModalVisible(false)}>{constants.textStrings.reject}</Text>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>
            </View>
        );
    }

    setModalVisible(visible) {
        this.setState({
            modalVisible: visible,
        });
    }

    pressClose() {
        request.put('/core/lecture/close/' + this.props.lecture_id + '/')
            .then(() => {
                Alert.alert(constants.textStrings.app, constants.textStrings.closeMessage)
                this.setState({
                    qr_code_id: undefined,
                    modalVisible: false,
                })
            }).catch((error) => {
                this.setState({ error: error.response })
                Alert.alert(constants.textStrings.app, constants.errors.closeMessageError)
                console.log(error.response);
            })
    }
}

CurrentQRCode.propTypes = {
    navigation: PropTypes.object,
    openCurrentStudentsStatus: PropTypes.func,
    qr_code_id: PropTypes.number,
    course_name: PropTypes.string,
    lecture_id: PropTypes.number,
};

export default CurrentQRCode;