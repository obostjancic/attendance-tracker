import { Component } from "react";
import {BackHandler} from "react-native";
import React from "react";
import PropTypes from "prop-types";
import request from "../utils/request";
import CurrentQRCode from "./CurrentQRCode";
import CurrentStudentsStatus from "./CurrentStudentsStatus";

class ProfessorOpenCourse extends Component {
    _willFocusSubscription;
    _willBlurSubscription;

    constructor(props) {
        super(props);
        this.state = {
            render: "currentQR",
            id: undefined,
            course_name: "",
        };
        this._willFocusSubscription = props.navigation.addListener('willFocus', () => {
            BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
        });
    }

    componentDidMount() {
        request.get(`/core/lecture/current/`)
            .then((response) => {
                let id = response.data.id;
                if (id !== this.state.id) {
                    this.setState({ id });
                }
                if (this.state.course_name !== response.data.course.name) {
                    this.setState({ course_name: response.data.course.name });
                }
            }).catch((error) => {
                this.setState({ error: error.response });
                console.log(error.response);
            });
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', () =>
        {
            BackHandler.removeEventListener('hardwareBackPress', () => {})
        });
    }

    componentDidUpdate(prevProps, prevState) {
        request.get('/core/lecture/current/')
            .then((response) => {
                let id = response.data.id;
                if (id !== prevState.id) {
                    this.setState({ id });
                }
                if (prevState.course_name !== response.data.course.name) {
                    this.setState({ course_name: response.data.course.name });
                }
            }).catch((error) => {
                if (error.response.data.error === "There is no current open lecture for this person") {
                    this.setState({ id: undefined });
                } else {
                    this.setState({ error: error.response });
                }
            });
    }

    componentWillUnmount() {
        this._willFocusSubscription && this._willFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }

    render() {
        let renderScreen = null;
        switch (this.state.render) {
            case "currentQR":
                renderScreen = <CurrentQRCode qr_code_id={this.state.id} course_name={this.state.course_name} lecture_id={this.state.id} navigation={this.props.navigation} openCurrentStudentsStatus={this.openCurrentStudentsStatus.bind(this)}/>
                break;
            case "currentStudentsStatus":
                renderScreen = <CurrentStudentsStatus lecture_id={this.state.id} course_name={this.state.course_name}/>
                break;
        }
        return renderScreen;
    }

    handleBackPress = () => {
        if (this.state.render === 'currentStudentsStatus') {
            this.setState({
                render: "currentQR",
            });
        } else if (this.state.render === 'currentQR') {
            this.props.navigation.pop();
            this.setState({
                render: "",
            });
        } else {
            return false;
        }
        return true;
    }

    openCurrentStudentsStatus() {
        this.setState({
            render: "currentStudentsStatus",
        });
    }

}

ProfessorOpenCourse.propTypes = {
    navigation: PropTypes.object,
};

export default ProfessorOpenCourse;