import { Component } from "react";
import {Alert, ScrollView, Text, View} from "react-native";
import React from "react";
import * as constants from '../Constants';
import componentsStyle from '../css/components';
import request from "../utils/request";
import PropTypes from "prop-types";
import RNFetchBlob from 'rn-fetch-blob'
import deviceInfo from 'react-native-device-info/deviceinfo'


class CourseStudents extends Component {

    constructor(props) {
        super(props);
        this.state = {
            course: props.course,
            statistics: [],
            statisticsE: [],
            roles: [],
            deviceId: deviceInfo.getUniqueID(),
            baseURL: process.env.NODE_ENV !== 'production' ? "https://attendance-tracker-develop.herokuapp.com/api" : "https://attendance-tracker-release.herokuapp.com/api",
        }
        this.onGetFileE = this.getFileE.bind(this);
        this.onGetStatisticFileE = this.getStatisticFileE.bind(this);

        this.onGetFile = this.getFile.bind(this);
        this.onGetStatisticFile = this.getStatisticFile.bind(this);
    }

    componentDidMount() {
        request.get('/core/person/info/')
            .then((response) => {
                this.setState({ roles: response.data.roles });
            }).catch((error) => {
                this.setState({ response: {} });
                Alert.alert(constants.textStrings.app, constants.errors.fetchPersonInfoFailed)
                console.log(error.response);
            });

        request.get(`/core/course/attendance/statistics/${this.props.course.id}` + '/L/')
            .then((response) => {
                let statistics = response.data;
                this.setState({ statistics});
            }).catch((error) => {
                this.setState({ error: error.response });
                console.log(error.response);
            });
        this.props.navigation.addListener('willFocus', (payload)=>{
            request.get(`/core/course/attendance/statistics/${this.props.course.id}` + '/L/')
                .then((response) => {
                    let statistics = response.data;
                    if (statistics !== this.state.statistics){
                        this.setState({ statistics});
                    }
                }).catch((error) => {
                    this.setState({ error: error.response });
                    console.log(error.response);
                });
        });

        request.get(`/core/course/attendance/statistics/${this.props.course.id}` + '/E/')
            .then((response) => {
                let statisticsE = response.data;
                this.setState({ statisticsE});
            }).catch((error) => {
                this.setState({ error: error.response });
                console.log(error.response);
            });
        this.props.navigation.addListener('willFocus', (payload)=>{
            request.get(`/core/course/attendance/statistics/${this.props.course.id}` + '/E/')
                .then((response) => {
                    let statisticsE = response.data;
                    if (statisticsE !== this.state.statisticsE){
                        this.setState({ statisticsE});
                    }
                }).catch((error) => {
                    this.setState({ error: error.response });
                    console.log(error.response);
                });
        });
    }

    render() {
        let statistics = this.state.statistics.map((course, idx) => (<View
            key={idx}
            style={componentsStyle.courses}>
            <Text style={componentsStyle.lectureName}>{course[0]}</Text>
            <Text style={componentsStyle.lectureItem}> ({course[1]}) - {course[2]}</Text>
        </View>));

        let statisticsE = this.state.statisticsE.map((course, idx) => (<View
            key={idx}
            style={componentsStyle.courses}>
            <Text style={componentsStyle.lectureName}>{course[0]}</Text>
            <Text style={componentsStyle.lectureItem}> ({course[1]}) - {course[2]}</Text>
        </View>));

        if (this.state.roles.includes('A')){
            return (
                <ScrollView style={componentsStyle.scrollContainer}>
                    <Text style={componentsStyle.textHeader}>{this.props.course.name}</Text>
                    <Text style={componentsStyle.textHeaderLecture}>{constants.textStrings.total}</Text>
                    {statisticsE}
                    <Text style={componentsStyle.buttonTextPDF} onPress={this.onGetFileE}>{constants.textStrings.takeAList}</Text>
                    <Text style={componentsStyle.buttonTextPDF} onPress={this.onGetStatisticFileE}>{constants.textStrings.takeAStatisticList}</Text>
                </ScrollView>
            );
        }
        else {
            return (
                <ScrollView style={componentsStyle.scrollContainer}>
                    <Text style={componentsStyle.textHeader}>{this.props.course.name}</Text>
                    <Text style={componentsStyle.textHeaderLecture}>{constants.textStrings.total}</Text>
                    {statistics}
                    <Text style={componentsStyle.buttonTextPDF} onPress={this.onGetFile}>{constants.textStrings.takeAList}</Text>
                    <Text style={componentsStyle.buttonTextPDF} onPress={this.onGetStatisticFile}>{constants.textStrings.takeAStatisticList}</Text>
                </ScrollView>
            );
        }
    }

    getFileE () {
        const { config, fs } = RNFetchBlob
        let downloadDir = fs.dirs.DownloadDir;
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                title: "Spisak prisutnih na predmetu " + this.props.course.name + ".xlsx",
                path: downloadDir + "/Spisak prisutnih na predmetu " + this.props.course.name + ".xlsx",
                description: 'Downloading xlsx.',
            },
        }

        config(options).fetch('GET',  this.state.baseURL + `/core/report/attendants/lectures/` + this.props.course.id + '/E/',
            {
                "Device-Id": this.state.deviceId ? this.state.deviceId : '',
            })
            .progress((received, total) => {
                console.log('progress ' + Math.floor(received/total*100) + '%')
            })
            .then((res) => {
                console.log('res: ', res)
            })
            .catch((error) => {
                console.log('Error: ', error);
            });
    }

    getFile () {
        const { config, fs } = RNFetchBlob
        let downloadDir = fs.dirs.DownloadDir;
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                title: "Spisak prisutnih na predmetu " + this.props.course.name + ".xlsx",
                path: downloadDir + "/Spisak prisutnih na predmetu " + this.props.course.name + ".xlsx",
                description: 'Downloading xlsx.',
            },
        }

        config(options).fetch('GET',  this.state.baseURL + `/core/report/attendants/lectures/` + this.props.course.id + '/L/',
            {
                "Device-Id": this.state.deviceId ? this.state.deviceId : '',
            })
            .progress((received, total) => {
                console.log('progress ' + Math.floor(received/total*100) + '%')
            })
            .then((res) => {
                console.log('res: ', res)
            })
            .catch((error) => {
                console.log('Error: ', error);
            });
    }

    getStatisticFileE() {
        const { config, fs } = RNFetchBlob
        let downloadDir = fs.dirs.DownloadDir;
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                title: "Statistika prisustva na predmetu " + this.props.course.name + ".xlsx",
                path: downloadDir + "/Statistika prisustva prisutnih na predmetu " + this.props.course.name + ".xlsx",
                description: 'Downloading xlsx.',
            },
        }
        config(options).fetch('GET',  this.state.baseURL + `/core/report/attendance/course/` + this.props.course.id + '/E/',
            {
                "Device-Id": this.state.deviceId ? this.state.deviceId : '',
            })
            .progress((received, total) => {
                console.log('progress ' + Math.floor(received/total*100) + '%')
            })
            .then((res) => {
                console.log('res: ', res)
            })
            .catch((error) => {
                console.log('ERRRROORRO', error);
            });
    }

    getStatisticFile() {
        const { config, fs } = RNFetchBlob
        let downloadDir = fs.dirs.DownloadDir;
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                title: "Statistika prisustva na predmetu " + this.props.course.name + ".xlsx",
                path: downloadDir + "/Statistika prisustva prisutnih na predmetu " + this.props.course.name + ".xlsx",
                description: 'Downloading xlsx.',
            },
        }
        config(options).fetch('GET',  this.state.baseURL + `/core/report/attendance/course/` + this.props.course.id + '/L/',
            {
                "Device-Id": this.state.deviceId ? this.state.deviceId : '',
            })
            .progress((received, total) => {
                console.log('progress ' + Math.floor(received/total*100) + '%')
            })
            .then((res) => {
                console.log('res: ', res)
            })
            .catch((error) => {
                console.log('ERRRROORRO', error);
            });
    }
}

CourseStudents.propTypes = {
    course: PropTypes.object,
    openQrGenerator: PropTypes.func,
    professorOpenCourse: PropTypes.func,
    navigation: PropTypes.object,
};


export default CourseStudents;
