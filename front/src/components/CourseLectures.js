import React, { Component } from 'react';

import { Text, View, TouchableOpacity, ScrollView, Alert} from 'react-native';
import PropTypes from 'prop-types';
import componentsStyle from "../css/components";
import request from "../utils/request";
import * as constants from '../Constants'

class CourseLecture extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lectures: [],
            activeLecture: null,
        }
    }

    componentDidMount() {
        request.get(`/core/lecture/course/${this.props.course.id}`)
            .then((response) => {
                this.setState({ lectures: response.data });
            }).catch((error) => {
                this.setState({ error: error.response });
                Alert.alert(constants.textStrings.app, constants.errors.lecturesListFetchFailed);
                console.log(error.response);
            });
    }

    render() {
        let lectures = this.state.lectures.map((lecture, idx) => this.state.activeLecture === idx ? <View key={idx}>
            <TouchableOpacity style={componentsStyle.activeListItemBGColor}
                onPress={() => this.listItemSelected(idx)}>
                <Text style={componentsStyle.activeListItemTextColor}>Predavanje {idx + 1}</Text>
            </TouchableOpacity>
            <View style={componentsStyle.responsiveBoxList}>
                <View style={componentsStyle.detailsStyleList}>
                    <Text style={componentsStyle.detailsStyleList} onPress={() => this.props.onLectureStudents({ data: lecture, lectureIdx: idx })}>{constants.lectureStudents}</Text>
                </View>
            </View>
        </View> : <TouchableOpacity key={idx} style={componentsStyle.listItem}
            onPress={() => this.listItemSelected(idx)}><Text>Predavanje {idx + 1}</Text>
        </TouchableOpacity>
        );
        return (
            <ScrollView style={componentsStyle.scrollContainer}>
                <Text style={componentsStyle.textHeader}>{this.props.course.name}</Text>
                {lectures}
            </ScrollView>
        );
    }

    listItemSelected = (idx) => {
        let activeLecture = null;
        if (this.state.activeLecture !== idx) {
            activeLecture = idx;
        }
        this.setState({ activeLecture });
    }
}

CourseLecture.propTypes = {
    course: PropTypes.object,
    onLectureStudents: PropTypes.func,
};



export default CourseLecture;