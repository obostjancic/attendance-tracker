import { Component } from "react";
import { Alert, Text, TouchableOpacity, View } from "react-native";
import React from "react";

import componentsStyle from '../css/components';
import PropTypes from "prop-types";
import QRCode from "react-native-qrcode";
import * as constants from "../Constants";
import request from "../utils/request";


class QrCodeGenerator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            qr_code_id: props.qr_code_id.toString(),
        }

        this.pressSendData = this.pressSendData.bind(this);
    }

    componentDidMount() {
    }

    render() {
        return (
            <View style={componentsStyle.container}>
                <View style={componentsStyle.responsiveBox}>
                    <Text style={componentsStyle.textHeader}>{this.props.course.name}</Text>
                    <QRCode style={componentsStyle.qrCodeStyle}
                        value={this.state.qr_code_id}
                        size={200}
                        bgColor="black"
                        fgColor='white' />
                    <TouchableOpacity onPress={this.pressSendData} style={componentsStyle.buttonStyle}>
                        <Text style={componentsStyle.buttonText}>{constants.textStrings.sendEmail}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    pressSendData() {
        request.post('/core/qr_code/mail/', {
            qr_code: this.state.qr_code_id,
        }).then(() => {
            Alert.alert(constants.textStrings.app, constants.textStrings.onSendEmail)
        }).catch((error) => {
            this.setState({ error: error.response })
            Alert.alert(constants.textStrings.app, constants.errors.sendEmailFailed)
            console.log(error.response);
        })
    }

}

QrCodeGenerator.propTypes = {
    qr_code_id: PropTypes.number,
    course: PropTypes.object,
    professorOpenCourse: PropTypes.func,
    openCourses: PropTypes.func,
    navigation: PropTypes.object,
};


export default QrCodeGenerator;