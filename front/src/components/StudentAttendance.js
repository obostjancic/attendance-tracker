import React, { Component } from "react";
import { ScrollView, View, Text } from "react-native";
import request from "../utils/request";
import * as constants from '../Constants';
import componentsStyle from '../css/components';
import PropTypes from "prop-types";

class StudentAttendance extends Component {
    static navigationOptions = {
        headerLeft: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            courses: [],
            exercises: [],
            role: 'S',
        };
    }

    componentDidMount() {
        request.get('/core/course/attendance/percentage/L/')
            .then((response) => {
                let courses = response.data;
                this.setState({ courses });
            }).catch((error) => {
                this.setState({ error: error.response });
                console.log(error.response);
            });
        this.props.navigation.addListener('willFocus', (payload)=>{
            request.get('/core/course/attendance/percentage/L/')
                .then((response) => {
                    let courses = response.data;
                    if (courses !== this.state.courses) {
                        this.setState({ courses });
                    }
                }).catch((error) => {
                    this.setState({ error: error.response });
                    console.log(error.response);
                });
        });

        request.get('/core/course/attendance/percentage/E/')
            .then((response) => {
                let exercises = response.data;
                this.setState({ exercises });
            }).catch((error) => {
                this.setState({ error: error.response });
                console.log(error.response);
            });
        this.props.navigation.addListener('willFocus', (payload)=>{
            request.get('/core/course/attendance/percentage/E/')
                .then((response) => {
                    let exercises = response.data;
                    if (exercises !== this.state.exercises) {
                        this.setState({ exercises });
                    }
                }).catch((error) => {
                    this.setState({ error: error.response });
                    console.log(error.response);
                });
        });
    }


    render() {
        let courses = this.state.courses.map((course, idx) => (<View
            key={idx}
            style={componentsStyle.courses}>
            <Text style={componentsStyle.lectureName}>{course.course}</Text>
            <Text style={componentsStyle.lectureItem}> ({course.total}) - {course.attendance}</Text>
        </View>));

        let exercises = this.state.exercises.map((course, idx) => (<View
            key={idx}
            style={componentsStyle.courses}>
            <Text style={componentsStyle.lectureName}>{course.course}</Text>
            <Text style={componentsStyle.lectureItem}> ({course.total}) - {course.attendance}</Text>
        </View>));

        return (
            <ScrollView style={componentsStyle.scrollContainer}>
                <Text style={componentsStyle.textHeaderEL}>{constants.textStrings.lectureAttendance}</Text>
                <View style={componentsStyle.listStyle}>
                    <ScrollView>
                        {courses}
                    </ScrollView>
                </View>
                <Text></Text>
                <Text style={componentsStyle.textHeaderEL}>{constants.textStrings.exercisesAttendance}</Text>
                <View style={componentsStyle.listStyle}>
                    <ScrollView>
                        {exercises}
                    </ScrollView>
                    <Text></Text>
                </View>
            </ScrollView>
        );
    }
}

StudentAttendance.propTypes = {
    navigation: PropTypes.object,
};

export default StudentAttendance;