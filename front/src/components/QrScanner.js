import deviceInfo from "react-native-device-info";

import { Component } from "react";
import { Alert, Text, TouchableOpacity, View } from "react-native";
import QRCodeScanner from "react-native-qrcode-scanner";
import React from "react";
import * as constants from '../Constants';
import componentsStyle from '../css/components';
import request from "../utils/request";
import gpsLocation from "../utils/gpsLocation";
import askForPermissions from "../utils/permissions";

class QrScanner extends Component {

    constructor(props) {
        super(props);
        this.state = {
            deviceId: deviceInfo.getUniqueID(),
            qr: 0,
            location: {
                latitude: null,
                longitude: null,
            },
        };

        this.onPressSendData = this.pressSendData.bind(this);

    }

    componentDidMount() {
        askForPermissions();
        gpsLocation().then((loc) => {
            this.setState({ location: loc });
            console.log(this.state);
        });
    }

    render() {
        askForPermissions();
        return (
            <View style={componentsStyle.container}>
                <View style={componentsStyle.responsiveBoxQR}>
                    <QRCodeScanner
                        onRead={this.onSuccess.bind(this)}
                    />
                    <TouchableOpacity style={componentsStyle.buttonTouchable}>
                        <Text style={componentsStyle.buttonTextQR}>{constants.textStrings.codeScan}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    pressSendData() {
        request.post('/core/lecture/attend/', {
            qr_code: this.state.qr,
            location: {
                latitude: this.state.location.latitude,
                longitude: this.state.location.longitude,
            },

        }).then(Alert.alert(
            constants.textStrings.app,
            constants.textStrings.onScanMessage
        )).catch((error) => {
            this.setState({ error: error.response })
            Alert.alert(constants.textStrings.app, constants.errors.scanningFailed)
            console.log(error.response);
        })
    }

    onSuccess(e) {
        this.setState({ qr: (!isNaN(e.data)) ? parseInt(e.data) : null }, function () {
            if (Number.isInteger(this.state.qr)){
                this.onPressSendData();
            }
            else
                Alert.alert(
                    constants.textStrings.app,
                    constants.textStrings.wrongQr
                );
        });
    }
}


export default QrScanner;
