import React, { Component } from 'react';

import { View, Image, Text, TouchableOpacity, Alert } from 'react-native';
import * as constants from '../Constants';
import PropTypes from 'prop-types';
import componentsStyle from '../css/components';
import request from '../utils/request';
import askForPermissions from "../utils/permissions";


class InitialPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            response: null,
            roles: [],
        };
    }

    componentDidMount() {
        askForPermissions();
        request.get('/core/person/info/')
            .then((response) => {
                this.setState({ ...response.data, response });
            }).catch((error) => {
                this.setState({ response: {} });
                Alert.alert(constants.textStrings.app, constants.errors.fetchPersonInfoFailed)
                console.log(error.response);
            });
    }

    render() {
        const { navigate } = this.props.navigation;
        askForPermissions();
        return (
            <View style={componentsStyle.container}>
                <View style={componentsStyle.responsiveBox}>
                    <Image source={require('../assets/tracker.png')}
                        style={componentsStyle.image} />
                    <View>
                        <Text>{(this.state.roles.length != 0) ? `${constants.textStrings.currentlyRegistered} ${this.state.roles}` : constants.textStrings.currentlyNotRegistered} </Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => (this.state.roles.includes('S')) ? navigate(constants.navigation.student) : navigate(constants.navigation.studentRegistration)}
                        style={componentsStyle.buttonStyle}
                    >
                        <Text style={componentsStyle.buttonText}>{constants.textStrings.student}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => (this.state.roles.includes('T') || this.state.roles.includes('A')) ? navigate(constants.navigation.professor) : navigate(constants.navigation.professorRegistration)}
                        style={componentsStyle.buttonStyleTwo}
                        disabled={this.state.response === null}
                    >
                        <Text style={componentsStyle.buttonText}>{constants.textStrings.professor}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

}

InitialPage.propTypes = {
    navigation: PropTypes.object,
};

export default InitialPage;