import React, { Component } from "react";
import PropTypes from 'prop-types';
import { BackHandler } from 'react-native';
import ProfessorCourses from './ProfessorCourses';
import CourseQR from './CourseQR';
import CourseLectures from './CourseLectures'
import LectureStudents from './LectureStudents';
import QrCodeGenerator from './QrCodeGenerator';
import CourseStudents from "./CourseStudents";
import RNExitApp from 'react-native-exit-app';

class Courses extends Component {
    _didFocusSubscription;
    _willBlurSubscription;

    constructor(props) {
        super(props);
        this.state = {
            render: "ProfessorCourses",
            previousScreen: "",
            currentCourse: {},
            currentLecture: {},
            qr_code_id: 0,
        }

        this.openCourseQR = this.openCourseQR.bind(this);
        this.openCourseLectures = this.openCourseLectures.bind(this);
        this.openCourseStudents = this.openCourseStudents.bind(this);
        this.onLectureStudents = this.onLectureStudents.bind(this);
        this.openQrGenerator = this.openQrGenerator.bind(this);
        this.openCourses = this.openCourses.bind(this);

        this._didFocusSubscription = props.navigation.addListener('didFocus', () => {
            BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
        });
    }

    componentDidMount() {
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', () =>
            BackHandler.removeEventListener('hardwareBackPress', () => {
            })
        );
    }

    componentWillUnmount() {
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }

    render() {
        let renderScreen = null;
        switch (this.state.render) {
            case "CourseQR":
                renderScreen = <CourseQR navigation={this.props.navigation} course={this.state.currentCourse} openQrGenerator={this.openQrGenerator} />
                break;
            case "QRCodeGenerator":
                renderScreen = <QrCodeGenerator qr_code_id={this.state.qr_code_id} course={this.state.currentCourse} openCourses={this.openCourses} navigation={this.props.navigation}/>
                break;
            case "CourseLectures":
                renderScreen = <CourseLectures onLectureStudents={this.onLectureStudents} course={this.state.currentCourse} />
                break;
            case "CourseStudents":
                renderScreen = <CourseStudents navigation={this.props.navigation} course={this.state.currentCourse} openCourseStudents={this.openCourseStudents}/>
                break;
            case "LectureStudents":
                renderScreen = <LectureStudents lecture={this.state.currentLecture} course={this.state.currentCourse} />
                break;
            default:
                renderScreen = (<ProfessorCourses openCourseQR={this.openCourseQR}
                    openCourseLectures={this.openCourseLectures}
                    openCourseStudents={this.openCourseStudents} />)
                break;
        }
        return (renderScreen);
    }

    handleBackPress = () => {
        if (this.state.render === 'ProfessorCourses') {
            this.props.navigation.popToTop();
            this.setState({
                render: "",
            });
        } else if (this.state.render === 'CourseQR' ||
          this.state.render === 'QRCodeGenerator' ||
          this.state.render === 'CourseLectures' ||
          this.state.render === 'CourseStudents' ||
          this.state.render === 'LectureStudents') {
            this.setState({
                render: this.state.previousScreen,
                previousScreen: 'ProfessorCourses',
            });
        } else {
            RNExitApp.exitApp();
        }
        return true;
    }

    openCourseQR(course) {
        this.setState({
            render: "CourseQR",
            currentCourse: course,
            previousScreen: 'ProfessorCourses',
        });
    }

    openCourses() {
        this.setState({
            render: "ProfessorCourses",
        });
    }

    openCourseLectures(course) {
        this.setState({
            render: "CourseLectures",
            currentCourse: course,
            previousScreen: 'ProfessorCourses',
        });
    }

    onLectureStudents(lecture) {
        this.setState({
            render: "LectureStudents",
            currentLecture: lecture,
            previousScreen: 'CourseLectures',
        })
    }

    openQrGenerator(course, id, previousScreen) {
        this.setState({
            render: "QRCodeGenerator",
            previousScreen,
            qr_code_id: id,
            currentCourse: course,
        });
    }

    openCourseStudents(course) {
        this.setState({
            render: "CourseStudents",
            currentCourse: course,
            previousScreen: 'ProfessorCourses',
        });
    }
}

Courses.propTypes = {
    navigation: PropTypes.object,
};


export default Courses;