import React, { Component } from "react";
import PropTypes from 'prop-types';
import componentsStyle from '../css/components'
import { Text, View, ScrollView } from 'react-native';
import request from '../utils/request';

class LectureStudents extends Component {
    constructor(props) {
        super(props);
        this.state = {
            students: [],
            course_name: '',
        }
    }

    componentDidMount() {
        request.get(`/core/lecture/attendees/${this.props.lecture.data.id}`)
            .then((response) => {
                this.setState({
                    students: response.data,
                });
            }).catch((error) => {
                this.setState({ error: error.response });
                console.log(error.response);
            });
    }

    render() {
        let students = this.state.students.map((student, idx) => <Text key={idx} style={{ fontSize: 18, padding: 2 }}>{student.person.first_name} {student.person.last_name}</Text>);
        return (
            <ScrollView style={componentsStyle.scrollContainer}>
                <Text style={componentsStyle.textHeaderName}>{this.props.course.name}</Text>
                <Text style={componentsStyle.textHeaderLecture}>Predavanje {this.props.lecture.lectureIdx + 1}</Text>
                <View style={componentsStyle.lectureStudentsTwo}>{students}</View>
            </ScrollView>
        );
    }
}

LectureStudents.propTypes = {
    lecture: PropTypes.object,
    course: PropTypes.object,
};


export default LectureStudents;