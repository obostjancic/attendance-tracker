import React, { Component } from "react";
import { ScrollView, View, Text, TouchableOpacity, Alert } from "react-native";
import componentsStyle from '../css/components';
import request from '../utils/request';
import * as constants from "../Constants";
import PropTypes from 'prop-types';

class ProfessorCourses extends Component {
    static navigationOptions = {
        headerLeft: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            courses: [],
            role: 'T',
            active: false,
            activeCourse: null,
        };
    }

    componentDidMount() {
        request.get(`/core/course/${this.state.role}`)
            .then((response) => {
                let courses = response.data;
                this.setState({ courses });
            }).catch((error) => {
                this.setState({ error: error.response });
                Alert.alert(constants.textStrings.app, constants.errors.courseListFetchFailed);
                console.log(error.response);
            });
    }

    render() {
        let courses = this.state.courses.map((course, idx) => this.state.activeCourse === idx ? <View key={idx}>
            <TouchableOpacity style={componentsStyle.activeListItemBGColor} onPress={() => this.listItemSelected(idx)}><Text
                style={componentsStyle.activeListItemTextColor}>{course.name}</Text></TouchableOpacity>
            <View style={componentsStyle.responsiveBoxList}>
                <View style={componentsStyle.detailsStyleList}>
                    <Text style={componentsStyle.detailsStyleList} onPress={() => this.props.openCourseQR(course)}>{constants.lecture.open}</Text>
                    <Text style={componentsStyle.detailsStyle} onPress={() => this.props.openCourseLectures(course)}>{constants.lecture.lectures}</Text>
                    <Text style={componentsStyle.detailsStyle} onPress={() => this.props.openCourseStudents(course)}>{constants.lecture.students}</Text>
                </View>
            </View>
        </View> : <TouchableOpacity key={idx} style={componentsStyle.listItem} onPress={() => this.listItemSelected(idx)}><Text>{course.name}</Text></TouchableOpacity>);
        return (
            <ScrollView style={componentsStyle.scrollContainer}>
                {courses}
            </ScrollView>
        );
    }

    listItemSelected = (idx) => {
        let activeCourse = null;
        if (this.state.activeCourse !== idx) {
            activeCourse = idx;
        }
        this.setState({ activeCourse });
    }
}

ProfessorCourses.propTypes = {
    openCourseQR: PropTypes.func,
    openCourseLectures: PropTypes.func,
    openCourseStudents: PropTypes.func,
};


export default ProfessorCourses;