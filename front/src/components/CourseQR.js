import React, { Component } from 'react';

import { Text, View, TouchableOpacity, TextInput, ScrollView, Picker, Alert } from 'react-native';
import PropTypes from 'prop-types';
import componentsStyle from "../css/components";
import * as constants from "../Constants";
import request from "../utils/request";

class CourseQR extends Component {
    constructor(props) {
        super(props);
        this.state = {
            course: props.course,
            classroom: '',
            classrooms: [],
            duration: '',
            person_limit: '',
            selected: -1,
        }
        this.onPressSendData = this.pressSendData.bind(this);
    }

    componentDidMount() {
        request.get('/core/classroom/')
            .then((response) => {
                this.setState({
                    classrooms: response.data,
                    selected: response.data[0].id,
                });
            }).catch((error) => {
                this.setState({ error: error.response });
                Alert.alert(constants.textStrings.app, constants.errors.classroomListFetchFailed);
                console.log(error.response);
            });

        request.get('/core/lecture/current/')
            .then((response) => {
                let id = response.data.id;
                if (id && this.props.course.name === response.data.course.name) {
                    this.props.openQrGenerator(this.props.course, id, "ProfessorCourses")
                }
            }).catch((error) => {
                console.log('Error: ', error.response);
            });
    }

    render() {
        return (
            <ScrollView style={componentsStyle.scrollContainer}>
                <View style={componentsStyle.container}>
                    <View style={componentsStyle.responsiveBox}>
                        <Text style={componentsStyle.textHeader}>{this.state.course.name}</Text>
                        <Text style={componentsStyle.textPicker}>{constants.textStrings.chooseClassroom}</Text>
                        <Picker
                            selectedValue={this.state.selected}
                            style={{ height: 50, width: 100 }}
                            onValueChange={(itemValue) => this.setState({ selected: itemValue })}>
                            {this.state.classrooms.map((classroom, idx) => <Picker.Item key={idx} label={classroom.name} value={classroom.id} />)}
                        </Picker>
                        <TextInput keyboardType='numeric' onChangeText={(text) => this.setState({ duration: text })}
                            placeholder={constants.placeholders.duration}
                            placeholderTextColor="rgb(0,0,0)"
                            style={[componentsStyle.input]}
                            value={this.state.duration} />
                        <TextInput keyboardType='numeric' onChangeText={(text) => this.setState({ person_limit: text })}
                            placeholder={constants.placeholders.person_limit}
                            placeholderTextColor="rgb(0,0,0)"
                            style={[componentsStyle.input]}
                            value={this.state.person_limit} />
                        <TouchableOpacity onPress={this.onPressSendData}
                            style={componentsStyle.buttonStyleThree}>
                            <Text style={componentsStyle.buttonText}>{constants.textStrings.generate}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }

    pressSendData() {
        request.post('/core/lecture/start/', {
            course: this.state.course.id,
            classroom: parseInt(this.state.selected, 10),
            duration: parseInt(this.state.duration, 10),
            person_limit: parseInt(this.state.person_limit, 10),
        }).then((response) => {
            this.props.openQrGenerator(this.props.course, response.data.qr_code.id, "ProfessorCourses");
        }).catch((error) => {
            this.setState({ error });
            Alert.alert(constants.textStrings.app, constants.errors.startLectureFailed);
            console.log(error.response);
        });
    }
}

CourseQR.propTypes = {
    course: PropTypes.object,
    openQrGenerator: PropTypes.func,
    professorOpenCourse: PropTypes.func,
    navigation: PropTypes.object,
};


export default CourseQR;