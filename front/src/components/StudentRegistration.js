import React from "react";
import { Alert, Text, TextInput, TouchableOpacity, View, ScrollView } from "react-native";
import * as constants from '../Constants';
import PropTypes from 'prop-types';
import componentsStyle from '../css/components';
import ValidateHelper from "../helpers/ValidateHelper";
import deviceInfo from 'react-native-device-info';
import request from "../utils/request";

class StudentRegistration extends React.Component {
    static navigationOptions = {
        title: constants.navigationOptions.studentTitle,
    };

    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            firstNameValidate: true,
            last_name: '',
            lastNameValidate: true,
            email: '',
            emailValidate: true,
            externalId: '',
            navigation: props.navigation,
        };

        this.navigateTo = this.navigateTo.bind(this);
        this.onPressSendData = this.pressSendData.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.navigation !== prevProps.navigation) {
            this.setState({ navigate: this.props.navigation });
        }
    }

    render() {

        const { first_name, last_name, email, externalId, firstNameValidate, lastNameValidate, emailValidate } = this.state;
        const onSubmit =
            first_name.length > 0 &&
            last_name.length > 0 &&
            email.length > 0 &&
            externalId.length > 0 &&
            firstNameValidate === true &&
            lastNameValidate === true &&
            emailValidate === true;

        return (
            <ScrollView style={componentsStyle.scrollContainer}>
                <View style={componentsStyle.container}>
                    <View style={componentsStyle.responsiveBox}>
                        <TextInput onChangeText={(text) => this.validate(text, constants.fieldTypes.name)}
                            placeholder={constants.placeholders.name}
                            placeholderTextColor="rgb(0,0,0)"
                            style={[componentsStyle.input, !this.state.firstNameValidate ? componentsStyle.error : null]}
                            value={this.state.first_name} />
                        <TextInput onChangeText={(text) => this.validate(text, constants.fieldTypes.surname)}
                            placeholder={constants.placeholders.surname}
                            placeholderTextColor="rgb(0,0,0)"
                            style={[componentsStyle.input, !this.state.lastNameValidate ? componentsStyle.error : null]}
                            value={this.state.last_name} />
                        <TextInput onChangeText={(text) => { this.setState({ externalId: text }); }}
                            placeholder={constants.placeholders.index}
                            placeholderTextColor="rgb(0,0,0)"
                            style={componentsStyle.input}
                            value={this.state.externalId} />
                        <TextInput onChangeText={(text) => this.validate(text, constants.fieldTypes.email)}
                            placeholder={constants.placeholders.email}
                            placeholderTextColor="rgb(0,0,0)"
                            style={[componentsStyle.input, !this.state.emailValidate ? componentsStyle.error : null]}
                            autoCapitalize='none'
                            value={this.state.email} />
                        <TouchableOpacity onPress={this.onPressSendData}
                            disabled={!onSubmit}
                            style={componentsStyle.buttonStyleThree}>
                            <Text style={componentsStyle.buttonText}>{constants.textStrings.send}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }

    pressSendData() {
        request.post('/core/register/', {
            person: {
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                external_id: this.state.externalId,
                email: this.state.email,
            },
            device: {
                device_external_id: deviceInfo.getUniqueID(),
            },
        }).then((response) => {
            this.setState({ ...response.data.person });
            return this.replaceWith(constants.navigation.student),
            Alert.alert(`${constants.textStrings.app, constants.textStrings.registration}  ${this.state.first_name} ${this.state.last_name}`)
        }).catch((error) => {
            this.setState({ error: error.response });
            Alert.alert(constants.textStrings.app, constants.errors.registrationFailed)
            console.log(error.response);
        });
    }

    navigateTo(path) {
        this.state.navigation.navigate(path);
    }

    replaceWith(path) {
        this.state.navigation.replace(path);
    }

    validate(text, type) {
        let validate = ValidateHelper.validate(text, type);
        switch (type) {
            case constants.fieldTypes.name:
                this.setState({
                    firstNameValidate: validate,
                    first_name: text,
                });
                break;
            case constants.fieldTypes.surname:
                this.setState({
                    lastNameValidate: validate,
                    last_name: text,
                });
                break;
            case constants.fieldTypes.email:
                this.setState({
                    emailValidate: validate,
                    email: text,
                });
                break;
        }
    }
}

StudentRegistration.propTypes = {
    navigation: PropTypes.object,
};

export default StudentRegistration;
