import axios from 'axios';
import deviceInfo from 'react-native-device-info';

const client = ( deviceId = null) => {
    let defaultOptions = {
        headers: {
            "Device-Id": deviceId ? deviceId : '',
        },
    };
    return {
        get: (url, options = {}) => {
            defaultOptions = getDefaultOptions();
            return axios.get(url, { ...defaultOptions, ...options })
        },
        post: (url, data, options = {}) => {
            defaultOptions = getDefaultOptions();
            console.log(defaultOptions, url);
            return axios.post(url, data, { ...defaultOptions, ...options })
        },
        put: (url, data, options = {}) => {
            defaultOptions = getDefaultOptions();
            return axios.put(url, data, { ...defaultOptions, ...options })
        },
        delete: (url, options = {}) => {
            defaultOptions = getDefaultOptions();
            return axios.delete(url, { ...defaultOptions, ...options })
        },

    }
};

const getDefaultOptions = () => {
    let deviceId = getDeviceId();
    let baseURL = process.env.NODE_ENV !== 'production' ? "https://attendance-tracker-develop.herokuapp.com/api" : "https://attendance-tracker-release.herokuapp.com/api";
    return {
        baseURL,
        headers: {
            "Device-Id": deviceId ? deviceId : '',
        },
    };
};

const getDeviceId = () => deviceInfo.getUniqueID();
const request = client(getDeviceId());

export default request;