import Permissions from 'react-native-permissions';
import RNExitApp from 'react-native-exit-app';

const askForPermissions = () => {
    Permissions.checkMultiple(['camera', 'location', 'storage']).then(response => {
        if (response.camera === 'denied' || response.camera === 'undetermined') {
            Permissions.request('camera').then(responseCamera => {
                if (responseCamera === 'denied' || responseCamera === 'undetermined') {
                    RNExitApp.exitApp();
                } else {
                    Permissions.request('location').then(responseLocation => {
                        if (responseLocation === 'denied' || responseLocation === 'undetermined') {
                            RNExitApp.exitApp();
                        } else {
                            Permissions.request('storage').then(responseStorage => {
                                if (responseStorage === 'denied' || responseStorage === 'undetermined') {
                                  RNExitApp.exitApp();
                                }
                            });
                        }
                    });
                }
            });
        }
        if (response.location === 'denied' || response.location === 'undetermined') {
            Permissions.request('location').then(responseLocation => {
                if (responseLocation === 'denied' || responseLocation === 'undetermined') {
                    RNExitApp.exitApp();
                } else {
                    Permissions.request('camera').then(responseCamera => {
                        if (responseCamera === 'denied' || responseCamera === 'undetermined') {
                            RNExitApp.exitApp();
                        } else {
                            Permissions.request('storage').then(responseStorage => {
                                if (responseStorage === 'denied' || responseStorage === 'undetermined') {
                                    RNExitApp.exitApp();
                                }
                            });
                        }
                    });
                }
            });
        }
        if (response.storage === 'denied' || response.storage === 'undetermined') {
            Permissions.request('storage').then(responseStorage => {
                if (responseStorage === 'denied' || responseStorage === 'undetermined') {
                    RNExitApp.exitApp();
                } else {
                    Permissions.request('camera').then(responseCamera => {
                        if (responseCamera === 'denied' || responseCamera === 'undetermined') {
                            RNExitApp.exitApp();
                        } else {
                            Permissions.request('location').then(responseLocation => {
                                if (responseLocation === 'denied' || responseLocation === 'undetermined') {
                                    RNExitApp.exitApp();
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

export default askForPermissions;