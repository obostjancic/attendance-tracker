import Geolocation from 'react-native-geolocation-service';

const gpsLocation = () => (
    new Promise((resolve, reject) => (
        Geolocation.getCurrentPosition(
            (position) => {
                resolve ({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                });
            },
            (error) => console.log(error.message),
            {enableHighAccuracy: true, timeout: 5000, maximumAge: 1000 },
        )
    ))
);

export default gpsLocation;