/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import {createMaterialTopTabNavigator, createStackNavigator} from 'react-navigation';

import InitialPage from './src/components/InitialPage';
import StudentRegistration from './src/components/StudentRegistration';
import ProfessorRegistration from './src/components/ProfessorRegistration';
import QrScanner from './src/components/QrScanner';
import StudentAttendance from './src/components/StudentAttendance';
import Courses from "./src/components/Courses";
import ProfessorCourses from "./src/components/ProfessorCourses";
import ProfessorOpenCourse from "./src/components/ProfessorOpenCourse";
import CourseQR from "./src/components/CourseQR";
import QrCodeGenerator from "./src/components/QrCodeGenerator";
import CourseLectures from "./src/components/CourseLectures";

const BuildApp = createStackNavigator({
    Home: { screen: InitialPage, navigationOptions: {header: null}},
    StudentRegistration: { screen: StudentRegistration},
    ProfessorRegistration: {screen: ProfessorRegistration},

    Student: {
        screen: createMaterialTopTabNavigator(
            {
                Prijava: {screen: QrScanner, navigationOptions: {header: null}},
                Prisustva: {screen: StudentAttendance, navigationOptions: {header: null}},
            },
            {
                tabBarOptions: {
                    style: {
                        backgroundColor: "#1f56af",
                    },
                    tabStyle: {
                        activeBackgroundColor: "#1f56af",
                        inactiveBackgroundColor: "#fff",
                    },
                    labelStyle: {
                        fontSize: 15,
                        fontWeight: 'bold',
                    },
                    activeTintColor: "white",
                    inactiveTintColor: "white",
                    indicatorStyle: {backgroundColor: 'black'},
                },
            },
        ),
        navigationOptions: {header: null}},
    Professor: {
        screen: createMaterialTopTabNavigator(
            {
                Kursevi: {screen: Courses , navigationOptions: {header: null}},
                Trenutno: {screen: ProfessorOpenCourse, navigationOptions: {header: null}},
            },
            {
                tabBarOptions: {
                    style: {
                        backgroundColor: "#1f56af",
                    },
                    tabStyle: {
                        activeBackgroundColor: "#1f56af",
                        inactiveBackgroundColor: "#fff",
                    },
                    labelStyle: {
                        fontSize: 15,
                        fontWeight: 'bold',
                    },
                    activeTintColor: "white",
                    inactiveTintColor: "white",
                    indicatorStyle: {backgroundColor: 'black'},
                },
            },
        ),
        navigationOptions: {header: null, qr_code_id: undefined}},
    OpenLecture: {
        screen: CourseQR,
        navigationOptions: {header: null},
    },
    ProfessorCourses: {
        screen: ProfessorCourses,
        navigationOptions: {header: null},
    },
    OpenQrGenerator: {
        screen: QrCodeGenerator,
        navigationOptions: {header: null},
    },
    CourseLectures: {
        screen: CourseLectures,
        navigationOptions: {header: null},
    },
});

export default BuildApp;
